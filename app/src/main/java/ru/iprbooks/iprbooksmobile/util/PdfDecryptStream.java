package ru.iprbooks.iprbooksmobile.util;

import com.radaee.pdf.Document;

import java.io.IOException;
import java.io.RandomAccessFile;

public class PdfDecryptStream implements Document.PDFStream {

    public final static int BLOCK_SIZE = 4096;
    private RandomAccessFile file;
    private char[] key;

    public void open(String path, String key) {
        try {
            this.key = key.toCharArray();
            file = new RandomAccessFile(path, "rw");
            file.seek(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean writeable() {
        return true;
    }

    @Override
    public int get_size() {
        try {
            return file.length() > 0 ? (int) file.length() : 0;
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public int read(byte[] data) {
        int pos;
        int len;
        byte[] out = new byte[data.length];
        try {
            pos = (int) file.getFilePointer();
            len = file.read(out);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }

        for (int i = pos; i < pos + out.length; i++) {
            int k = i / BLOCK_SIZE;
            int cur;
            if (k % 2 == 0) {
                cur = (i * 2) % key.length;
            } else {
                cur = key.length - (i * 2 + 1) % key.length;
            }
            out[i - pos] = (byte) (out[i - pos] ^ key[cur]);
        }
        System.arraycopy(out, 0, data, 0, data.length);
        return Math.max(len, 0);
    }

    @Override
    public int write(byte[] data) {
        try {
            file.write(data);
            return data.length;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void seek(int pos) {
        try {
            file.seek(pos);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int tell() {
        try {
            return (int) file.getFilePointer();
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

}