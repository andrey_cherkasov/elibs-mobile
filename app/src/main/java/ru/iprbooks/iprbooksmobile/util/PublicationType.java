package ru.iprbooks.iprbooksmobile.util;

public class PublicationType {

    public final static int BOOK = 1;

    public final static int JOURNAL = 2;

}
