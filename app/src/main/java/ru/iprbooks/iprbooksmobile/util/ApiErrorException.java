package ru.iprbooks.iprbooksmobile.util;

public class ApiErrorException extends Exception {

    public ApiErrorException(String message) {
        super(message);
    }
}
