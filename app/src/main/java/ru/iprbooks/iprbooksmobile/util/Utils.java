package ru.iprbooks.iprbooksmobile.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.provider.Settings;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.iprbooks.iprbooksmobile.BuildConfig;

public class Utils {

    private final Context context;

    public Utils(Context context) {
        this.context = context;
    }

    @SuppressLint("HardwareIds")
    public final String getDeviceId() {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public String getDay(Long time) {
        Date date = new Date();
        date.setTime(time);
        return new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(date);
    }

    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            StringBuilder hash = new StringBuilder(number.toString(16));
            while (hash.length() < 32) {
                hash.insert(0, "0");
            }
            return hash.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String generateHash(String salt, String accessToken) {
        String deviceId = getDeviceId();

        Date currentDate = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        String dateStr = sdf.format(currentDate);

        return getMD5(salt + accessToken + deviceId + dateStr);
    }

    public static String hashPassword(String password) {
        String correctPassword = getMD5(password);
        return getMD5(String.format("%s%s", correctPassword, BuildConfig.SALT));
    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

}
