package ru.iprbooks.iprbooksmobile;

import android.app.Application;
import android.os.StrictMode;

import androidx.annotation.NonNull;
import androidx.hilt.work.HiltWorkerFactory;
import androidx.work.Configuration;

import javax.inject.Inject;

import dagger.hilt.InstallIn;
import dagger.hilt.android.HiltAndroidApp;
import dagger.hilt.components.SingletonComponent;

@HiltAndroidApp
public class IprSmartApplication extends Application implements Configuration.Provider {

    @Inject
    HiltWorkerFactory workerFactory;

    @NonNull
    @Override
    public Configuration getWorkManagerConfiguration() {
        return new Configuration.Builder()
                .setWorkerFactory(workerFactory)
                .build();
    }

//    public void onCreate() {
//        if (BuildConfig.DEBUG) {
//            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
////                    .detectAll()
//                    .detectDiskReads()
//                    .detectDiskWrites()
//                    .detectNetwork()   // or .detectAll() for all detectable problems
//                    .penaltyDialog()
//                    .penaltyLog()
//                    .build());
////            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//////                    .detectAll()
////                    .detectLeakedSqlLiteObjects()
////                    .detectLeakedClosableObjects()
//////                    .penaltyLog()
////                    .penaltyDeath()
////                    .build());
//        }
//        super.onCreate();
//    }

}
