package ru.iprbooks.iprbooksmobile.di;


import android.content.Context;

import androidx.room.Room;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.data.local.IprSmartDataBase;

@Module
@InstallIn(SingletonComponent.class)
public class DataBaseModule {

    @Provides
    @Singleton
    IprSmartDataBase provideIprSmartDataBase(@ApplicationContext Context context) {
        return Room.databaseBuilder(
                context,
                IprSmartDataBase.class,
                BuildConfig.APP_DB_NAME
        ).build();
    }

}