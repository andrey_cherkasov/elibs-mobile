package ru.iprbooks.iprbooksmobile.di;

import android.content.Context;

import androidx.work.WorkManager;

import com.pddstudio.preferences.encrypted.EncryptedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.data.local.FileHelper;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.util.Utils;


@Module
@InstallIn(SingletonComponent.class)
public class SystemServiceModule {

    @Provides
    @Singleton
    EncryptedPreferences provideEncryptedSharedPreferences(@ApplicationContext Context context) {
        return new EncryptedPreferences.Builder(context).withEncryptionPassword(BuildConfig.SP_SECRET_KEY).build();
    }

    @Provides
    @Singleton
    EncryptedPreferences.EncryptedEditor provideEncryptedSharedPreferencesEditor(EncryptedPreferences encryptedPreferences) {
        return encryptedPreferences.edit();
    }

    @Provides
    @Singleton
    Utils provideUtils(@ApplicationContext Context context) {
        return new Utils(context);
    }

    @Provides
    @Singleton
    WorkManager provideWorkManager(@ApplicationContext Context context) {
        return WorkManager.getInstance(context);
    }

    @Provides
    @Singleton
    FileHelper provideFileHelper(@ApplicationContext Context context, SharedPreferencesHelper preferencesHelper) {
        return new FileHelper(context, preferencesHelper);
    }

}
