package ru.iprbooks.iprbooksmobile.di;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.iterseptors.AuthInterceptor;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.iterseptors.LoggingInterceptor;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.iterseptors.NetworkConnectionInterceptor;
import ru.iprbooks.iprbooksmobile.util.Utils;

@Module
@InstallIn(SingletonComponent.class)
public class NetworkModule {

    @Provides
    @Singleton
    @Named("LoggingInterceptor")
    Interceptor provideLoggingInterceptor() {
        return LoggingInterceptor.create();
    }

    @Provides
    @Singleton
    @Named("NetworkConnectionInterceptor")
    Interceptor provideNetworkConnectionInterceptor(@ApplicationContext Context context) {
        return new NetworkConnectionInterceptor(context);
    }

    @Provides
    @Singleton
    @Named("AuthInterceptor")
    Interceptor provideAuthInterceptor(SharedPreferencesHelper preferencesHelper, Utils utils) {
        return new AuthInterceptor(preferencesHelper, utils);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(@Named("LoggingInterceptor") Interceptor loggingInterceptor,
                                     @Named("NetworkConnectionInterceptor") Interceptor networkConnectionInterceptor,
                                     @Named("AuthInterceptor") Interceptor authInterceptor) {

        return new OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(authInterceptor)
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    MoshiConverterFactory provideMoshiConverterFactory() {
        return MoshiConverterFactory.create();
    }

    @Provides
    @Singleton
    RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient,
                             MoshiConverterFactory moshiConverterFactory,
                             RxJava2CallAdapterFactory rxJava2CallAdapterFactory) {

        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(okHttpClient)
                .addConverterFactory(moshiConverterFactory)
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .build();
    }

}
