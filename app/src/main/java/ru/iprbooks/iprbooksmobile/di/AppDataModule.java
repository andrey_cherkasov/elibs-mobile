package ru.iprbooks.iprbooksmobile.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Retrofit;
import ru.iprbooks.iprbooksmobile.data.local.IprSmartDataBase;
import ru.iprbooks.iprbooksmobile.data.local.dao.BookmarkDao;
import ru.iprbooks.iprbooksmobile.data.local.dao.PublicationDao;
import ru.iprbooks.iprbooksmobile.data.local.dao.QuoteDao;
import ru.iprbooks.iprbooksmobile.data.remote.AuthApiInterface;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;

@Module
@InstallIn(SingletonComponent.class)
public class AppDataModule {

    @Provides
    @Singleton
    AuthApiInterface provideAuthApiInterface(Retrofit retrofit) {
        return retrofit.create(AuthApiInterface.class);
    }

    @Provides
    @Singleton
    DataApiInterface provideDataApiInterface(Retrofit retrofit) {
        return retrofit.create(DataApiInterface.class);
    }

    @Provides
    @Singleton
    PublicationDao providePublicationDao(IprSmartDataBase dataBase) {
        return dataBase.publicationDao();
    }

    @Provides
    @Singleton
    QuoteDao provideQuoteDao(IprSmartDataBase dataBase) {
        return dataBase.quoteDao();
    }

    @Provides
    @Singleton
    BookmarkDao provideBookmarkDao(IprSmartDataBase dataBase) {
        return dataBase.bookmarkDao();
    }

}
