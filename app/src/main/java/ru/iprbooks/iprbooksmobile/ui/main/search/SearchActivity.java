package ru.iprbooks.iprbooksmobile.ui.main.search;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import dagger.hilt.android.AndroidEntryPoint;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.NoConnectivityException;
import ru.iprbooks.iprbooksmobile.databinding.ActivitySearchBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BasePagingAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.LoadingDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.PaginationListener;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.publicationDetail.PublicationDetailActivity;
import ru.iprbooks.iprbooksmobile.ui.main.publications.PublicationsDelegateAdapter;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.EmptyListException;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@AndroidEntryPoint
public class SearchActivity extends BaseActivity implements SearchView {

    public final static String TAG = "SearchActivity";

    private ActivitySearchBinding binding;
    private SearchViewModel viewModel;
    private BasePagingAdapter<IListItem> adapter;
    private final CompositeDisposable disposable = new CompositeDisposable();
    private final BehaviorSubject<String> textInputSubject = BehaviorSubject.create();
    private final Flowable<String> textInputFlowable = textInputSubject.toFlowable(BackpressureStrategy.LATEST);
    private int type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySearchBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(SearchViewModel.class);
        type = getIntent().getIntExtra(MainActivity.PUBLICATION_TYPE, PublicationType.BOOK);
        setContentView(binding.getRoot());

        SearchDelegateAdapter searchDelegateAdapter = new SearchDelegateAdapter();
        searchDelegateAdapter.setOnListViewItemClickListener((v, item, position) -> openDetailScreen(item.getId()));

        List<BaseDelegateAdapter> delegateAdapters = new ArrayList<>();
        delegateAdapters.add(searchDelegateAdapter);
        delegateAdapters.add(new LoadingDelegateAdapter());

        // init adapter
        adapter = new BasePagingAdapter<>(new DiffUtil.ItemCallback<IListItem>() {
            @Override
            public boolean areItemsTheSame(@NonNull IListItem oldItem, @NonNull IListItem newItem) {
                if (oldItem instanceof Publication && newItem instanceof Publication) {
                    return ((Publication) oldItem).getId().equals(((Publication) newItem).getId());
                }
                return true;
            }

            @Override
            public boolean areContentsTheSame(@NonNull IListItem oldItem, @NonNull IListItem newItem) {
                return true;
            }
        }, delegateAdapters);

        // init ui
        binding.list.setAdapter(adapter);
        binding.list.addOnScrollListener(new PaginationListener((LinearLayoutManager) binding.list.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                viewModel.search(type);
            }
        });
        binding.noInternetLayout.refresh.setOnClickListener(v -> updateList());
        binding.searchClear.setOnClickListener(v -> binding.search.setText(""));
        binding.back.setOnClickListener(v -> finish());
        binding.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String search = s.toString();
                if (!search.equals(viewModel.getSearch().getValue())) {
                    textInputSubject.onNext(search);
                }
            }
        });

        disposable.add(textInputFlowable.debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(s -> {
                    viewModel.setSearch(s);
                    updateList();
                }, Throwable::printStackTrace));

        // observer
        viewModel.getPublications().observe(this, listStateData -> {
            DataStatus status = listStateData.getStatus();
            if (status.equals(DataStatus.SUCCESS)) {
                if (listStateData.getData() != null) {
                    adapter.submitList(new ArrayList<>(listStateData.getData().getData()));
                } else {
                    adapter.submitList(new ArrayList<>());
                }
                binding.loadingLayout.getRoot().setVisibility(View.GONE);
            } else if (status.equals(DataStatus.ERROR)) {
                adapter.submitList(new ArrayList<>());
                Throwable throwable = listStateData.getError();
                if (throwable != null) {
                    if (throwable instanceof EmptyListException) {
                        binding.notFoundLayout.getRoot().setVisibility(View.VISIBLE);
                    } else if (throwable instanceof ApiErrorException) {
                        showMessage(throwable.getMessage());
                    } else if (throwable instanceof NoConnectivityException) {
                        binding.noInternetLayout.getRoot().setVisibility(View.VISIBLE);
                    } else {
                        showMessage(getString(R.string.error));
                        throwable.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.clear();
    }

    @Override
    public void openDetailScreen(long id) {
        Intent intent = new Intent(this, PublicationDetailActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_ID, id);
        intent.putExtra(MainActivity.PUBLICATION_TYPE, type);
        intent.putExtra(MainActivity.PUBLICATION_OPENED_FROM, TAG);
        startActivity(intent);
    }

    @Override
    public void updateList() {
        binding.notFoundLayout.getRoot().setVisibility(View.GONE);
        binding.noInternetLayout.getRoot().setVisibility(View.GONE);
        binding.loadingLayout.getRoot().setVisibility(View.VISIBLE);
        viewModel.searchInit(type);
    }

}
