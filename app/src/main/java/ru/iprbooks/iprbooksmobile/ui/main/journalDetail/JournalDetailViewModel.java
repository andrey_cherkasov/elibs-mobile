package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.model.Journal;
import ru.iprbooks.iprbooksmobile.data.model.JournalYear;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;

@HiltViewModel
public class JournalDetailViewModel extends BaseDisposablesViewModel {

    private final MutableStateLiveData<Journal> journalLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<List<JournalNumber>> journalNumberLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<String> titleLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<String> subTitleLiveData = new MutableStateLiveData<>();

    private final DataApiInterface dataApi;


    @Inject
    public JournalDetailViewModel(DataApiInterface dataApi) {
        this.dataApi = dataApi;
    }

    LiveData<StateData<Journal>> getJournal() {
        return journalLiveData;
    }

    LiveData<StateData<List<JournalNumber>>> getJournalNumber() {
        return journalNumberLiveData;
    }

    LiveData<StateData<String>> getTitle() {
        return titleLiveData;
    }

    LiveData<StateData<String>> getSubTitle() {
        return subTitleLiveData;
    }

    void getJournal(long publicationId) {
        disposables.add(dataApi.getJournal(publicationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> journalLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                })
                .map(ResponseData::getData)
                .subscribe(journalLiveData::setSuccess, journalLiveData::setError));
    }

    void getNumberByYears(List<JournalYear> yearsList) {
        List<JournalNumber> journalNumbersByYearsList = new ArrayList<>();

        List<Single<ResponseData<JournalYear>>> requestsList = new ArrayList<>();
        for (JournalYear journalYear : yearsList) {
            requestsList.add(dataApi.getNumbersByYear(journalYear.getId()));
        }

        disposables.add(Single.concat(requestsList)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(subscription -> journalNumberLiveData.setLoading())
                .map(ResponseData::getData)
                .subscribe(data -> {
                            journalNumbersByYearsList.add(new JournalNumber(data.getYear()));
                            for (Publication number : data.getNumbers()) {
                                journalNumbersByYearsList.add(new JournalNumber(number));
                            }
                        },
                        journalNumberLiveData::setError,
                        () -> journalNumberLiveData.setSuccess(journalNumbersByYearsList)))
        ;
    }

    void setTitle(String title) {
        titleLiveData.setSuccess(title);
    }

    void setSubTitle(String subTitle) {
        subTitleLiveData.setSuccess(subTitle);
    }

}
