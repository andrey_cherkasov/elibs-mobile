package ru.iprbooks.iprbooksmobile.ui.main.journals;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.model.DataList;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.model.Journal;
import ru.iprbooks.iprbooksmobile.data.model.Loading;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.ui.main.filter.Filter;
import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.EmptyListException;

@HiltViewModel
public class JournalsViewModel extends BaseDisposablesViewModel {

    private final DataApiInterface dataApi;

    private final MutableStateLiveData<DataList<IListItem>> journalsLiveData = new MutableStateLiveData<>();
    private final MutableLiveData<Filter> filterLiveData = new MutableLiveData<>();
    private DataList<IListItem> journals = new DataList<>();


    @Inject
    public JournalsViewModel(DataApiInterface dataApi) {
        this.dataApi = dataApi;
        journals.setData(new ArrayList<>());
    }

    LiveData<StateData<DataList<IListItem>>> getJournalsLiveData() {
        return journalsLiveData;
    }

    LiveData<Filter> getFilter() {
        return filterLiveData;
    }

    void setFilter(Filter filter) {
        filterLiveData.setValue(filter);
    }

    void getJournalsInit() {
        page = 1;
        journals.getData().clear();
        journals.setCurrentPage(page);
        journals.setLastPage(page + 1);
        getJournals();
    }

    void getJournals() {
        StateData<DataList<IListItem>> journalsStateData = journalsLiveData.getValue();
        if (journalsStateData != null) {
            if (journalsStateData.getStatus() == DataStatus.LOADING) {
                return;
            }

            if (journals.getCurrentPage() >= journals.getLastPage()) {
                return;
            }
        }

        disposables.add(dataApi.getJournalsCatalog(page, filterLiveData.getValue().toMap())
                .subscribeOn(Schedulers.io())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                    if (response.getData().getTotal() == 0) {
                        throw new EmptyListException();
                    }
                }).observeOn(Schedulers.computation())
                .map(ResponseData::getData)
                .map(journalDataList -> {
                    List<PublicationListItemVO> journalVOList = new ArrayList<>();
                    for (Journal journal : journalDataList.getData()) {
                        journalVOList.add(new PublicationListItemVO(
                                journal.getId(),
                                journal.getTitle(),
                                String.format(Locale.getDefault(), "%d", journal.getPubYear()),
                                journal.getDescription(),
                                journal.getImage()
                        ));
                    }

                    List<IListItem> newJournalsList = new ArrayList<>(journals.getData());
                    if (newJournalsList.size() > 0) {
                        newJournalsList.remove(newJournalsList.size() - 1);
                    }

                    newJournalsList.addAll(journalVOList);

                    if (journalDataList.getCurrentPage() < journalDataList.getLastPage()) {
                        newJournalsList.add(new Loading());
                    }

                    return new DataList<>(
                            journalDataList.getTotal(),
                            journalDataList.getLastPage(),
                            journalDataList.getPerPage(),
                            journalDataList.getCurrentPage(),
                            newJournalsList
                    );
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> journalsLiveData.setLoading())
                .subscribe(iListItemDataList -> {
                    journals = iListItemDataList;
                    journalsLiveData.setSuccess(iListItemDataList);
                    page++;
                }, journalsLiveData::setError));
    }

}
