package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.app.Application;

import androidx.collection.ArrayMap;
import androidx.lifecycle.LiveData;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.model.Autocomplete;
import ru.iprbooks.iprbooksmobile.data.model.PubType;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;

@HiltViewModel
public class FilterViewModel extends BaseDisposablesViewModel {

    private final MutableStateLiveData<Filter> filterLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<List<PubType>> pubTypesSelectedLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<Integer> compilationsLiveData = new MutableStateLiveData<>();

    private final MutableStateLiveData<List<PubType>> pubTypesStateLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<List<Autocomplete>> pubHouseStateLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<List<Autocomplete>> authorsStateLiveData = new MutableStateLiveData<>();

    private final Application app;
    private final DataApiInterface dataApi;

    @Inject
    public FilterViewModel(Application app, DataApiInterface dataApi) {
        this.app = app;
        this.dataApi = dataApi;
    }

    LiveData<StateData<Filter>> getFilter() {
        return filterLiveData;
    }

    LiveData<StateData<List<PubType>>> getPubTypesSelected() {
        return pubTypesSelectedLiveData;
    }

    LiveData<StateData<Integer>> getCompilations() {
        return compilationsLiveData;
    }

    LiveData<StateData<List<PubType>>> getPubTypes() {
        return pubTypesStateLiveData;
    }

    LiveData<StateData<List<Autocomplete>>> getPubHouse() {
        return pubHouseStateLiveData;
    }

    LiveData<StateData<List<Autocomplete>>> getAuthors() {
        return authorsStateLiveData;
    }

    void autocompletePerform(Filter filter, Filter.AutocompleteField field, String search) {
        Map<String, String> autocompleteParams = new ArrayMap<>();
        autocompleteParams.put(Filter.AUTOCOMPLETE_UGNP, filter.getUgnp());
        autocompleteParams.put(Filter.AUTOCOMPLETE_TYPE, filter.getType());
        autocompleteParams.put(Filter.AUTOCOMPLETE_FIELD, Filter.getField(field));
        autocompleteParams.put(Filter.AUTOCOMPLETE_SEARCH, search);

        disposables.add(dataApi.autocomplete(autocompleteParams)
                .debounce(1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(autocompleteResponseData -> updateLiveData(field,
                        autocompleteResponseData.getData().getData()),
                        throwable -> updateLiveData(field, new ArrayList<>())));
    }

    void loadPubTypes() {
        disposables.add(getPubTypesObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pubTypesStateLiveData::setSuccess));
    }

    void setCompilation(Integer compilationId) {
        compilationsLiveData.setSuccess(compilationId);
    }

    void setPubTypes(List<PubType> list) {
        pubTypesSelectedLiveData.setSuccess(list);
    }

    void setFilter(Filter filter) {
        filterLiveData.setSuccess(filter);
    }

    private Observable<List<PubType>> getPubTypesObservable() {
        return Observable.create(emitter -> {
            // get json string
            StringBuilder json = new StringBuilder();
            InputStream inputStream = app.getAssets().open("PubTypes.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                json.append(line);
            }
            bufferedReader.close();
            inputStream.close();

            // json to list
            Moshi moshi = new Moshi.Builder().build();
            Type type = Types.newParameterizedType(List.class, PubType.class);
            JsonAdapter<List<PubType>> adapter = moshi.adapter(type);
            List<PubType> pubTypes = adapter.fromJson(json.toString());

            if (pubTypes != null) {
                emitter.onNext(pubTypes);
            } else {
                emitter.onError(new Exception());
            }

            emitter.onComplete();
        });
    }

    private void updateLiveData(Filter.AutocompleteField autocompleteField, List<Autocomplete> data) {
        switch (autocompleteField) {
            case PUB_HOUSE:
                pubHouseStateLiveData.setSuccess(data);
                break;
            case AUTHORS:
                authorsStateLiveData.setSuccess(data);
                break;
        }
    }

}
