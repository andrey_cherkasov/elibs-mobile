package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.radaee.pdf.Document;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.local.FileHelper;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.local.dao.BookmarkDao;
import ru.iprbooks.iprbooksmobile.data.local.dao.PublicationDao;
import ru.iprbooks.iprbooksmobile.data.local.dao.QuoteDao;
import ru.iprbooks.iprbooksmobile.data.model.Bookmark;
import ru.iprbooks.iprbooksmobile.data.model.Content;
import ru.iprbooks.iprbooksmobile.data.model.DataList;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.Quote;
import ru.iprbooks.iprbooksmobile.data.model.ResponseApi;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.PdfDecryptStream;

@HiltViewModel
public class PdfViewerViewModel extends BaseDisposablesViewModel {

    private final DataApiInterface dataApi;
    private final PublicationDao publicationDao;
    private final QuoteDao quoteDao;
    private final BookmarkDao bookmarkDao;
    private final FileHelper fileHelper;
    private final SharedPreferencesHelper preferencesHelper;

    private final MutableStateLiveData<Publication> publicationStateLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<Document> documentStateLiveData = new MutableStateLiveData<>();

    private final MutableStateLiveData<List<Content>> contentsLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<List<Bookmark>> bookmarksLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<List<Quote>> quotesLiveData = new MutableStateLiveData<>();

    private final MutableLiveData<Content> clickedContent = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isBlindOpenedLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> isActionBarShownLiveData = new MutableLiveData<>();

    private final BehaviorSubject<Integer> pageSubject = BehaviorSubject.create();

    @Inject
    public PdfViewerViewModel(DataApiInterface dataApi, PublicationDao publicationDao,
                              QuoteDao quoteDao, BookmarkDao bookmarkDao,
                              FileHelper fileHelper, SharedPreferencesHelper preferencesHelper) {
        this.dataApi = dataApi;
        this.publicationDao = publicationDao;
        this.quoteDao = quoteDao;
        this.bookmarkDao = bookmarkDao;
        this.fileHelper = fileHelper;
        this.preferencesHelper = preferencesHelper;

        isBlindOpenedLiveData.setValue(false);
        isActionBarShownLiveData.setValue(null);
    }

    public LiveData<Boolean> isBlindOpened() {
        return isBlindOpenedLiveData;
    }

    public void setBlindOpened(boolean opened) {
        isBlindOpenedLiveData.setValue(opened);
    }

    public LiveData<Boolean> isActionBarShown() {
        return isActionBarShownLiveData;
    }

    public void setActionBarShown(boolean shown) {
        isActionBarShownLiveData.setValue(shown);
    }

    public LiveData<Content> getClickedContent() {
        return clickedContent;
    }

    public void setClickedContent(Content content) {
        clickedContent.setValue(content);
    }

    public LiveData<StateData<List<Content>>> getContents() {
        return contentsLiveData;
    }

    public LiveData<StateData<List<Bookmark>>> getBookmarks() {
        return bookmarksLiveData;
    }

    public LiveData<StateData<List<Quote>>> getQuotes() {
        return quotesLiveData;
    }

    public LiveData<StateData<Publication>> getPublication() {
        return publicationStateLiveData;
    }

    public LiveData<StateData<Document>> getDocument() {
        return documentStateLiveData;
    }


    void loadDocument(long publicationId) {
        String filePath = fileHelper.getFileById(publicationId).getPath();

        // load publication entity
        disposables.add(publicationDao.getPublication(publicationId, preferencesHelper.loadUser().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> documentStateLiveData.setLoading())
                .subscribe(publication -> {
                    publicationStateLiveData.setSuccess(publication);

                    // load publication file
                    disposables.add(getDocumentObservable(filePath)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(documentStateLiveData::setSuccess, documentStateLiveData::setError));

                    // save page
                    Flowable<Integer> pageFlowable = pageSubject.toFlowable(BackpressureStrategy.LATEST);
                    disposables.add(pageFlowable.subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io())
                            .debounce(1, TimeUnit.SECONDS)
                            .subscribe(page -> publicationDao.savePage(publication.getLocalId(), page),
                                    Throwable::printStackTrace));

                    // load contents
                    contentsLiveData.setSuccess(getPublication().getValue()
                            .getData().getContent().getData());

                    // sync & load bookmarks
                    syncBookmarks(publication);

                    // sync & load quotes
                    syncQuotes(publication);
                }));
    }

    private void syncBookmarks(Publication publication) {
        disposables.add(bookmarkDao.getBookmarks(publication.getId(), preferencesHelper.loadUser().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(bookmarksLiveData::setSuccess, Throwable::printStackTrace));

        disposables.add(Single.fromCallable(() -> bookmarkDao.getNeedSyncBookmarks(publication.getId(), preferencesHelper.loadUser().getId()))
                .flatMapObservable(Observable::fromIterable)
                .toFlowable(BackpressureStrategy.BUFFER)
                .parallel(5)
                .runOn(Schedulers.io())
                .map(item -> {
                    try {
                        if (item.getStatus() == 1) {
                            dataApi.deleteBookmark(publication.getId(), item.getExternalId()).execute();
                        } else {
                            dataApi.addBookmark(item.getPublicationId(), item.getPage(), item.getText()).execute();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return true;
                })
                .sequential()
                .toList()
                .subscribeOn(Schedulers.io())
                .flatMap(bookmarks -> dataApi.getBookmarks(publication.getId()))
                .map(ResponseData::getData)
                .map(DataList::getData)
                .flatMapObservable(Observable::fromIterable)
                .map(bookmark -> {
                    bookmark.setPublicationId(publication.getId());
                    bookmark.setUserId(preferencesHelper.loadUser().getId());
                    return bookmark;
                })
                .toList()
                .subscribe(result -> {
                    bookmarkDao.deleteByPublication(publication.getId(), preferencesHelper.loadUser().getId());
                    bookmarkDao.insertAll(result);
                }, Throwable::printStackTrace));
    }

    private void syncQuotes(Publication publication) {
        disposables.add(quoteDao.getQuotes(publication.getId(), preferencesHelper.loadUser().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(quotesLiveData::setSuccess, Throwable::printStackTrace));

        disposables.add(Single.fromCallable(() -> quoteDao.getNeedSyncQuotes(publication.getId(), preferencesHelper.loadUser().getId()))
                .flatMapObservable(Observable::fromIterable)
                .toFlowable(BackpressureStrategy.BUFFER)
                .parallel(5)
                .runOn(Schedulers.io())
                .map(item -> {
                    try {
                        if (item.getStatus() == 1) {
                            dataApi.deleteQuote(publication.getId(), item.getExternalId()).execute();
                        } else {
                            dataApi.addQuote(item.getPublicationId(), item.getPage(), item.getText()).execute();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return true;
                })
                .sequential()
                .toList()
                .subscribeOn(Schedulers.io())
                .flatMap(quotes -> dataApi.getQuotes(publication.getId()))
                .map(ResponseData::getData)
                .map(DataList::getData)
                .flatMapObservable(Observable::fromIterable)
                .map(quote -> {
                    quote.setPublicationId(publication.getId());
                    quote.setUserId(preferencesHelper.loadUser().getId());
                    return quote;
                })
                .toList()
                .subscribe(result -> {
                    quoteDao.deleteByPublication(publication.getId(), preferencesHelper.loadUser().getId());
                    quoteDao.insertAll(result);
                }, Throwable::printStackTrace));
    }

    void savePage(int page) {
        publicationStateLiveData.getValue().getData().setLastPage(page);
        pageSubject.onNext(page);
    }

    private Observable<Document> getDocumentObservable(String filePath) {
        String accessToken = preferencesHelper.loadUser().getAccessToken();
        String key = accessToken.substring(0, 16);

        return Observable.create(emitter -> {
            Document document = new Document();
            PdfDecryptStream stream = new PdfDecryptStream();
            stream.open(filePath, key);
            int ret = document.OpenStream(stream, null);

            switch (ret) {
                case -1:
                    emitter.onError(new Throwable("Need input password"));
                    break;
                case -2:
                    emitter.onError(new Throwable("Unknown encryption"));
                    break;
                case -3:
                    emitter.onError(new Throwable("Damaged or invalid format"));
                    break;
                case -10:
                    emitter.onError(new Throwable("Access denied or invalid file path"));
                    break;
                case 0:
                    emitter.onNext(document);
                    break;
                default:
                    emitter.onError(new Throwable("Unknown error"));
                    break;
            }

            emitter.onComplete();
        });
    }

    public void addBookmark(String text) {
        // create bookmark
        Bookmark bookmark = new Bookmark();
        bookmark.setPublicationId(publicationStateLiveData.getValue().getData().getId());
        bookmark.setPage(publicationStateLiveData.getValue().getData().getLastPage() + 1);
        bookmark.setText(text);
        bookmark.setUserId(preferencesHelper.loadUser().getId());
        bookmark.setStatus((byte) 2);

        disposables.add(Observable.fromCallable(() -> {
            // add bookmark to local DB (ext_id = null)
            long bookmarkId = bookmarkDao.insert(bookmark);

            // add bookmark to server
            ResponseData<Bookmark> response = dataApi.addBookmark(
                    bookmark.getPublicationId(),
                    bookmark.getPage(),
                    bookmark.getText()
            ).execute().body();

            // if success update bookmark ext_id & status in local DB
            if (response.getMeta().isSuccess()) {
                bookmark.setId(bookmarkId);
                bookmark.setExternalId(response.getData().getId());
                bookmark.setStatus((byte) 0);
                bookmarkDao.update(bookmark);
            }

            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                }, Throwable::printStackTrace));
    }

    public void deleteBookmark(Bookmark bookmark) {
        disposables.add(Observable.fromCallable(() -> {
            // delete remote bookmark
            ResponseApi response = dataApi.deleteBookmark(
                    bookmark.getPublicationId(),
                    bookmark.getExternalId()
            ).execute().body();

            if (response.getMeta().isSuccess()) {
                bookmarkDao.deleteByExtId(bookmark.getExternalId());
            }

            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                }, throwable -> {
                    // mark local bookmark deleted & for delete
                    disposables.add(bookmarkDao.setDeleted(bookmark.getExternalId())
                            .subscribeOn(Schedulers.io())
                            .subscribe());
                }));
    }

    boolean isPageNotContainsInBookmarks(int page) {
        for (Bookmark bookmark : getBookmarks().getValue().getData()) {
            if (bookmark.getPage() == page) {
                return false;
            }
        }

        return true;
    }

    public void addQuote(String text) {
        // create quote
        Quote quote = new Quote();
        quote.setPublicationId(publicationStateLiveData.getValue().getData().getId());
        quote.setPage(publicationStateLiveData.getValue().getData().getLastPage() + 1);
        quote.setText(text);
        quote.setUserId(preferencesHelper.loadUser().getId());
        quote.setStatus((byte) 2);

        disposables.add(Observable.fromCallable(() -> {
            // add quote to local DB (ext_id = null)
            long quoteId = quoteDao.insert(quote);
            quote.setId(quoteId);

            // add quote to server
            ResponseData<Quote> response = dataApi.addQuote(
                    quote.getPublicationId(),
                    quote.getPage(),
                    quote.getText()
            ).execute().body();

            // if success update quote ext_id in local DB
            if (response.getMeta().isSuccess()) {
                quote.setId(quoteId);
                quote.setExternalId(response.getData().getId());
                quote.setStatus((byte) 0);
                quoteDao.update(quote);
            }

            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                }, Throwable::printStackTrace));
    }

    public void editQuote(Quote quote, String text) {
        String date = new SimpleDateFormat("dd.MM.yyyy hh:mm", Locale.getDefault()).format(new Date());
        disposables.add(Observable.fromCallable(() -> {
            // save quote local changes(text, date)
            quoteDao.update(quote.getId(), text, date);

            // update server quote
            dataApi.updateQuote(quote.getExternalId(), text).execute();
            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                }, Throwable::printStackTrace));
    }

    public void deleteQuote(Quote quote) {
        disposables.add(Observable.fromCallable(() -> {
            // delete remote quote
            ResponseApi response = dataApi.deleteQuote(
                    quote.getPublicationId(),
                    quote.getExternalId()
            ).execute().body();

            if (response.getMeta().isSuccess()) {
                quoteDao.deleteByExtId(quote.getExternalId());
            }

            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(aBoolean -> {
                }, throwable -> {
                    // mark local quote deleted & for delete
                    disposables.add(quoteDao.setDeleted(quote.getExternalId())
                            .subscribeOn(Schedulers.io())
                            .subscribe());
                }));
    }

}
