package ru.iprbooks.iprbooksmobile.ui.main.help;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.radiobutton.MaterialRadioButton;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.FragmentHelpBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;

@AndroidEntryPoint
public class HelpFragment extends BaseFragment implements LoadingView<String> {

    private FragmentHelpBinding binding;
    private HelpViewModel viewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentHelpBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(HelpViewModel.class);

        String[] helpTypes = getResources().getStringArray(R.array.help_types);
        for (int i = 0; i < helpTypes.length; i++) {
            MaterialRadioButton radioButton = new MaterialRadioButton(requireContext());
            radioButton.setId(i);
            radioButton.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT, 1));
            radioButton.setText(helpTypes[i]);
            radioButton.setUseMaterialThemeColors(true);
            binding.typeRadioGroup.addView(radioButton);
        }

        viewModel.stateLiveData.observe(requireActivity(), stringStateData -> {
            switch (stringStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    showMessage(stringStateData.getData());
                    binding.typeRadioGroup.check(0);
                    binding.requestText.setText("");
                    break;
                case ERROR:
                    showLoading(false);
                    stringStateData.getError().printStackTrace();
                    showMessage(stringStateData.getError().getMessage());
                    break;
            }
        });

        binding.typeRadioGroup.check(0);
        binding.ok.setOnClickListener(v -> viewModel.sendHelpRequest(
                helpTypes[binding.typeRadioGroup.getCheckedRadioButtonId()],
                binding.requestText.getText().toString())
        );
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.getRoot().setVisibility(loading ? View.VISIBLE : View.GONE);
        binding.ok.setEnabled(!loading);
    }

    @Override
    public void setData(String data) {
    }

}
