package ru.iprbooks.iprbooksmobile.ui.base.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import java.util.List;


public class BasePagingAdapter<T> extends ListAdapter<T, BaseViewHolder> {

    private final List<BaseDelegateAdapter> delegateAdapters;

    public BasePagingAdapter(@NonNull DiffUtil.ItemCallback<T> diffCallback, List<BaseDelegateAdapter> delegateAdapters) {
        super(diffCallback);
        this.delegateAdapters = delegateAdapters;
    }

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(getCurrentList().get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        for (int i = 0; i < delegateAdapters.size(); i++) {
            if (delegateAdapters.get(i).isForViewType(getCurrentList(), position)) {
                return i;
            }
        }

        throw new NullPointerException("Can not get viewType for position " + position);
    }

    @Override
    public int getItemCount() {
        return getCurrentList().size();
    }

}
