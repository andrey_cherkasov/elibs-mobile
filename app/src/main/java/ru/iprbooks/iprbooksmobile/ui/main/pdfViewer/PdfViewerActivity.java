package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;

import com.radaee.pdf.Document;
import com.radaee.pdf.Global;
import com.radaee.pdf.Page;
import com.radaee.view.ILayoutView;

import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.databinding.ActivityPdfViewerBinding;
import ru.iprbooks.iprbooksmobile.databinding.DialogEdittextBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.bookmarks.BookmarksFragment;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.contents.ContentsFragment;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.ContentType;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.quotes.QuotesFragment;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.settings.SettingsActivity;
import ru.iprbooks.iprbooksmobile.ui.view.FastScrollView;

@AndroidEntryPoint
public class PdfViewerActivity extends BaseActivity implements PdfViewerView, LoadingView<Document> {

    public static int mLicenseType = 3;
    public static String mCompany = "IPRMEDIA";
    public static String mEmail = "ctomatolor@gmail.com";
    public static String mKey = "45C0KZ-1TH02H-HBN204-WJU7SV-VI7LEB-YAXDX0";

    private ActivityPdfViewerBinding binding;
    private PdfViewerViewModel viewModel;
    private Document document;
    private ActionBar actionBar;
    private String lastSearch;
    private String selectedText;

    private ContentsFragment contentsFragment;
    private BookmarksFragment bookmarksFragment;
    private QuotesFragment quotesFragment;

    private MenuItem addBookmarkMenuItem;

    @Inject
    SharedPreferencesHelper preferencesHelper;

    private long publicationId;

    private final ActivityResultLauncher<Intent> settingsActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    applySettings();
                    viewModel.loadDocument(publicationId);
                }
            });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Global.Init(this, mLicenseType, mCompany, mEmail, mKey);
        applySettings();

        binding = ActivityPdfViewerBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(PdfViewerViewModel.class);
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appToolbar.toolbar);
        actionBar = getSupportActionBar();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            publicationId = extras.getLong(MainActivity.PUBLICATION_ID, 0);
        }

        contentsFragment = new ContentsFragment();
        bookmarksFragment = new BookmarksFragment();
        quotesFragment = new QuotesFragment();


        viewModel.getPublication().observe(this, publicationStateData -> {
            if (publicationStateData.getStatus().equals(DataStatus.SUCCESS)) {
                if (actionBar != null) {
                    actionBar.setTitle(publicationStateData.getData().getTitle());
                    actionBar.setDisplayHomeAsUpEnabled(true);
                }
            }
        });

        viewModel.getDocument().observe(this, documentStateData -> {
            if (documentStateData.getStatus().equals(DataStatus.SUCCESS)) {
                setData(documentStateData.getData());
            }
        });

        binding.pdfThumb.setOnHideShowListener(new FastScrollView.OnHideShowListener() {
            @Override
            public void onShow() {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1f);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        binding.pagesCount.setVisibility(View.VISIBLE);
                        binding.pagesCount.setAlpha(1);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                alphaAnimation.setDuration(600);
                binding.pagesCount.startAnimation(alphaAnimation);
            }

            @Override
            public void onHide() {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1f, 0);
                alphaAnimation.setFillAfter(true);
                alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        binding.pagesCount.setAlpha(0);
                        binding.pagesCount.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
                alphaAnimation.setDuration(600);
                binding.pagesCount.startAnimation(alphaAnimation);
            }
        });
        binding.pdfThumb.setOnScrollListener(scrollY -> {
            int diffHeight = binding.reader.getHeight() - binding.pdfThumb.getHeight();
            binding.reader.PDFGotoPage(scrollY * document.GetPageCount() / diffHeight);
        });

        binding.bottomNavigation.setOnItemSelectedListener(item -> {
            int id = item.getItemId();
            if (id == R.id.nav_contents) {
                changeFragment(contentsFragment);
            } else if (id == R.id.nav_bookmarks) {
                changeFragment(bookmarksFragment);
            } else if (id == R.id.nav_quotes) {
                changeFragment(quotesFragment);
            }
            return true;
        });

        binding.blind.setVisibility(viewModel.isBlindOpened().getValue() ? View.VISIBLE : View.GONE);

        binding.addQuote.setOnClickListener(view -> {
            viewModel.addQuote(selectedText);
            hideActions();
            showMessage(getString(R.string.quote_added));
        });
        binding.copy.setOnClickListener(view -> copyToClipBoard());
        binding.shareSelected.setOnClickListener(view -> shareSelected());
        binding.actionsClose.setOnClickListener(view -> hideActions());

        binding.searchClose.setOnClickListener(view -> hideSearch());
        binding.searchClear.setOnClickListener(view -> binding.searchText.setText(""));
        binding.search.setOnClickListener(view -> find(binding.searchText.getText().toString()));
        binding.searchText.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_NEXT) {
                find(binding.searchText.getText().toString());
                return true;
            }
            return false;
        });

        viewModel.getBookmarks().observe(this, bookmarks -> {
            if (addBookmarkMenuItem != null && viewModel.getDocument() != null && viewModel.getPublication() != null) {
                addBookmarkMenuItem.setVisible(viewModel.isPageNotContainsInBookmarks(viewModel.getPublication()
                        .getValue().getData().getLastPage() + 1));
            }
        });

        viewModel.getClickedContent().observe(this, content -> {
            binding.reader.PDFGotoPage(content.getPage() - 1);
            hideBlind();
            hideActionBar();
        });

        if (viewModel.getDocument().getValue() == null) {
            viewModel.loadDocument(publicationId);
        }
    }

    @Override
    public void onBackPressed() {
        if (viewModel.isBlindOpened().getValue()) {
            hideBlind();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pdfviewer, menu);
        addBookmarkMenuItem = menu.findItem(R.id.action_add_bookmark);
        addBookmarkMenuItem.setVisible(false);
        MenuItem item = menu.findItem(R.id.action_pin);
        if (binding.reader.isPinned()) {
            item.setTitle(R.string.unpin);
        } else {
            item.setTitle(R.string.pin);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            if (viewModel.isBlindOpened().getValue()) {
                hideBlind();
            } else {
                finish();
            }
        } else if (itemId == R.id.action_search) {
            showSearch();
        } else if (itemId == R.id.action_settings) {
            openPdfViewerSettingsScreen();
        } else if (itemId == R.id.action_pin) {
            if (binding.reader.isPinned()) {
                item.setTitle(R.string.pin);
            } else {
                item.setTitle(R.string.unpin);
            }
            binding.reader.setPinned(!binding.reader.isPinned());
        } else if (itemId == R.id.action_add_bookmark) {
            showAddBookmarkDialog();
        } else if (itemId == R.id.action_contents) {
            showBlind(ContentType.CONTENT);
        } else if (itemId == R.id.action_bookmarks) {
            showBlind(ContentType.BOOKMARK);
        } else if (itemId == R.id.action_quotes) {
            showBlind(ContentType.QUOTE);
        }
        return true;
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.PUBLICATION_ID, publicationId);
        setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.loadingLayout.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(Document data) {
        document = data;
        binding.reader.PDFOpen(data, new ILayoutView.PDFLayoutListener() {

            @Override
            public void OnPDFPageModified(int pageno) {

            }

            @Override
            public void OnPDFPageChanged(int pageno) {
                String text = String.format(Locale.getDefault(),
                        getString(R.string.current_of_total_page),
                        pageno + 1, data.GetPageCount());
                binding.pagesCount.setText(text);

                if (addBookmarkMenuItem != null && viewModel.getBookmarks().getValue() != null) {
                    addBookmarkMenuItem.setVisible(viewModel.isPageNotContainsInBookmarks(pageno + 1));
                }

                viewModel.savePage(pageno);

                int diffHeight = binding.reader.getHeight() - binding.pdfThumb.getHeight();
                binding.pdfThumb.scrollThumb((float) diffHeight * pageno / (document.GetPageCount() - 1));
            }

            @Override
            public void OnPDFAnnotTapped(int pno, Page.Annotation annot) {

            }

            @Override
            public void OnPDFBlankTapped(int pagebo) {
                if (actionBar != null) {
                    if (binding.appToolbar.appBarLayout.getVisibility() == View.VISIBLE) {
                        hideActionBar();
                    } else {
                        showActionBar();
                    }
                }
            }

            @Override
            public void OnPDFSelectEnd(String text) {
                if (text != null) {
                    if (text.length() > 1000) {
                        selectedText = text.substring(0, 999);
                    } else {
                        selectedText = text;
                    }
                }
            }

            @Override
            public void OnPDFOpenURI(String uri) {

            }

            @Override
            public void OnPDFOpenJS(String js) {

            }

            @Override
            public void OnPDFOpenMovie(String path) {

            }

            @Override
            public void OnPDFOpenSound(int[] paras, String path) {

            }

            @Override
            public void OnPDFOpenAttachment(String path) {

            }

            @Override
            public void OnPDFOpen3D(String path) {

            }

            @Override
            public void OnPDFZoomStart() {

            }

            @Override
            public void OnPDFZoomEnd() {

            }

            @Override
            public boolean OnPDFDoubleTapped(int pagebo, float x, float y) {
                return false;
            }

            @Override
            public void OnPDFLongPressed(int pagebo, float x, float y) {
                showActions();
            }

            @Override
            public void OnPDFSearchFinished(boolean found) {

            }

            @Override
            public void OnPDFPageDisplayed(Canvas canvas, ILayoutView.IVPage vpage) {

            }

            @Override
            public void OnPDFPageRendered(ILayoutView.IVPage vpage) {

            }
        });

        binding.reader.PDFGotoPage(viewModel.getPublication().getValue().getData().getLastPage());
        if (viewModel.isActionBarShown().getValue() == null) {
            hideActionBar();
        } else {
            binding.appToolbar.appBarLayout.setVisibility(viewModel.isActionBarShown().getValue() ?
                    View.VISIBLE : View.GONE);
        }

        binding.pdfThumb.setDisplayHeight(binding.reader.getHeight());
        binding.actionsCard.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        binding.searchCard.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
    }

    @Override
    public void find(String text) {
        if (text != null) {
            if (!text.equals(lastSearch)) {
                binding.reader.PDFFindEnd();
                binding.reader.PDFFindStart(text, false, false);
                binding.reader.PDFFind(1);
                lastSearch = text;
            } else {
                binding.reader.PDFFind(1);
            }
        }
    }

    @Override
    public void showActionBar() {
        viewModel.setActionBarShown(true);
        binding.appToolbar.appBarLayout.setVisibility(View.VISIBLE);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 1,
                -binding.appToolbar.appBarLayout.getMeasuredHeight(), 0);
        translateAnimation.setDuration(200);
        binding.appToolbar.appBarLayout.startAnimation(translateAnimation);
    }

    @Override
    public void hideActionBar() {
        viewModel.setActionBarShown(false);
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 1,
                0, -binding.appToolbar.appBarLayout.getMeasuredHeight());
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.appToolbar.appBarLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        translateAnimation.setDuration(200);
        binding.appToolbar.appBarLayout.startAnimation(translateAnimation);
    }

    @Override
    public void showActions() {
        if (actionBar.isShowing()) {
            hideActionBar();
        }
        binding.actionsCard.setVisibility(View.VISIBLE);
        binding.reader.PDFSetSelect();

        TranslateAnimation translateAnimation = new TranslateAnimation(0, 1,
                -binding.actionsCard.getMeasuredHeight(), 0);
        translateAnimation.setDuration(200);
        binding.actionsCard.startAnimation(translateAnimation);
    }

    @Override
    public void hideActions() {
        binding.reader.PDFSetSelect();

        TranslateAnimation translateAnimation = new TranslateAnimation(0, 1,
                0, -binding.actionsCard.getMeasuredHeight());
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.actionsCard.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        translateAnimation.setDuration(200);
        binding.actionsCard.startAnimation(translateAnimation);
    }

    @Override
    public void showSearch() {
        if (actionBar.isShowing()) {
            hideActionBar();
        }
        binding.searchCard.setVisibility(View.VISIBLE);

        TranslateAnimation translateAnimation = new TranslateAnimation(0, 1,
                -binding.searchCard.getMeasuredHeight(), 0);
        translateAnimation.setDuration(200);
        binding.searchCard.startAnimation(translateAnimation);
    }

    @Override
    public void hideSearch() {
        TranslateAnimation translateAnimation = new TranslateAnimation(0, 1,
                0, -binding.searchCard.getMeasuredHeight());
        translateAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                binding.searchText.setText("");
                binding.reader.PDFFindEnd();
                binding.searchCard.setVisibility(View.GONE);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(binding.searchText.getWindowToken(), 0);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        translateAnimation.setDuration(200);
        binding.searchCard.startAnimation(translateAnimation);
    }

    @Override
    public void copyToClipBoard() {
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(selectedText, selectedText);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, R.string.text_copy_to_clipboard, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void shareSelected() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        intent.putExtra(Intent.EXTRA_TEXT, selectedText);
        startActivity(Intent.createChooser(intent, "Share"));
    }

    @Override
    public void openPdfViewerSettingsScreen() {
        Intent intent = new Intent(this, SettingsActivity.class);
        settingsActivityResultLauncher.launch(intent);
    }

    @Override
    public void applySettings() {
        Global.debug_mode = false;
        Global.g_dark_mode = preferencesHelper.isNightMode();
        Global.g_view_mode = SharedPreferencesHelper.viewModeMapping(preferencesHelper.getViewMode());

        if (!preferencesHelper.isSystemBrightness()) {
            float brightness = preferencesHelper.getBrightness() / 100f;
            WindowManager.LayoutParams lp = getWindow().getAttributes();
            lp.screenBrightness = brightness;
            getWindow().setAttributes(lp);
        }

        if (preferencesHelper.isAlwaysOn()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                    | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        }
    }

    @Override
    public void showBlind(ContentType type) {
        viewModel.setBlindOpened(true);
        switch (type) {
            case CONTENT:
                binding.bottomNavigation.setSelectedItemId(R.id.nav_contents);
                break;
            case BOOKMARK:
                binding.bottomNavigation.setSelectedItemId(R.id.nav_bookmarks);
                break;
            case QUOTE:
                binding.bottomNavigation.setSelectedItemId(R.id.nav_quotes);
                break;
        }

        binding.blind.setVisibility(View.VISIBLE);
    }

    private void changeFragment(BaseFragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.contents_nav_host_fragment, fragment)
                .commit();
    }

    @Override
    public void hideBlind() {
        binding.blind.setVisibility(View.GONE);
        viewModel.setBlindOpened(false);
    }

    @Override
    public void showAddBookmarkDialog() {
        DialogEdittextBinding dialogEdittextBinding = DialogEdittextBinding.inflate(LayoutInflater.from(this));
        dialogEdittextBinding.inputLayout.setHint(R.string.name);
        dialogEdittextBinding.editText.setText(String.format(Locale.getDefault(),
                getString(R.string.new_bookmark), viewModel.getBookmarks().getValue().getData().size() + 1));

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setView(dialogEdittextBinding.getRoot())
                .setTitle(R.string.want_to_add_bookmark)
                .setPositiveButton(R.string.ok, (dialogInterface, i) ->
                        viewModel.addBookmark(dialogEdittextBinding.editText.getText().toString()))
                .setNegativeButton(R.string.cancel, null)
                .create();

        alertDialog.show();
    }

}
