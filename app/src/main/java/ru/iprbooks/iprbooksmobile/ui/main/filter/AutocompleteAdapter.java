package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.Autocomplete;

public class AutocompleteAdapter extends BaseAdapter implements Filterable {

    private final List<Autocomplete> results = new ArrayList<>();

    void setData(List<Autocomplete> list) {
        results.clear();
        results.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return results.size();
    }

    @Override
    public String getItem(int i) {
        return results.get(i).getItem();
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
            view = inflater.inflate(android.R.layout.simple_dropdown_item_1line, viewGroup, false);
        }

        ((TextView) view.findViewById(android.R.id.text1)).setText(getItem(i));
        return view;
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    filterResults.values = results;
                    filterResults.count = 10;
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults res) {
                if (res != null && res.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };
    }

}
