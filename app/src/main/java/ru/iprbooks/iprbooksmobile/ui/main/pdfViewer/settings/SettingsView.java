package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.settings;

public interface SettingsView {

    void setNightMode(Boolean mode);

    void setSystemBrightness(Boolean brightness);

    void showDialogModes();

}
