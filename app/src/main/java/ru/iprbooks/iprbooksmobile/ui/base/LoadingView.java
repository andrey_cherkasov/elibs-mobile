package ru.iprbooks.iprbooksmobile.ui.base;

public interface LoadingView<T> {

    void showLoading(boolean loading);

    void setData(T data);

}
