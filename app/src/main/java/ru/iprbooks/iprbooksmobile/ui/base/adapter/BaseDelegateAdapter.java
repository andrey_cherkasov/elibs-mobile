package ru.iprbooks.iprbooksmobile.ui.base.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;

import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.IListItem;

public interface BaseDelegateAdapter {

    @NonNull
    BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType);

    void onBindViewHolder(@NonNull BaseViewHolder holder, @NonNull List<IListItem> items, int position);

    boolean isForViewType(@NonNull List<?> items, int position);

}
