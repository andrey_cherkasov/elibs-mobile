package ru.iprbooks.iprbooksmobile.ui.base.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;

import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.model.Loading;
import ru.iprbooks.iprbooksmobile.databinding.ItemLoadingBinding;

public class LoadingDelegateAdapter implements BaseDelegateAdapter {

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LoadingViewHolder(ItemLoadingBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, @NonNull List<IListItem> items, int position) {
        holder.onBind(items.get(position), position);
    }

    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return items.get(position) instanceof Loading;
    }


    static class LoadingViewHolder extends BaseViewHolder {

        ItemLoadingBinding binding;

        public LoadingViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemLoadingBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
        }

    }

}
