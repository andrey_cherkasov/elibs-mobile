package ru.iprbooks.iprbooksmobile.ui.main.publicationDetail;

import androidx.lifecycle.LiveData;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import java.io.File;
import java.util.UUID;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.local.FileHelper;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.local.dao.PublicationDao;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.Rating;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.data.remote.service.DownloadWorkManager;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@HiltViewModel
public class PublicationDetailViewModel extends BaseDisposablesViewModel {

    private final DataApiInterface dataApi;
    private final SharedPreferencesHelper preferencesHelper;
    private final PublicationDao publicationDao;
    private final WorkManager workManager;
    private final FileHelper fileHelper;

    private final MutableStateLiveData<Publication> publicationLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<Rating> rateLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<Boolean> favoriteLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<UUID> downloadLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<Integer> deleteLiveData = new MutableStateLiveData<>();


    @Inject
    PublicationDetailViewModel(WorkManager workManager, DataApiInterface dataApi, PublicationDao publicationDao,
                               SharedPreferencesHelper preferencesHelper, FileHelper fileHelper) {
        this.workManager = workManager;
        this.dataApi = dataApi;
        this.publicationDao = publicationDao;
        this.preferencesHelper = preferencesHelper;
        this.fileHelper = fileHelper;
    }

    LiveData<StateData<Publication>> getPublicationLiveData() {
        return publicationLiveData;
    }

    LiveData<StateData<Rating>> getRateLiveData() {
        return rateLiveData;
    }

    LiveData<StateData<Boolean>> getFavoriteLiveData() {
        return favoriteLiveData;
    }

    LiveData<StateData<UUID>> getDownloadLiveData() {
        return downloadLiveData;
    }

    LiveData<StateData<Integer>> getDeleteLiveData() {
        return deleteLiveData;
    }

    public void getPublication(long publicationId, int publicationType) {
        Single<ResponseData<Publication>> request;
        if (publicationType == PublicationType.BOOK) {
            request = dataApi.getBook(publicationId);
        } else {
            request = dataApi.getNumber(publicationId);
        }

        disposables.add(publicationDao.getPublication(publicationId, preferencesHelper.loadUser().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> publicationLiveData.setLoading())
                .subscribe(localPublication -> {
                            publicationLiveData.setSuccess(localPublication);

                            // update local publication
                            disposables.add(request.subscribeOn(Schedulers.io())
                                    .observeOn(Schedulers.io())
                                    .subscribe(publicationResponseData -> {
                                        if (publicationResponseData.getMeta().isSuccess()) {
                                            Publication remotePublication = publicationResponseData.getData();
                                            remotePublication.setPublicationType(publicationType);
                                            remotePublication.setDownloaded(true);
                                            remotePublication.setUserId(preferencesHelper.loadUser().getId());
                                            publicationDao.update(remotePublication);
                                        }
                                    }, throwable -> {
                                    }));
                        },
                        localThrowable -> disposables.add(request
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .doOnSuccess(response -> {
                                    if (!response.getMeta().isSuccess()) {
                                        throw new ApiErrorException(response.getMeta().getMessage());
                                    }
                                }).map(ResponseData::getData)
                                .subscribe(publication -> {
                                    publication.setPublicationType(publicationType);
                                    publication.setUserId(preferencesHelper.loadUser().getId());
                                    publicationLiveData.setSuccess(publication);
                                }, publicationLiveData::setError))));
    }

    public void ratePublication(long publicationId, float rating) {
        disposables.add(dataApi.updatePublicationRating(publicationId, (int) rating)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> rateLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                }).map(ResponseData::getData)
                .subscribe(data -> {
                    publicationLiveData.getValue().getData().setUserRating(data.getUserRating());
                    rateLiveData.setSuccess(data);
                }, rateLiveData::setError));
    }

    public void performFavorite() {
        long publicationId = publicationLiveData.getValue().getData().getId();
        if (publicationLiveData.getValue().getData().isFavorite()) {
            deleteFromFavorites(publicationId);
        } else {
            addToFavorites(publicationId);
        }
    }

    private void addToFavorites(long publicationId) {
        disposables.add(dataApi.addPublicationToFavorites(publicationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> favoriteLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                }).subscribe(data -> {
                    publicationLiveData.getValue().getData().setFavorite(true);
                    favoriteLiveData.setSuccess(true);
                    setLocalFavorite(publicationId, true);
                }, favoriteLiveData::setError));
    }

    private void deleteFromFavorites(long publicationId) {
        disposables.add(dataApi.deletePublicationFromFavorites(publicationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> favoriteLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                }).subscribe(data -> {
                    publicationLiveData.getValue().getData().setFavorite(false);
                    favoriteLiveData.setSuccess(false);
                    setLocalFavorite(publicationId, false);
                }, favoriteLiveData::setError));
    }

    public void loadPublicationOnDevice(int publicationType) {
        Publication publication = publicationLiveData.getValue().getData();
        publication.setDownloaded(false);
        publication.setDownloading(true);
        disposables.add(publicationDao.insert(publication)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(id -> {
                    Data downloadData = new Data.Builder()
                            .putLong(DownloadWorkManager.PUBLICATION_ID, publication.getId())
                            .build();

                    OneTimeWorkRequest downloadRequest = new OneTimeWorkRequest.Builder(DownloadWorkManager.class)
                            .setInputData(downloadData)
                            .build();

                    downloadLiveData.setSuccess(downloadRequest.getId());
                    workManager.enqueue(downloadRequest);
                }));
    }

    public void cancelPublicationDownloading(long publicationId) {
        workManager.cancelAllWork();
        disposables.add(publicationDao.delete(publicationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe());
    }

    public void deletePublicationFromDevice(long publicationId) {
        File file = fileHelper.getFileById(publicationId);
        disposables.add(publicationDao.delete(publicationId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(id -> {
                    if (file.exists() && file.delete()) {
                        publicationLiveData.getValue().getData().setDownloaded(false);
                        publicationLiveData.getValue().getData().setDownloading(false);
                        deleteLiveData.setSuccess(id);
                    } else {
                        deleteLiveData.setError(new Throwable());
                    }
                }, deleteLiveData::setError));
    }

    private void setLocalFavorite(long publicationId, boolean favorite) {
        disposables.add(publicationDao.getPublication(publicationId, preferencesHelper.loadUser().getId())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(localPublication -> {
                    localPublication.setFavorite(favorite);
                    publicationDao.update(localPublication);
                }, Throwable::printStackTrace));
    }

}
