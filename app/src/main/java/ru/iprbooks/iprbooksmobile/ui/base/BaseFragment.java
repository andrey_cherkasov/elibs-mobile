package ru.iprbooks.iprbooksmobile.ui.base;

import android.widget.Toast;

import androidx.fragment.app.Fragment;

import ru.iprbooks.iprbooksmobile.R;

public abstract class BaseFragment extends Fragment implements BaseView {

    @Override
    public void showMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showError(Throwable error) {
        String message;
        if (error != null) {
            error.printStackTrace();
            message = error.getMessage();
        } else {
            message = getString(R.string.error);
        }
        showMessage(message);
    }
}
