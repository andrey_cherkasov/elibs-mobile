package ru.iprbooks.iprbooksmobile.ui.main.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.databinding.ActivityLoginBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.register.RegisterActivity;
import ru.iprbooks.iprbooksmobile.ui.main.restore.RestoreActivity;

@AndroidEntryPoint
public class LoginActivity extends BaseActivity implements LoginView, LoadingView<User> {

    private ActivityLoginBinding binding;
    private LoginViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        setContentView(binding.getRoot());
        init();
    }

    private void init() {
        if (BuildConfig.DEBUG) {
            binding.loginEditText.setText("irinai1901@hotmail.com");
            binding.passwordEditText.setText("190089");
        }

        binding.passwordEditText.setTransformationMethod(new PasswordTransformationMethod());

        binding.restoreButton.setOnClickListener(v -> openRestoreScreen());
        binding.signUpButton.setOnClickListener(v -> openRegisterScreen());
        binding.loginButton.setOnClickListener(v -> {
            String phoneNumber = binding.loginEditText.getText().toString();
            String password = binding.passwordEditText.getText().toString();
            viewModel.login(phoneNumber, password);
        });

        viewModel.getUser().observe(this, userStateData -> {
            switch (userStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    openMainScreen();
                    break;
                case ERROR:
                    showLoading(false);
                    showError(userStateData.getError());
                    break;
            }
        });
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.getRoot().setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(User data) {

    }

    @Override
    public void openRestoreScreen() {
        Intent intent = new Intent(this, RestoreActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void openRegisterScreen() {
        Intent intent = new Intent(this, RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void openMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
