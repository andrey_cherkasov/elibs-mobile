package ru.iprbooks.iprbooksmobile.ui.main.login;

import androidx.lifecycle.LiveData;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.data.remote.AuthApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.Utils;

@HiltViewModel
public final class LoginViewModel extends BaseDisposablesViewModel {

    private final MutableStateLiveData<User> userLiveData = new MutableStateLiveData<>();

    private final AuthApiInterface authApi;
    private final SharedPreferencesHelper preferencesHelper;
    private final Utils utils;


    @Inject
    LoginViewModel(AuthApiInterface authApi, SharedPreferencesHelper preferencesHelper, Utils utils) {
        this.authApi = authApi;
        this.preferencesHelper = preferencesHelper;
        this.utils = utils;
    }

    LiveData<StateData<User>> getUser() {
        return userLiveData;
    }

    public void login(String login, String password) {
        disposables.add(authApi.auth(login, utils.getDeviceId(), Utils.hashPassword(password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> userLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                })
                .map(ResponseData::getData)
                .subscribe(user -> {
                    preferencesHelper.saveUser(user);
                    userLiveData.setSuccess(user);
                }, userLiveData::setError));
    }

}
