package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.PubType;
import ru.iprbooks.iprbooksmobile.databinding.ItemPubtypeBinding;
import ru.iprbooks.iprbooksmobile.databinding.ItemPubtypeHeadBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;

public class PubTypesAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private final static int PUB_TYPE_HEAD = 1;
    private final static int PUB_TYPE = 2;

    private OnListViewItemClickListener<PubType> listener;
    private final List<PubType> items = new ArrayList<>();


    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == PUB_TYPE_HEAD) {
            return new PubTypeHeadViewHolder(ItemPubtypeHeadBinding.inflate(
                    LayoutInflater.from(parent.getContext()), parent, false));
        }

        return new PubTypeViewHolder(ItemPubtypeBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(items.get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position).getParentId() == null) {
            return PUB_TYPE_HEAD;
        }
        return PUB_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    void setItems(List<PubType> newItems) {
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    public List<PubType> getItems() {
        return items;
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<PubType> listener) {
        this.listener = listener;
    }


    class PubTypeHeadViewHolder extends BaseViewHolder {

        ItemPubtypeHeadBinding binding;

        public PubTypeHeadViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemPubtypeHeadBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            PubType pubType = (PubType) item;
            binding.title.setText(pubType.getTitle());
            binding.checkbox.setChecked(pubType.getChecked());
            binding.getRoot().setOnClickListener(v -> {
                int i = position + 1;
                while (i < items.size() && items.get(i).getParentId() != null) {
                    items.get(i).setChecked(!pubType.getChecked());
                    i++;
                }
                pubType.setChecked(!pubType.getChecked());
                notifyItemRangeChanged(position, i - position);
                if (listener != null) {
                    listener.onItemClick(binding.getRoot(), pubType, position);
                }
            });
        }
    }

    class PubTypeViewHolder extends BaseViewHolder {

        ItemPubtypeBinding binding;

        public PubTypeViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemPubtypeBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            PubType pubType = (PubType) item;
            binding.title.setText(pubType.getTitle());
            binding.checkbox.setChecked(pubType.getChecked());
            binding.getRoot().setOnClickListener(v -> {
                pubType.setChecked(!pubType.getChecked());
                notifyItemChanged(position);
                if (listener != null) {
                    listener.onItemClick(binding.getRoot(), pubType, position);
                }
            });
        }
    }

}
