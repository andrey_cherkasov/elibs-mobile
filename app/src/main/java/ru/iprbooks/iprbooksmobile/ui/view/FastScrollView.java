package ru.iprbooks.iprbooksmobile.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;

public class FastScrollView extends LinearLayout {

    private final static int ANIM_APPEARANCE_TIME = 300;
    private final static int TIME = 3000;

    private OnScrollListener listener;
    private int startY;
    private boolean isScroll;
    private ViewGroup.MarginLayoutParams params;
    private int displayHeight;
    private Handler handler;
    private boolean isShown = true;
    private OnHideShowListener onHideShowListener;
    private final Runnable runnable = this::hide;


    public FastScrollView(Context context) {
        super(context);
        if (!isInEditMode()) {
            initUi();
        }
    }

    public FastScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            initUi();
        }
    }

    public void setOnHideShowListener(OnHideShowListener listener) {
        onHideShowListener = listener;
    }

    private void initUi() {
        handler = new Handler();
    }

    public void scrollThumb(final float diffY) {
        handler.removeCallbacks(runnable);
        if (!isShown) {
            show();
        }

        if (!isScroll) {
            if (params == null) {
                initThumb();
            }
            scrollBy(0, diffY);
            handler.postDelayed(runnable, TIME);
        }
    }

    private void initThumb() {
        params = (ViewGroup.MarginLayoutParams) getLayoutParams();
    }

    private void scrollBy(final float diffY) {
        handler.removeCallbacks(runnable);
        scrollBy(params.topMargin, diffY);
        setLayoutParams(params);
        if (listener != null) {
            listener.onScroll(params.topMargin);
        }
    }

    private void scrollBy(float start, float diff) {
        handler.removeCallbacks(runnable);
        params.topMargin = (int) (start + diff);
        if (params.topMargin < 0) {
            params.topMargin = 0;
        }
        if (params.topMargin + getHeight() >= displayHeight && displayHeight > 0) {
            params.topMargin = displayHeight - getHeight();
        }
        setLayoutParams(params);
    }

    public void setOnScrollListener(OnScrollListener listener) {
        this.listener = listener;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                isScroll = true;
                if (params == null) {
                    initThumb();
                }
                startY = (int) event.getRawY();
                return true;
            case MotionEvent.ACTION_MOVE:
                int moveY = (int) event.getRawY();
                int diffY = moveY - startY;
                startY = moveY;
                scrollBy(diffY);
                return true;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                handler.postDelayed(runnable, TIME);
                isScroll = false;
                return false;
            default:
                return false;
        }
    }


    public void setDisplayHeight(int displayHeight) {
        this.displayHeight = displayHeight;
    }

    public void hide() {
        if (onHideShowListener != null) {
            onHideShowListener.onHide();
        }

        isShown = false;
        TranslateAnimation animation = new TranslateAnimation(0, getWidth(), 0, 0);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animation.setFillAfter(true);
        animation.setDuration(ANIM_APPEARANCE_TIME);
        startAnimation(animation);
    }

    public void show() {
        if (onHideShowListener != null) {
            onHideShowListener.onShow();
        }

        isShown = true;
        TranslateAnimation animation = new TranslateAnimation(getWidth(), 0, 0, 0);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        animation.setFillEnabled(true);
        animation.setDuration(ANIM_APPEARANCE_TIME);
        startAnimation(animation);
    }


    public interface OnHideShowListener {

        void onShow();

        void onHide();

    }

    public interface OnScrollListener {
        void onScroll(int scrollY);
    }

}

