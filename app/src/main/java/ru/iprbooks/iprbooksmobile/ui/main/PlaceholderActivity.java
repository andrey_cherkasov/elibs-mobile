package ru.iprbooks.iprbooksmobile.ui.main;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.databinding.ActivityPlaceholderBinding;
import ru.iprbooks.iprbooksmobile.ui.main.login.LoginActivity;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;

@AndroidEntryPoint
public class PlaceholderActivity extends AppCompatActivity implements PlaceholderView {

    @Inject
    SharedPreferencesHelper preferencesHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityPlaceholderBinding binding = ActivityPlaceholderBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        init();
    }

    protected void init() {
        User user = preferencesHelper.loadUser();
        if (user != null && user.getId() > 0) {
            openMainScreen();
        } else {
            openLoginScreen();
        }
    }

    @Override
    public void openMainScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void openLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
