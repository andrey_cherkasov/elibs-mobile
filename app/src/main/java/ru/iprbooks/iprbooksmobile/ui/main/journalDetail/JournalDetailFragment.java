package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Locale;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.Journal;
import ru.iprbooks.iprbooksmobile.databinding.FragmentJournalDetailBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;

public class JournalDetailFragment extends BaseFragment implements JournalDetailView, LoadingView<Journal> {

    private FragmentJournalDetailBinding binding;
    private JournalDetailViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentJournalDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(JournalDetailViewModel.class);
        long journalId = requireArguments().getLong(MainActivity.PUBLICATION_ID, 0);
        viewModel.getJournal().observe(getViewLifecycleOwner(), journalStateData -> {
            switch (journalStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    setData(journalStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    showError(journalStateData.getError());
                    break;
            }
        });

        viewModel.setSubTitle("");
        binding.numbers.setOnClickListener(v -> openJournalNumbersScreen());

        if (viewModel.getJournal().getValue() == null) {
            viewModel.getJournal(journalId);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.loadingLayout.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(Journal journal) {
        viewModel.setTitle(journal.getTitle());
        Glide.with(this)
                .load(journal.getBigImage())
                .placeholder(R.drawable.default_book_cover)
                .error(R.drawable.default_book_cover)
                .into(binding.bookCover);
        binding.title.setText(journal.getTitle());
        binding.description.setText(journal.getDescription());
        binding.pubHouse.setText(journal.getPubHouse());
        binding.place.setText(journal.getPlace());
        binding.isbn.setText(journal.getIssn());
        binding.vak.setText(journal.getInVak() == null || journal.getInVak().isEmpty()
                ? getString(R.string.no) : journal.getInVak());
        binding.year.setText(String.format(Locale.getDefault(), "%d", journal.getPubYear()));

        binding.layout.setVisibility(View.VISIBLE);
    }

    @Override
    public void openJournalNumbersScreen() {
        Bundle arguments = new Bundle();
        arguments.putParcelableArrayList(JournalDetailActivity.NUMBERS_BY_YEARS,
                (ArrayList<? extends Parcelable>) viewModel.getJournal().getValue().getData().getYears());
        Navigation.findNavController(binding.getRoot())
                .navigate(R.id.action_nav_journal_detail_to_nav_journal_numbers, arguments);
        viewModel.setSubTitle(getString(R.string.numbers_list_text));
    }

}
