package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.quotes;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.ListAdapter;
import androidx.viewbinding.ViewBinding;

import java.util.Locale;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.Quote;
import ru.iprbooks.iprbooksmobile.databinding.ItemBlindBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;

public class QuotesAdapter extends ListAdapter<Quote, QuotesAdapter.QuoteViewHolder> {

    private OnListViewItemClickListener<Quote> listener;
    private OnListViewItemClickListener<Quote> deleteListener;
    private OnListViewItemClickListener<Quote> editListener;

    protected QuotesAdapter(@NonNull AsyncDifferConfig<Quote> config) {
        super(config);
    }

    @NonNull
    @Override
    public QuoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new QuoteViewHolder(ItemBlindBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull QuoteViewHolder holder, int position) {
        holder.onBind(getItem(position), position);
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<Quote> listener) {
        this.listener = listener;
    }

    public void setDeleteListener(OnListViewItemClickListener<Quote> listener) {
        this.deleteListener = listener;
    }

    public void setEditListener(OnListViewItemClickListener<Quote> listener) {
        this.editListener = listener;
    }

    class QuoteViewHolder extends BaseViewHolder {

        ItemBlindBinding binding;

        public QuoteViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemBlindBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            Quote quote = (Quote) item;
            binding.text.setText(quote.getText());
            binding.page.setText(String.format(Locale.getDefault(),
                    binding.getRoot().getContext().getString(R.string.item_blind_page),
                    quote.getPage()));

            binding.getRoot().setOnClickListener(v -> {
                if (listener != null) {
                    int pos = getAbsoluteAdapterPosition();
                    listener.onItemClick(v, getCurrentList().get(pos), pos);
                }
            });

            binding.getRoot().setOnLongClickListener(v -> {
                if (editListener != null) {
                    int pos = getAbsoluteAdapterPosition();
                    editListener.onItemClick(v, getCurrentList().get(pos), pos);
                }
                return true;
            });

            binding.delete.setOnClickListener(v -> {
                if (deleteListener != null) {
                    int pos = getAbsoluteAdapterPosition();
                    deleteListener.onItemClick(v, getCurrentList().get(pos), pos);
                }
            });
        }
    }

}
