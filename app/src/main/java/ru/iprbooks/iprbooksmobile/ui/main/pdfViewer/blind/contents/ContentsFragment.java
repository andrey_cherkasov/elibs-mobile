package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.contents;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.data.model.Content;
import ru.iprbooks.iprbooksmobile.databinding.FragmentBlindBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.PdfViewerViewModel;

@AndroidEntryPoint
public class ContentsFragment extends BaseFragment implements LoadingView<List<Content>> {

    private FragmentBlindBinding binding;
    private ContentsAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentBlindBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PdfViewerViewModel viewModel = new ViewModelProvider(requireActivity()).get(PdfViewerViewModel.class);

        adapter = new ContentsAdapter();
        adapter.setOnListViewItemClickListener((v, item, position) -> viewModel.setClickedContent(item));
        binding.list.setAdapter(adapter);

        viewModel.getContents().observe(getViewLifecycleOwner(), listStateData -> {
            switch (listStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    setData(listStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    break;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.loadingLayout.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(List<Content> data) {
        adapter.addItems(data);
    }

}
