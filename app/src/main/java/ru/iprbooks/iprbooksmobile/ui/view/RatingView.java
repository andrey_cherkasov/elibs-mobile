package ru.iprbooks.iprbooksmobile.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.RatingLayoutBinding;

public class RatingView extends ConstraintLayout {

    private OnRatingClickListener listener;
    private RatingLayoutBinding binding;
    private float rating;
    private boolean editable = false;
    private final List<ImageView> stars = new ArrayList<>(5);

    public RatingView(Context context) {
        super(context);
        initUi();
    }

    public RatingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RatingView, 0, 0);
        try {
            editable = typedArray.getBoolean(R.styleable.RatingView_editable, false);
        } finally {
            typedArray.recycle();
        }
        initUi();
    }

    public RatingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUi();
    }

    void initUi() {
        binding = RatingLayoutBinding.inflate(LayoutInflater.from(getContext()));

        stars.add(binding.star1);
        stars.add(binding.star2);
        stars.add(binding.star3);
        stars.add(binding.star4);
        stars.add(binding.star5);

        if (editable) {
            for (ImageView star : stars) {
                star.setOnClickListener(view -> {
                    for (int i = 0; i < stars.size(); i++) {
                        if (i < stars.indexOf(star) + 1) {
                            stars.get(i).setImageResource(R.drawable.ic_star);
                        } else {
                            stars.get(i).setImageResource(R.drawable.ic_star_outline);
                        }
                    }

                    rating = stars.indexOf(star) + 1;
                    binding.rating.setText(String.format(Locale.getDefault(), "(%.2f)", rating));
                });
            }
        } else {
            binding.getRoot().setOnClickListener(v -> {
                if (listener != null) {
                    listener.onRatingClick();
                }
            });
        }

        addView(binding.getRoot());
    }

    public void setOnRatingClickListener(OnRatingClickListener listener) {
        this.listener = listener;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        if (rating > 5) {
            return;
        }

        this.rating = rating;
        float fractionalRatingPart = rating % 1;
        binding.rating.setText(String.format(Locale.getDefault(), "(%.2f)", rating));

        for (int i = 0; i < stars.size(); i++) {
            if (i < (int) rating) {
                stars.get(i).setImageResource(R.drawable.ic_star);
            } else {
                stars.get(i).setImageResource(R.drawable.ic_star_outline);
            }
        }

        if (fractionalRatingPart > 0) {
            if (0 < fractionalRatingPart && fractionalRatingPart <= 0.25f) {
                stars.get((int) rating).setImageResource(R.drawable.ic_star_outline_25);
            } else if (0.25 < fractionalRatingPart && fractionalRatingPart <= 0.5) {
                stars.get((int) rating).setImageResource(R.drawable.ic_star_outline_50);
            } else if (0.5 < fractionalRatingPart && fractionalRatingPart <= 0.75) {
                stars.get((int) rating).setImageResource(R.drawable.ic_star_outline_75);
            } else if (0.75 < fractionalRatingPart && fractionalRatingPart < 1) {
                stars.get((int) rating).setImageResource(R.drawable.ic_star);
            }
        }
    }

    public interface OnRatingClickListener {
        void onRatingClick();
    }

}
