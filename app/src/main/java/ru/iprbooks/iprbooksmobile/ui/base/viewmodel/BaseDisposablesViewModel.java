package ru.iprbooks.iprbooksmobile.ui.base.viewmodel;

import androidx.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;

public class BaseDisposablesViewModel extends ViewModel {

    protected CompositeDisposable disposables = new CompositeDisposable();
    protected int page = 1;

    @Override
    protected void onCleared() {
        super.onCleared();
        disposables.clear();
    }

}
