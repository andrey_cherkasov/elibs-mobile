package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.settings;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;

import com.radaee.pdf.Global;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.databinding.ActivityPdfViewerSettingsBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;

@AndroidEntryPoint
public class SettingsActivity extends BaseActivity implements SettingsView, View.OnClickListener {

    @Inject
    SharedPreferencesHelper preferencesHelper;
    private ActivityPdfViewerSettingsBinding binding;
    private String[] pagingModes;
    private int viewMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityPdfViewerSettingsBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appToolbar.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.reading_settings);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        pagingModes = getResources().getStringArray(R.array.paging_modes);

        setNightMode(preferencesHelper.isNightMode());
        viewMode = preferencesHelper.getViewMode();

        binding.viewModeSubtext.setText(pagingModes[viewMode]);
        binding.brightnessControl.setProgress(preferencesHelper.getBrightness());
        binding.alwaysOnControl.setChecked(preferencesHelper.isAlwaysOn());
        binding.systemBrightnessControl.setChecked(preferencesHelper.isSystemBrightness());
        if (preferencesHelper.isSystemBrightness()) {
            binding.brightnessControl.setEnabled(false);
        }
        binding.brightnessControl.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!binding.systemBrightnessControl.isChecked()) {
                    onBrightnessChange(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        binding.systemBrightnessControl.setOnCheckedChangeListener((compoundButton, b) -> setSystemBrightness(b));
        binding.nightModeControl.setOnCheckedChangeListener((compoundButton, b) -> setNightMode(b));
        binding.alwaysOnControl.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            } else {
                this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
                        | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            }
        });
        if (!preferencesHelper.isSystemBrightness()) {
            onBrightnessChange(preferencesHelper.getBrightness());
        }

        binding.nightModeLayout.setOnClickListener(this);
        binding.viewModeLayout.setOnClickListener(this);
        binding.systemBrightnessLayout.setOnClickListener(this);
        binding.alwaysOnLayout.setOnClickListener(this);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void finish() {
        setResult(RESULT_OK);
        preferencesHelper.setNightMode(binding.nightModeControl.isChecked());
        preferencesHelper.setViewMode(viewMode);
        preferencesHelper.setBrightness(binding.brightnessControl.getProgress());
        preferencesHelper.setSystemBrightness(binding.systemBrightnessControl.isChecked());
        preferencesHelper.setAlwaysOn(binding.alwaysOnControl.isChecked());
        super.finish();
    }

    @Override
    public void onClick(View view) {
        int viewId = view.getId();

        if (viewId == R.id.night_mode_layout) {
            binding.nightModeControl.setChecked(!binding.nightModeControl.isChecked());
        } else if (viewId == R.id.view_mode_layout) {
            showDialogModes();
        } else if (viewId == R.id.system_brightness_layout) {
            setSystemBrightness(!binding.systemBrightnessControl.isChecked());
        } else if (viewId == R.id.always_on_layout) {
            binding.alwaysOnControl.setChecked(!binding.alwaysOnControl.isChecked());
        }
    }

    @Override
    public void setNightMode(Boolean mode) {
        if (mode) {
            binding.nightModeSubtext.setText(R.string.on);
        } else {
            binding.nightModeSubtext.setText(R.string.off);
        }
        binding.nightModeControl.setChecked(mode);
        Global.g_dark_mode = mode;
    }

    @Override
    public void setSystemBrightness(Boolean brightness) {
        binding.brightnessControl.setEnabled(!brightness);
        if (brightness) {
            onBrightnessChange(-1);
        } else {
            onBrightnessChange(binding.brightnessControl.getProgress());
        }

        binding.systemBrightnessControl.setChecked(brightness);
    }

    @Override
    public void showDialogModes() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.view_mode_text);
        builder.setSingleChoiceItems(pagingModes, viewMode, (dialogInterface, i) -> {
            viewMode = i;
            binding.viewModeSubtext.setText(pagingModes[i]);
            Global.g_view_mode = SharedPreferencesHelper.viewModeMapping(i);
            dialogInterface.dismiss();
        });
        builder.setNegativeButton(R.string.cancel, null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    void onBrightnessChange(int value) {
        float brightness = value / (float) 100;
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.screenBrightness = brightness;
        getWindow().setAttributes(lp);
    }

}
