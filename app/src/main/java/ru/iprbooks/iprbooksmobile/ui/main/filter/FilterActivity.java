package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.HashSet;
import java.util.Set;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.ActivityFilterBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;

@AndroidEntryPoint
public class FilterActivity extends BaseActivity {

    private AppBarConfiguration appBarConfiguration;

    private FilterViewModel viewModel;
    private Filter filter;

    private final Set<Integer> topLevelDest = new HashSet<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityFilterBinding binding = ActivityFilterBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(FilterViewModel.class);
        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBarMain.toolbar);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            filter = extras.getParcelable(Filter.TAG);
        }

        init();
    }

    private void init() {
        appBarConfiguration = new AppBarConfiguration.Builder(topLevelDest).build();

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container_view);

        if (navHostFragment != null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable(Filter.TAG, filter);
            NavController navController = navHostFragment.getNavController();
            navController.setGraph(R.navigation.filter_navigation, arguments);
            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        }

        viewModel.getFilter().observe(this, filterStateData -> {
            Intent intent = new Intent();
            intent.putExtra(Filter.TAG, filterStateData.getData());
            setResult(RESULT_OK, intent);
            finish();
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.fragment_container_view);
        if (!NavigationUI.navigateUp(navController, appBarConfiguration)) {
            finish();
        }
        return super.onSupportNavigateUp();
    }

}
