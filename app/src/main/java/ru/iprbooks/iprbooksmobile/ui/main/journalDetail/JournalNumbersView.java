package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import ru.iprbooks.iprbooksmobile.data.model.Publication;

public interface JournalNumbersView {

    void openNumberDetailScreen(Publication number);

}
