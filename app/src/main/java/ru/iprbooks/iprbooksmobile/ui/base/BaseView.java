package ru.iprbooks.iprbooksmobile.ui.base;

public interface BaseView {

    void showMessage(String message);

    void showError(Throwable error);

}
