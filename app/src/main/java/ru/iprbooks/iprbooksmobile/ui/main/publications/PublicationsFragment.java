package ru.iprbooks.iprbooksmobile.ui.main.publications;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.NoConnectivityException;
import ru.iprbooks.iprbooksmobile.databinding.FragmentPublicationsBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BasePagingAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.LoadingDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.PaginationListener;
import ru.iprbooks.iprbooksmobile.ui.main.downloaded.DownloadedFragment;
import ru.iprbooks.iprbooksmobile.ui.main.favorites.FavoritesFragment;
import ru.iprbooks.iprbooksmobile.ui.main.filter.Filter;
import ru.iprbooks.iprbooksmobile.ui.main.filter.FilterActivity;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.publicationDetail.PublicationDetailActivity;
import ru.iprbooks.iprbooksmobile.ui.main.search.SearchActivity;
import ru.iprbooks.iprbooksmobile.ui.main.ugnp.UgnpFragment;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.EmptyListException;
import ru.iprbooks.iprbooksmobile.util.PublicationType;


@AndroidEntryPoint
public class PublicationsFragment extends BaseFragment implements PublicationsView {

    private static final String TAG = "BooksFragment";

    private FragmentPublicationsBinding binding;
    private PublicationsViewModel viewModel;
    private MenuItem clearFilerMenuItem;
    private BasePagingAdapter<IListItem> adapter;
    private int type;
    private boolean favorite;
    private boolean downloaded;
    private boolean simpleUi;


    private final ActivityResultLauncher<Intent> filterActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (data != null) {
                        viewModel.setFilter(data.getParcelableExtra(Filter.TAG));
                        if (!viewModel.getFilter().getValue().isEmpty()) {
                            clearFilerMenuItem.setVisible(true);
                            updateList();
                        }
                    }
                }
            });

    private final ActivityResultLauncher<Intent> removeItemActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (data != null) {
                        if (data.getBooleanExtra(MainActivity.NEED_REMOVE, false)) {
                            viewModel.removePublication(data.getLongExtra(MainActivity.PUBLICATION_ID, 0));
                        }
                    }
                }
            });

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(PublicationsViewModel.class);

        PublicationsDelegateAdapter publicationsDelegateAdapter = new PublicationsDelegateAdapter();
        publicationsDelegateAdapter.setOnListViewItemClickListener((v, item, position) ->
                openPublicationDetailScreen(item));

        List<BaseDelegateAdapter> delegateAdapters = new ArrayList<>();
        delegateAdapters.add(new LoadingDelegateAdapter());
        delegateAdapters.add(publicationsDelegateAdapter);

        adapter = new BasePagingAdapter<>(new DiffUtil.ItemCallback<IListItem>() {
            @Override
            public boolean areItemsTheSame(@NonNull IListItem oldItem, @NonNull IListItem newItem) {
                if (oldItem instanceof PublicationListItemVO && newItem instanceof PublicationListItemVO) {
                    return ((PublicationListItemVO) oldItem).getId() == ((PublicationListItemVO) newItem).getId();
                }
                return false;
            }

            @Override
            public boolean areContentsTheSame(@NonNull IListItem oldItem, @NonNull IListItem newItem) {
                return true;
            }
        }, delegateAdapters);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPublicationsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        // init params
        Bundle arguments = getArguments();
        String ugnp = "00";
        if (arguments != null) {
            type = arguments.getInt(MainActivity.PUBLICATION_TYPE, PublicationType.BOOK);
            downloaded = arguments.getBoolean(MainActivity.IS_DOWNLOADED, false);
            favorite = arguments.getBoolean(MainActivity.IS_FAVORITE, false);
            ugnp = arguments.getString(UgnpFragment.UGNP_ID, "00");
        }
        simpleUi = downloaded || favorite;

        // init ui
        binding.notFoundLayout.notFoundText.setText(simpleUi ? R.string.empty_list : R.string.empty_result);
        binding.notFoundLayout.openCatalog.setVisibility(simpleUi ? View.VISIBLE : View.GONE);
        binding.notFoundLayout.openCatalog.setOnClickListener(v -> {
            if (type == PublicationType.BOOK) {
                if (favorite) {
                    navigate(R.id.action_nav_favorites_to_nav_ugnp_books);
                } else if (downloaded) {
                    navigate(R.id.action_nav_downloaded_to_nav_ugnp_books);
                }
            } else {
                if (favorite) {
                    navigate(R.id.action_nav_favorites_to_nav_ugnp_journals);
                } else if (downloaded) {
                    navigate(R.id.action_nav_downloaded_to_nav_ugnp_journals);
                }
            }
        });
        binding.filter.setVisibility(simpleUi ? View.GONE : View.VISIBLE);
        binding.filter.setOnClickListener(v -> showFilter());

        binding.list.setAdapter(adapter);
        binding.list.addOnScrollListener(new PaginationListener((LinearLayoutManager) binding.list.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                if (favorite) {
                    viewModel.getFavoritesPublications(type);
                } else if (downloaded) {
                    viewModel.getDownloadedPublications(type);
                } else {
                    viewModel.getPublications();
                }
            }
        });

        binding.noInternetLayout.refresh.setOnClickListener(v -> updateList());

        // observer
        viewModel.getPublicationsLiveData().observe(getViewLifecycleOwner(), listStateData -> {
            DataStatus status = listStateData.getStatus();
            if (status.equals(DataStatus.SUCCESS)) {
                adapter.submitList(new ArrayList<>(listStateData.getData().getData()));
                binding.loadingLayout.getRoot().setVisibility(View.GONE);
            } else if (status.equals(DataStatus.ERROR)) {
                Throwable throwable = listStateData.getError();
                if (throwable != null) {
                    if (throwable instanceof EmptyListException) {
                        binding.notFoundLayout.getRoot().setVisibility(View.VISIBLE);
                    } else if (throwable instanceof ApiErrorException) {
                        showMessage(throwable.getMessage());
                    } else if (throwable instanceof NoConnectivityException) {
                        binding.noInternetLayout.getRoot().setVisibility(View.VISIBLE);
                        binding.filter.setVisibility(View.GONE);
                    } else {
                        showMessage(getString(R.string.error));
                        throwable.printStackTrace();
                    }
                }
            }
        });

        if (viewModel.getPublicationsLiveData().getValue() == null) {
            viewModel.setFilter(new Filter(ugnp, Filter.TYPE_BOOK));
            updateList();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        if (!simpleUi) {
            inflater.inflate(R.menu.activity_filter, menu);
            clearFilerMenuItem = menu.findItem(R.id.action_clear_filter);
            clearFilerMenuItem.setVisible(false);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == R.id.action_clear_filter) {
            clearFilter();
        } else if (menuId == R.id.action_search) {
            openSearchScreen();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void navigate(int action) {
        Bundle args = new Bundle();
        args.putSerializable(UgnpFragment.UGNP_TYPE, type);
        Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
                .navigate(action, args);
    }

    @Override
    public void updateList() {
        adapter.submitList(null);
        binding.notFoundLayout.getRoot().setVisibility(View.GONE);
        binding.noInternetLayout.getRoot().setVisibility(View.GONE);
        binding.loadingLayout.getRoot().setVisibility(View.VISIBLE);
        binding.filter.setVisibility(simpleUi ? View.GONE : View.VISIBLE);

        if (favorite) {
            viewModel.getFavoritesPublicationsInit(type);
        } else if (downloaded) {
            viewModel.getDownloadedPublicationsInit(type);
        } else {
            viewModel.getPublicationsInit();
        }
    }

    @Override
    public void openPublicationDetailScreen(PublicationListItemVO publication) {
        Intent intent = new Intent(getContext(), PublicationDetailActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_ID, publication.getId());
        intent.putExtra(MainActivity.PUBLICATION_TYPE, type);
        if (favorite) {
            intent.putExtra(MainActivity.PUBLICATION_OPENED_FROM, FavoritesFragment.TAG);
        } else if (downloaded) {
            intent.putExtra(MainActivity.PUBLICATION_OPENED_FROM, DownloadedFragment.TAG);
        } else {
            intent.putExtra(MainActivity.PUBLICATION_OPENED_FROM, PublicationsFragment.TAG);
        }

        if (simpleUi) {
            removeItemActivityResultLauncher.launch(intent);
        } else {
            startActivity(intent);
        }
    }

    @Override
    public void openSearchScreen() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_TYPE, type);
        startActivity(intent);
    }

    @Override
    public void showFilter() {
        Intent intent = new Intent(getContext(), FilterActivity.class);
        intent.putExtra(Filter.TAG, viewModel.getFilter().getValue());
        filterActivityResultLauncher.launch(intent);
    }

    @Override
    public void clearFilter() {
        clearFilerMenuItem.setVisible(false);
        viewModel.getFilter().getValue().clear();
        updateList();
    }

}
