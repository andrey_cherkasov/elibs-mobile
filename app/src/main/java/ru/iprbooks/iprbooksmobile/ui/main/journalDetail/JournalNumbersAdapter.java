package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.databinding.ItemJournalNumberBinding;
import ru.iprbooks.iprbooksmobile.databinding.ItemJournalYearBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;

public class JournalNumbersAdapter extends RecyclerView.Adapter<BaseViewHolder> {

    private final static int TYPE_YEAR = 1;
    private final static int TYPE_NUMBER = 2;

    private final List<JournalNumber> items = new ArrayList<>();
    private OnListViewItemClickListener<Publication> listener;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_YEAR) {
            return new YearViewHolder(ItemJournalYearBinding.inflate(
                    LayoutInflater.from(parent.getContext()), parent, false));
        } else if (viewType == TYPE_NUMBER) {
            return new NumberViewHolder(ItemJournalNumberBinding.inflate(
                    LayoutInflater.from(parent.getContext()), parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, int position) {
        holder.onBind(items.get(position), position);
    }

    @Override
    public int getItemViewType(int position) {
        JournalNumber item = items.get(position);
        if (item.getYear() != null && item.getYear().length() > 0) {
            return TYPE_YEAR;
        } else if (item.getNumber() != null) {
            return TYPE_NUMBER;
        }
        return 0;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    void addItems(List<JournalNumber> newList) {
        items.clear();
        items.addAll(newList);
        notifyDataSetChanged();
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<Publication> listener) {
        this.listener = listener;
    }

    static class YearViewHolder extends BaseViewHolder {

        ItemJournalYearBinding binding;

        public YearViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemJournalYearBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            binding.year.setText(String.format(Locale.getDefault(),
                    binding.getRoot().getContext().getString(R.string.year_format),
                    ((JournalNumber) item).getYear()));
        }
    }

    class NumberViewHolder extends BaseViewHolder {

        ItemJournalNumberBinding binding;

        public NumberViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemJournalNumberBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            JournalNumber journalNumber = (JournalNumber) item;
            binding.number.setText(journalNumber.getNumber().getNumber());
            binding.getRoot().setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(binding.getRoot(), journalNumber.getNumber(), position);
                }
            });
        }
    }

}
