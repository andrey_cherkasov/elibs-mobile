package ru.iprbooks.iprbooksmobile.ui.main.journals;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;

import com.bumptech.glide.Glide;

import java.util.List;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.databinding.ItemPublicationBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;
import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;

public class JournalsDelegateAdapter implements BaseDelegateAdapter {

    private OnListViewItemClickListener<PublicationListItemVO> listener;

    @NonNull
    @Override
    public BaseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new JournalsViewHolder(ItemPublicationBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BaseViewHolder holder, @NonNull List<IListItem> items, int position) {
        holder.onBind(items.get(position), position);
    }


    @Override
    public boolean isForViewType(@NonNull List<?> items, int position) {
        return items.get(position) instanceof PublicationListItemVO;
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<PublicationListItemVO> listener) {
        this.listener = listener;
    }

    class JournalsViewHolder extends BaseViewHolder {

        ItemPublicationBinding binding;

        public JournalsViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemPublicationBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            PublicationListItemVO journal = (PublicationListItemVO) item;
            binding.title.setText(journal.getTitle());
            binding.subtitle.setText(journal.getSubtitle());
            binding.description.setText(journal.getDescription());
            Glide.with(binding.image.getContext())
                    .load(journal.getImage())
                    .placeholder(R.drawable.default_book_cover)
                    .error(R.drawable.default_book_cover)
                    .into(binding.image);

            binding.getRoot().setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemClick(v, journal, position);
                }
            });
        }
    }

}
