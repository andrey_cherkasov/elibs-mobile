package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.JournalYear;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.databinding.FragmentJournalNumbersBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.publicationDetail.PublicationDetailActivity;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

public class JournalNumbersFragment extends BaseFragment implements LoadingView<List<JournalNumber>>, JournalNumbersView {

    public final static String TAG = "JournalNumbersFragment";

    private FragmentJournalNumbersBinding binding;
    private JournalNumbersAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentJournalNumbersBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        JournalDetailViewModel viewModel = new ViewModelProvider(requireActivity()).get(JournalDetailViewModel.class);
        adapter = new JournalNumbersAdapter();
        binding.list.setAdapter(adapter);
        List<JournalYear> yearsList = requireArguments()
                .getParcelableArrayList(JournalDetailActivity.NUMBERS_BY_YEARS);

        adapter.setOnListViewItemClickListener((v, item, position) -> openNumberDetailScreen(item));

        viewModel.getJournalNumber().observe(getViewLifecycleOwner(), journalStateData -> {
            switch (journalStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    setData(journalStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    showError(journalStateData.getError());
                    break;
            }
        });

        viewModel.setSubTitle(getString(R.string.numbers_list_text));
        if (viewModel.getJournalNumber().getValue() == null) {
            viewModel.getNumberByYears(yearsList);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.loadingLayout.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(List<JournalNumber> data) {
        adapter.addItems(data);
    }

    @Override
    public void openNumberDetailScreen(Publication number) {
        Intent intent = new Intent(getContext(), PublicationDetailActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_ID, number.getId());
        intent.putExtra(MainActivity.PUBLICATION_TYPE, PublicationType.JOURNAL);
        intent.putExtra(MainActivity.PUBLICATION_OPENED_FROM, TAG);
        startActivity(intent);
    }

}
