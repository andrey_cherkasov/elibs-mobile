package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.SimpleItemAnimator;

import ru.iprbooks.iprbooksmobile.databinding.FragmentPubTypesFilterBinding;

public class PubTypesFilterFragment extends Fragment {

    private FragmentPubTypesFilterBinding binding;
    private FilterViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPubTypesFilterBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(FilterViewModel.class);

        Filter filter = requireArguments().getParcelable(Filter.TAG);

        PubTypesAdapter adapter = new PubTypesAdapter();
        adapter.setOnListViewItemClickListener((v, item, position) ->
                viewModel.setPubTypes(adapter.getItems()));
        binding.list.setAdapter(adapter);
        ((SimpleItemAnimator) binding.list.getItemAnimator()).setSupportsChangeAnimations(false);

        viewModel.getPubTypes().observe(getViewLifecycleOwner(), listStateData ->
                adapter.setItems(listStateData.getData()));

        if (filter != null && filter.getPubTypes().size() > 0) {
            viewModel.setPubTypes(filter.getPubTypes());
        } else {
            viewModel.loadPubTypes();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
