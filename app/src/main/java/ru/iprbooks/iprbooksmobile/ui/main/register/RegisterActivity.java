package ru.iprbooks.iprbooksmobile.ui.main.register;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.ActivityRegisterBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.login.LoginActivity;
import ru.iprbooks.iprbooksmobile.ui.main.login.LoginViewModel;
import ru.iprbooks.iprbooksmobile.util.Utils;

@AndroidEntryPoint
public class RegisterActivity extends BaseActivity implements RegisterView, LoadingView<String> {

    private ActivityRegisterBinding binding;
    private RegisterViewModel viewModel;
    private LoginViewModel loginViewModel;

    private String libLogin;
    private String libPass;
    private String fio;
    private String email;
    private int userType;
    private String userPass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(RegisterViewModel.class);
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        setContentView(binding.getRoot());
        init();
    }

    private void init() {
        // init step label
        binding.stepLabel.setText(String.format(Locale.getDefault(), getString(R.string.step_label), 1));

        if (BuildConfig.DEBUG) {
            binding.loginEditText.setText("sit.udmprof");
            binding.passwordStep1EditText.setText("MNbWMkSfEd");
        }

        // init spinner
        List<String> userTypes = Arrays.asList(getResources().getStringArray(R.array.user_types));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, R.layout.item_spinner, userTypes);
        binding.userTypeSpinner.setAdapter(adapter);
        binding.userTypeSpinner.setText(userTypes.get(0), false);
        userType = 1;

        //listeners
        binding.emailEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                showEmailError(!Utils.isValidEmail(binding.emailEditText.getText().toString()));
            }
        });
        binding.fioEditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                showFioError(binding.fioEditText.getText().toString().length() == 0);
            }
        });
        binding.passwordStep2EditText.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                showPasswordError(binding.passwordStep2EditText.getText().toString().length() < 6);
            }
        });

        binding.goToLoginButton.setOnClickListener(v -> openLoginScreen());
        binding.loginButton.setOnClickListener(v -> {
            libLogin = binding.loginEditText.getText().toString();
            libPass = binding.passwordStep1EditText.getText().toString();
            viewModel.checkLibraryCredentials(libLogin, libPass);
        });
        binding.userTypeSpinner.setOnItemClickListener((parent, view, position, id)
                -> userType = position + 1);
        binding.registerButton.setOnClickListener(v -> {
            fio = binding.fioEditText.getText().toString();
            email = binding.emailEditText.getText().toString();
            userPass = binding.passwordStep2EditText.getText().toString();
            viewModel.registerNewUser(libLogin, libPass, fio, email, userType, userPass);
        });

        viewModel.getFirstStep().observe(this, stateData -> {
            switch (stateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    showNextStep();
                    break;
                case ERROR:
                    showLoading(false);
                    showError(stateData.getError());
                    break;
            }
        });

        viewModel.getSecondStep().observe(this, stateData -> {
            switch (stateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    loginViewModel.login(email, userPass);
                    break;
                case ERROR:
                    showLoading(false);
                    showError(stateData.getError());
                    break;
            }
        });
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.getRoot().setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(String data) {

    }

    @Override
    public void openLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showNextStep() {
        binding.logoImageView.setVisibility(View.GONE);
        binding.loginLayout.setVisibility(View.GONE);
        binding.supportLayout.setVisibility(View.GONE);
        binding.registerStep1Layout.setVisibility(View.GONE);
        binding.registerStep2Layout.setVisibility(View.VISIBLE);
        binding.stepLabel.setText(String.format(Locale.getDefault(), getString(R.string.step_label), 2));
    }

    @Override
    public void showEmailError(boolean show) {
        binding.emailInputLayout.setError(show ? getString(R.string.invalid_email) : "");
    }

    @Override
    public void showFioError(boolean show) {
        binding.fioInputLayout.setError(show ? getString(R.string.fill_the_field) : "");
    }

    @Override
    public void showPasswordError(boolean show) {
        binding.passwordStep2InputLayout.setError(show ? getString(R.string.invalid_pass) : "");
    }

}
