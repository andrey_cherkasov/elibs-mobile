package ru.iprbooks.iprbooksmobile.ui.main.register;

import androidx.lifecycle.LiveData;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.remote.AuthApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.Utils;

@HiltViewModel
public final class RegisterViewModel extends BaseDisposablesViewModel {

    private final MutableStateLiveData<String> firstStepLiveData = new MutableStateLiveData<>();
    private final MutableStateLiveData<String> secondStepLiveData = new MutableStateLiveData<>();

    private final AuthApiInterface authApi;

    @Inject
    RegisterViewModel(AuthApiInterface authApi) {
        this.authApi = authApi;
    }

    LiveData<StateData<String>> getFirstStep() {
        return firstStepLiveData;
    }

    LiveData<StateData<String>> getSecondStep() {
        return secondStepLiveData;
    }

    public void checkLibraryCredentials(String libLogin, String libPass) {
        disposables.add(authApi.checkLibraryCredentials(libLogin, Utils.getMD5(libPass))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> firstStepLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new Exception(response.getMeta().getMessage());
                    }
                }).map(response -> response.getMeta().getMessage())
                .subscribe(firstStepLiveData::setSuccess, firstStepLiveData::setError));
    }

    public void registerNewUser(String libLogin, String libPass, String fio,
                                String email, Integer userType, String userPass) {
        disposables.add(authApi.registerNewUser(libLogin, libPass, fio, email, userType, userPass)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> secondStepLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                }).map(response -> response.getMeta().getMessage())
                .subscribe(secondStepLiveData::setSuccess, secondStepLiveData::setError)
        );
    }

}
