package ru.iprbooks.iprbooksmobile.ui.main.ratingDialog;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import org.jetbrains.annotations.NotNull;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.DialogRatingBinding;

public class RatingDialog extends DialogFragment {

    public final static String TAG = "DialogFragment";
    public final static String RATING = "RATING";

    private DialogRatingBinding binding;
    private OnRatingChangeListener listener;
    private float rating = 0;

    public static RatingDialog newInstance(float rating) {
        RatingDialog dialog = new RatingDialog();
        Bundle args = new Bundle();
        args.putFloat(RATING, rating);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments != null) {
            rating = getArguments().getFloat(RATING, 0);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat(RATING, rating);
    }

    public void setOnRatingChangeListener(OnRatingChangeListener listener) {
        this.listener = listener;
    }

    @NotNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            rating = savedInstanceState.getFloat(RATING, 0);
        }

        binding = DialogRatingBinding.inflate(getLayoutInflater());
        binding.rating.setRating(rating);


        return new AlertDialog.Builder(requireContext())
                .setTitle(R.string.rate_publication)
                .setView(binding.getRoot())
                .setPositiveButton(R.string.ok,
                        (dialog, whichButton) -> {
                            rating = binding.rating.getRating();
                            if (listener != null) {
                                listener.onRatingChange(rating);
                            }
                        }
                )
                .setNegativeButton(R.string.cancel, null)
                .create();
    }


    public interface OnRatingChangeListener {
        void onRatingChange(float rating);
    }

}
