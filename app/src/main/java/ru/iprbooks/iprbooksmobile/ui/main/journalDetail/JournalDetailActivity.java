package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.util.HashSet;
import java.util.Set;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.ActivityJournalDetailBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;

@AndroidEntryPoint
public class JournalDetailActivity extends BaseActivity {

    public final static String NUMBERS_BY_YEARS = "NUMBERS_BY_YEARS";

    private AppBarConfiguration appBarConfiguration;
    private ActionBar actionBar;

    private final Set<Integer> topLevelDest = new HashSet<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityJournalDetailBinding binding = ActivityJournalDetailBinding.inflate(getLayoutInflater());
        JournalDetailViewModel viewModel = new ViewModelProvider(this).get(JournalDetailViewModel.class);
        setContentView(binding.getRoot());

        setSupportActionBar(binding.appBarMain.toolbar);
        actionBar = getSupportActionBar();

        appBarConfiguration = new AppBarConfiguration.Builder(topLevelDest).build();
        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_container_view);

        if (navHostFragment != null) {
            NavController navController = navHostFragment.getNavController();
            navController.setGraph(R.navigation.journal_navigation, getIntent().getExtras());
            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        }

        viewModel.getTitle().observe(this, stringStateData ->
                actionBar.setTitle(stringStateData.getData()));

        viewModel.getSubTitle().observe(this, stringStateData ->
                actionBar.setSubtitle(stringStateData.getData()));
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.fragment_container_view);
        if (!NavigationUI.navigateUp(navController, appBarConfiguration)) {
            finish();
        }
        return super.onSupportNavigateUp();
    }

}
