package ru.iprbooks.iprbooksmobile.ui.main.publications;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.local.dao.PublicationDao;
import ru.iprbooks.iprbooksmobile.data.model.DataList;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.model.Loading;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.ui.main.filter.Filter;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.EmptyListException;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@HiltViewModel
public class PublicationsViewModel extends BaseDisposablesViewModel {

    private final DataApiInterface dataApi;
    private final PublicationDao publicationDao;
    private final SharedPreferencesHelper preferencesHelper;

    private final MutableStateLiveData<DataList<IListItem>> publicationsLiveData = new MutableStateLiveData<>();
    private final MutableLiveData<Filter> filterLiveData = new MutableLiveData<>();
    private DataList<IListItem> publications = new DataList<>();


    @Inject
    PublicationsViewModel(DataApiInterface dataApi, PublicationDao publicationDao,
                          SharedPreferencesHelper preferencesHelper) {
        this.dataApi = dataApi;
        this.publicationDao = publicationDao;
        this.preferencesHelper = preferencesHelper;
        publications.setData(new ArrayList<>());
    }

    LiveData<StateData<DataList<IListItem>>> getPublicationsLiveData() {
        return publicationsLiveData;
    }

    LiveData<Filter> getFilter() {
        return filterLiveData;
    }

    void setFilter(Filter filter) {
        filterLiveData.setValue(filter);
    }

    void getPublicationsInit() {
        page = 1;
        publications.getData().clear();
        publications.setCurrentPage(page);
        publications.setLastPage(page + 1);
        getPublications();
    }

    void getPublications() {
        StateData<DataList<IListItem>> publicationsStateData = publicationsLiveData.getValue();
        if (publicationsStateData != null) {
            if (publicationsStateData.getStatus() == DataStatus.LOADING) {
                return;
            }

            if (publications.getCurrentPage() >= publications.getLastPage()) {
                return;
            }
        }

        disposables.add(dataApi.getBooksCatalog(page, filterLiveData.getValue().toMap())
                .subscribeOn(Schedulers.io())
                .doOnSuccess(this::handleResponse)
                .observeOn(Schedulers.computation())
                .map(ResponseData::getData)
                .map(this::handleResponseData)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> publicationsLiveData.setLoading())
                .subscribe(iListItemDataList -> {
                    publications = iListItemDataList;
                    publicationsLiveData.setSuccess(iListItemDataList);
                    page++;
                }, publicationsLiveData::setError));
    }

    void getFavoritesPublicationsInit(int type) {
        page = 1;
        publications.getData().clear();
        publications.setCurrentPage(page);
        publications.setLastPage(page + 1);
        getFavoritesPublications(type);
    }

    void getFavoritesPublications(int type) {
        StateData<DataList<IListItem>> publicationsStateData = publicationsLiveData.getValue();
        if (publicationsStateData != null) {
            if (publicationsStateData.getStatus() == DataStatus.LOADING) {
                return;
            }

            if (publications.getCurrentPage() >= publications.getLastPage()) {
                return;
            }
        }

        Single<ResponseData<DataList<Publication>>> request;
        if (type == PublicationType.BOOK) {
            request = dataApi.getFavoriteBooksCatalog(page, -1L);
        } else {
            request = dataApi.getFavoriteNumbersCatalog(page, -1L);
        }

        disposables.add(request.subscribeOn(Schedulers.io())
                .doOnSuccess(this::handleResponse)
                .observeOn(Schedulers.computation())
                .map(ResponseData::getData)
                .map(this::handleResponseData)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> publicationsLiveData.setLoading())
                .subscribe(iListItemDataList -> {
                    publications = iListItemDataList;
                    publicationsLiveData.setSuccess(iListItemDataList);
                    page++;
                }, publicationsLiveData::setError));
    }

    void getDownloadedPublicationsInit(int type) {
        page = 1;
        publications.getData().clear();
        publications.setCurrentPage(page);
        publications.setLastPage(page + 1);
        getDownloadedPublications(type);
    }

    void getDownloadedPublications(int type) {
        StateData<DataList<IListItem>> publicationsStateData = publicationsLiveData.getValue();
        if (publicationsStateData != null) {
            if (publicationsStateData.getStatus() == DataStatus.LOADING) {
                return;
            }

            if (publications.getCurrentPage() >= publications.getLastPage()) {
                return;
            }
        }

        disposables.add(publicationDao.getPublications(page, type, preferencesHelper.loadUser().getId())
                .subscribeOn(Schedulers.io())
                .doOnSuccess(downloadedPublications -> {
                    if (page == 1 && downloadedPublications.size() == 0) {
                        throw new EmptyListException();
                    }
                })
                .observeOn(Schedulers.computation())
                .map(downloadedPublications -> {
                    DataList<Publication> publicationDataList = new DataList<>();
                    publicationDataList.setData(downloadedPublications);
                    publicationDataList.setTotal(publicationDao.getPublicationsCount(type,
                            preferencesHelper.loadUser().getId()));
                    publicationDataList.setCurrentPage(page);
                    publicationDataList.setLastPage(publicationDataList.getTotal() / PublicationDao.PER_PAGE + 1);
                    publicationDataList.setPerPage(PublicationDao.PER_PAGE);
                    return publicationDataList;
                })
                .map(this::handleResponseData)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> publicationsLiveData.setLoading())
                .subscribe(iListItemDataList -> {
                    publications = iListItemDataList;
                    publicationsLiveData.setSuccess(iListItemDataList);
                    page++;
                }, publicationsLiveData::setError));
    }

    public void removePublication(long id) {
        for (IListItem item : publications.getData()) {
            if (item instanceof PublicationListItemVO && ((PublicationListItemVO) item).getId() == id) {
                publications.getData().remove(item);
                publicationsLiveData.setSuccess(publications);
                return;
            }
        }
    }

    private void handleResponse(ResponseData<DataList<Publication>> response) throws Exception {
        if (!response.getMeta().isSuccess()) {
            throw new ApiErrorException(response.getMeta().getMessage());
        }
        if (response.getData().getTotal() == 0) {
            throw new EmptyListException();
        }
    }

    private DataList<IListItem> handleResponseData(DataList<Publication> responseData) {
        List<PublicationListItemVO> publicationVOList = new ArrayList<>();
        for (Publication publication : responseData.getData()) {
            publicationVOList.add(new PublicationListItemVO(
                    publication.getId(),
                    publication.getTitle(),
                    String.format(Locale.getDefault(), "%d", publication.getPubYear()),
                    publication.getDescription(),
                    publication.getImage()
            ));
        }

        List<IListItem> newPublicationsList = new ArrayList<>(publications.getData());
        if (newPublicationsList.size() > 0) {
            newPublicationsList.remove(newPublicationsList.size() - 1);
        }

        newPublicationsList.addAll(publicationVOList);

        if (responseData.getCurrentPage() < responseData.getLastPage()) {
            newPublicationsList.add(new Loading());
        }

        publications.getData().addAll(publicationVOList);
        publications.setTotal(responseData.getTotal());
        publications.setCurrentPage(responseData.getCurrentPage());
        publications.setLastPage(responseData.getLastPage());
        publications.setPerPage(responseData.getPerPage());


        return new DataList<>(
                responseData.getTotal(),
                responseData.getLastPage(),
                responseData.getPerPage(),
                responseData.getCurrentPage(),
                newPublicationsList
        );
    }

}
