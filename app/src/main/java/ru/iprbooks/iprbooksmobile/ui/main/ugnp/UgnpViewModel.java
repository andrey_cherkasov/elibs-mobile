package ru.iprbooks.iprbooksmobile.ui.main.ugnp;


import android.app.Application;

import androidx.lifecycle.LiveData;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.model.Ugnp;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;

@HiltViewModel
public class UgnpViewModel extends BaseDisposablesViewModel {

    private final Application app;
    private final MutableStateLiveData<List<Ugnp>> stateLiveData = new MutableStateLiveData<>();

    @Inject
    UgnpViewModel(Application app) {
        this.app = app;
    }

    public LiveData<StateData<List<Ugnp>>> getUgnp() {
        return stateLiveData;
    }

    void loadUgnpList() {
        disposables.add(getUgnpObservable()
                .subscribeOn(Schedulers.io())
                .doOnSubscribe(disposable -> stateLiveData.setLoading())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(stateLiveData::setSuccess, stateLiveData::setError));
    }

    private Observable<List<Ugnp>> getUgnpObservable() {
        return Observable.create(emitter -> {
            // get json string
            StringBuilder json = new StringBuilder();
            InputStream inputStream = app.getAssets().open("ugnp.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                json.append(line);
            }
            bufferedReader.close();
            inputStream.close();

            // json to list
            Moshi moshi = new Moshi.Builder().build();
            Type type = Types.newParameterizedType(List.class, Ugnp.class);
            JsonAdapter<List<Ugnp>> adapter = moshi.adapter(type);
            List<Ugnp> ugnpList = adapter.fromJson(json.toString());

            if (ugnpList != null) {
                emitter.onNext(ugnpList);
            } else {
                emitter.onError(new Exception());
            }

            emitter.onComplete();
        });
    }

}
