package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.bookmarks;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.DiffUtil;

import java.util.ArrayList;
import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.Bookmark;
import ru.iprbooks.iprbooksmobile.databinding.FragmentBlindBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.PdfViewerViewModel;

public class BookmarksFragment extends BaseFragment implements LoadingView<List<Bookmark>> {

    private FragmentBlindBinding binding;
    private BookmarksAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentBlindBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        PdfViewerViewModel viewModel = new ViewModelProvider(requireActivity()).get(PdfViewerViewModel.class);

        DiffUtil.ItemCallback<Bookmark> callback = new DiffUtil.ItemCallback<Bookmark>() {
            @Override
            public boolean areItemsTheSame(@NonNull Bookmark oldItem, @NonNull Bookmark newItem) {
                return oldItem.getId().equals(newItem.getId());
            }

            @Override
            public boolean areContentsTheSame(@NonNull Bookmark oldItem, @NonNull Bookmark newItem) {
                return true;
            }
        };
        AsyncDifferConfig<Bookmark> differConfig = new AsyncDifferConfig.Builder<>(callback).build();

        adapter = new BookmarksAdapter(differConfig);
        adapter.setOnListViewItemClickListener((v, item, position) -> viewModel.setClickedContent(item));
        adapter.setDeleteListener((v, item, position) -> viewModel.deleteBookmark(item));
        binding.list.setAdapter(adapter);

        viewModel.getBookmarks().observe(getViewLifecycleOwner(), listStateData -> {
            switch (listStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    setData(listStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    break;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.loadingLayout.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(List<Bookmark> data) {
        adapter.submitList(new ArrayList<>(data));
    }

}
