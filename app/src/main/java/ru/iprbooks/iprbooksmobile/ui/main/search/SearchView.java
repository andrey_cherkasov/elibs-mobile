package ru.iprbooks.iprbooksmobile.ui.main.search;

public interface SearchView {

    void openDetailScreen(long id);

    void updateList();

}
