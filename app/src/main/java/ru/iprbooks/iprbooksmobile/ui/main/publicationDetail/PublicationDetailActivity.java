package ru.iprbooks.iprbooksmobile.ui.main.publicationDetail;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.ViewModelProvider;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.WorkManager;

import com.bumptech.glide.Glide;

import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.Rating;
import ru.iprbooks.iprbooksmobile.data.remote.service.DownloadWorkManager;
import ru.iprbooks.iprbooksmobile.databinding.ActivityBookDetailBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.downloaded.DownloadedFragment;
import ru.iprbooks.iprbooksmobile.ui.main.favorites.FavoritesFragment;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.PdfViewerActivity;
import ru.iprbooks.iprbooksmobile.ui.main.ratingDialog.RatingDialog;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@AndroidEntryPoint
@RuntimePermissions
public class PublicationDetailActivity extends BaseActivity implements PublicationDetailView, LoadingView<Publication> {

    private ActivityBookDetailBinding binding;
    private PublicationDetailViewModel viewModel;
    private ActionBar actionBar;

    @Inject
    SharedPreferencesHelper preferencesHelper;

    private long publicationId;
    private int publicationType;

    private final BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                int progress = extras.getInt(DownloadWorkManager.PROGRESS);
                double progressBytes = extras.getDouble(DownloadWorkManager.PROGRESS_BYTES);
                binding.progress.setProgress(progress);
                binding.progressBytes.setText(String.format(Locale.getDefault(), "%.2f", progressBytes));
            }
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBookDetailBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(PublicationDetailViewModel.class);
        setContentView(binding.getRoot());
        publicationId = getIntent().getLongExtra(MainActivity.PUBLICATION_ID, 0);
        publicationType = getIntent().getIntExtra(MainActivity.PUBLICATION_TYPE, 0);

        setSupportActionBar(binding.appBarMain.toolbar);
        actionBar = getSupportActionBar();

        binding.download.setOnClickListener(view -> downloadPublication());
        binding.cancel.setOnClickListener(view -> cancelDownloading());
        binding.read.setOnClickListener(view -> readPublication());
        binding.delete.setOnClickListener(view -> deletePublication());
        binding.favoritesButton.setOnClickListener(view -> viewModel.performFavorite());

        viewModel.getPublicationLiveData().observe(this, publicationStateData -> {
            switch (publicationStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    assert publicationStateData.getData() != null;
                    setData(publicationStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    showError(publicationStateData.getError());
                    break;
            }
        });

        viewModel.getRateLiveData().observe(this, ratingStateData -> {
            switch (ratingStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    updateRating(ratingStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    showError(ratingStateData.getError());
                    break;
            }
        });

        viewModel.getFavoriteLiveData().observe(this, favoriteStateData -> {
            switch (favoriteStateData.getStatus()) {
                case LOADING:
                    binding.favoritesButton.setClickable(false);
                    break;
                case SUCCESS:
                    binding.favoritesButton.setClickable(true);
                    updateFavoritesStatus(favoriteStateData.getData());
                    break;
                case ERROR:
                    binding.favoritesButton.setClickable(true);
                    showError(favoriteStateData.getError());
                    break;
            }
        });

        viewModel.getDownloadLiveData().observe(this, uuidStateData -> {
            if (uuidStateData.getStatus().equals(DataStatus.SUCCESS)) {
                WorkManager.getInstance(this)
                        .getWorkInfoByIdLiveData(uuidStateData.getData())
                        .observe(this, workInfo -> {
                            switch (workInfo.getState()) {
                                case SUCCEEDED:
                                    publicationDownloaded();
                                    break;
                                case FAILED:
                                case BLOCKED:
                                    showError(new Throwable(getString(R.string.error)));
                                    cancelDownloading();
                                    break;
                            }
                        });
            }
        });

        viewModel.getDeleteLiveData().observe(this, stateData -> {
            switch (stateData.getStatus()) {
                case SUCCESS:
                    publicationDeleted();
                    break;
                case ERROR:
                    showError(new Throwable(getString(R.string.error)));
                    break;
            }
        });

        if (viewModel.getPublicationLiveData().getValue() == null) {
            viewModel.getPublication(publicationId, publicationType);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        bManager.unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.loadingLayout.setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(Publication publication) {
        actionBar.setTitle(publication.getTitle());
        if (publicationType == PublicationType.JOURNAL) {
            actionBar.setSubtitle(String.format(Locale.getDefault(), "%d, %s",
                    publication.getPubYear(), publication.getNumber()));
        }
        actionBar.setDisplayHomeAsUpEnabled(true);
        binding.favoritesButton.setImageResource(publication.isFavorite() ? R.drawable.ic_heart : R.drawable.ic_heart_outline);

        Glide.with(this)
                .load(publication.getBigImage())
                .placeholder(R.drawable.default_book_cover)
                .error(R.drawable.default_book_cover)
                .into(binding.bookCover);

        binding.title.setText(publication.getTitle());
        binding.description.setText(publication.getDescription());
        binding.pubHouse.setText(publication.getPubHouse());
        binding.number.setText(publication.getNumber());
        binding.author.setText(publication.getFullAuthors());
        binding.place.setText(publication.getPlace());
        binding.isbn.setText(publication.getIsbn());
        binding.year.setText(String.format(Locale.getDefault(), "%d", publication.getPubYear()));
        binding.pages.setText(publication.getPagesCount());
        String size = String.format(Locale.getDefault(), "%.2f Mb", publication.getSize() / Math.pow(1024, 2));
        binding.size.setText(size);
        binding.totalBytes.setText(String.format(Locale.getDefault(), " / %s", size));
        binding.rating.setRating(publication.getRating());
        binding.rating.setOnRatingClickListener(() -> showRatingDialog(publication.getUserRating()));

        if (publicationType == PublicationType.JOURNAL) {
            binding.authorLabel.setVisibility(View.GONE);
            binding.author.setVisibility(View.GONE);
            binding.yearLabel.setVisibility(View.GONE);
            binding.year.setVisibility(View.GONE);
            binding.pagesLabel.setVisibility(View.GONE);
            binding.pages.setVisibility(View.GONE);
            binding.placeLabel.setVisibility(View.GONE);
            binding.place.setVisibility(View.GONE);
        } else {
            binding.numberLabel.setVisibility(View.GONE);
            binding.number.setVisibility(View.GONE);
        }

        if (publication.isDownloaded()) {
            binding.download.setVisibility(View.GONE);
            binding.read.setVisibility(View.VISIBLE);
            binding.delete.setVisibility(View.VISIBLE);
        } else {
            binding.download.setVisibility(View.VISIBLE);
            binding.read.setVisibility(View.GONE);
            binding.delete.setVisibility(View.GONE);
        }

        binding.layout.setVisibility(View.VISIBLE);
    }

    private void registerReceiver() {
        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(DownloadWorkManager.PROGRESS + "_" + publicationId);
        bManager.registerReceiver(broadcastReceiver, intentFilter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void finish() {
        Intent intent = new Intent();
        String openedFrom = getIntent().getStringExtra(MainActivity.PUBLICATION_OPENED_FROM);
        intent.putExtra(MainActivity.PUBLICATION_ID, publicationId);
        intent.putExtra(MainActivity.PUBLICATION_TYPE, publicationType);
        if (openedFrom.equals(FavoritesFragment.TAG)) {
            intent.putExtra(MainActivity.NEED_REMOVE, !viewModel.getPublicationLiveData()
                    .getValue().getData().isFavorite());
        } else if (openedFrom.equals(DownloadedFragment.TAG)) {
            intent.putExtra(MainActivity.NEED_REMOVE, !viewModel.getPublicationLiveData()
                    .getValue().getData().isDownloaded());
        }

        setResult(RESULT_OK, intent);
        super.finish();
    }

    @Override
    @NeedsPermission({Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE})
    public void downloadPublication() {
        binding.download.setEnabled(false);
        binding.cancel.setVisibility(View.VISIBLE);
        viewModel.loadPublicationOnDevice(publicationType);
        binding.progress.setProgress(0);
        binding.progressBytes.setText("0");
        binding.progress.setVisibility(View.VISIBLE);
        binding.progressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void publicationDownloaded() {
        viewModel.getPublicationLiveData().getValue().getData().setDownloaded(true);
        viewModel.getPublicationLiveData().getValue().getData().setDownloading(false);

        binding.download.setEnabled(true);
        binding.download.setVisibility(View.GONE);
        binding.cancel.setVisibility(View.GONE);
        binding.progress.setVisibility(View.GONE);
        binding.progressLayout.setVisibility(View.GONE);
        binding.read.setVisibility(View.VISIBLE);
        binding.delete.setVisibility(View.VISIBLE);
    }

    @Override
    public void cancelDownloading() {
        binding.download.setEnabled(true);
        binding.cancel.setVisibility(View.GONE);
        viewModel.cancelPublicationDownloading(publicationId);
        binding.progress.setVisibility(View.GONE);
        binding.progressLayout.setVisibility(View.GONE);
    }

    @Override
    public void readPublication() {
        Intent intent = new Intent(this, PdfViewerActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_ID, publicationId);
        startActivity(intent);
    }

    @Override
    public void deletePublication() {
        AlertDialog alertDialog = new AlertDialog.Builder(PublicationDetailActivity.this)
                .setMessage(String.format(Locale.getDefault(), "%s \"%s\"?",
                        getString(R.string.want_delete), viewModel.getPublicationLiveData().getValue().getData().getTitle()))
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> {
                    binding.read.setEnabled(false);
                    binding.delete.setEnabled(false);
                    viewModel.deletePublicationFromDevice(publicationId);
                })
                .setNegativeButton(R.string.cancel, null)
                .create();

        alertDialog.show();
    }

    @Override
    public void publicationDeleted() {
        binding.read.setEnabled(true);
        binding.delete.setEnabled(true);
        binding.download.setVisibility(View.VISIBLE);
        binding.read.setVisibility(View.GONE);
        binding.delete.setVisibility(View.GONE);
    }

    @Override
    public void showRatingDialog(float userRating) {
        RatingDialog ratingDialog = RatingDialog.newInstance(userRating);
        ratingDialog.setOnRatingChangeListener(rating -> viewModel.ratePublication(publicationId, rating));
        ratingDialog.show(getSupportFragmentManager(), RatingDialog.TAG);
    }

    @Override
    public void updateRating(Rating rating) {
        binding.rating.setRating(rating.getRating());
    }

    @Override
    public void updateFavoritesStatus(boolean favorite) {
        binding.favoritesButton.setImageResource(viewModel.getPublicationLiveData().getValue()
                .getData().isFavorite() ? R.drawable.ic_heart : R.drawable.ic_heart_outline);
    }

}
