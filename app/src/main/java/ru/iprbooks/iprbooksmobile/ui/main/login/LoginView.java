package ru.iprbooks.iprbooksmobile.ui.main.login;

public interface LoginView {

    void openRestoreScreen();

    void openRegisterScreen();

    void openMainScreen();

}
