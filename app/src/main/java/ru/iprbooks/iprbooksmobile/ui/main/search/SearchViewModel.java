package ru.iprbooks.iprbooksmobile.ui.main.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.model.DataList;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.model.Loading;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.EmptyListException;

@HiltViewModel
public class SearchViewModel extends BaseDisposablesViewModel {

    private final DataApiInterface dataApi;

    private final MutableStateLiveData<DataList<IListItem>> publicationsLiveData = new MutableStateLiveData<>();
    private final MutableLiveData<String> searchLiveData = new MutableLiveData<>();
    private DataList<IListItem> publications = new DataList<>();


    @Inject
    public SearchViewModel(DataApiInterface dataApi) {
        this.dataApi = dataApi;
        searchLiveData.setValue("");
        publications.setData(new ArrayList<>());
    }

    LiveData<StateData<DataList<IListItem>>> getPublications() {
        return publicationsLiveData;
    }

    LiveData<String> getSearch() {
        return searchLiveData;
    }

    void setSearch(String search) {
        searchLiveData.setValue(search);
    }

    void searchInit(int type) {
        page = 1;
        publications.getData().clear();
        publications.setCurrentPage(page);
        publications.setLastPage(page + 1);
        search(type);
    }

    void search(int type) {
        if (searchLiveData.getValue().equals("")) {
            publicationsLiveData.setSuccess(null);
            return;
        }
        StateData<DataList<IListItem>> publicationsStateData = publicationsLiveData.getValue();
        if (publicationsStateData != null) {
            if (publicationsStateData.getStatus() == DataStatus.LOADING) {
                return;
            }

            if (publications.getCurrentPage() >= publications.getLastPage()) {
                return;
            }
        }

        disposables.add(dataApi.search(searchLiveData.getValue(), type, page)
                .subscribeOn(Schedulers.io())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                    if (response.getData().getTotal() == 0) {
                        throw new EmptyListException();
                    }
                })
                .map(ResponseData::getData)
                .map(publicationDataList -> {
                    List<PublicationListItemVO> publicationVOList = new ArrayList<>();
                    for (Publication publication : publicationDataList.getData()) {
                        publicationVOList.add(new PublicationListItemVO(
                                publication.getId(),
                                publication.getTitle(),
                                String.format(Locale.getDefault(), "%d", publication.getPubYear()),
                                publication.getDescription(),
                                publication.getImage()
                        ));
                    }

                    List<IListItem> newPublicationsList = new ArrayList<>(publications.getData());
                    if (newPublicationsList.size() > 0) {
                        newPublicationsList.remove(newPublicationsList.size() - 1);
                    }

                    newPublicationsList.addAll(publicationVOList);

                    if (publicationDataList.getCurrentPage() < publicationDataList.getLastPage()) {
                        newPublicationsList.add(new Loading());
                    }

                    return new DataList<>(
                            publicationDataList.getTotal(),
                            publicationDataList.getLastPage(),
                            publicationDataList.getPerPage(),
                            publicationDataList.getCurrentPage(),
                            newPublicationsList
                    );
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> publicationsLiveData.setLoading())
                .subscribe(iListItemDataList -> {
                    publications = iListItemDataList;
                    publicationsLiveData.setSuccess(iListItemDataList);
                    page++;
                }, publicationsLiveData::setError));
    }

}
