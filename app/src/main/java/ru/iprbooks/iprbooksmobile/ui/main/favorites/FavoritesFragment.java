package ru.iprbooks.iprbooksmobile.ui.main.favorites;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.NavigationUI;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.FragmentFavoritesBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.util.PublicationType;


public class FavoritesFragment extends BaseFragment {

    public static final String TAG = "FavoritesFragment";

    private FragmentFavoritesBinding binding;
    private NavController navController;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentFavoritesBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        NavHostFragment navHostFragment = (NavHostFragment) getChildFragmentManager()
                .findFragmentById(R.id.favorites_nav_host_fragment);

        if (navHostFragment != null) {
            Bundle startArgs = new Bundle();
            startArgs.putBoolean(MainActivity.IS_FAVORITE, true);
            startArgs.putBoolean(MainActivity.IS_DOWNLOADED, false);
            startArgs.putInt(MainActivity.PUBLICATION_TYPE, PublicationType.BOOK);

            navController = navHostFragment.getNavController();
            navController.setGraph(R.navigation.bottom_navigation, startArgs);

            NavigationUI.setupWithNavController(binding.bottomNavigation, navController);
        }

        binding.bottomNavigation.setOnItemSelectedListener(item -> {
            int itemId = item.getItemId();
            Bundle args = new Bundle();
            args.putBoolean(MainActivity.IS_FAVORITE, true);
            args.putBoolean(MainActivity.IS_DOWNLOADED, false);
            args.putInt(MainActivity.PUBLICATION_TYPE,
                    itemId == R.id.nav_books ? PublicationType.BOOK : PublicationType.JOURNAL);
            navController.navigate(itemId, args);
            return true;
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
