package ru.iprbooks.iprbooksmobile.ui.base.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {

    public BaseViewHolder(@NonNull ViewBinding binding) {
        super(binding.getRoot());
    }

    public void onBind(Object item, int position) {
    }

}
