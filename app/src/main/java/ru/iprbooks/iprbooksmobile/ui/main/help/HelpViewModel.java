package ru.iprbooks.iprbooksmobile.ui.main.help;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.model.ResponseApi;
import ru.iprbooks.iprbooksmobile.data.remote.DataApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;

@HiltViewModel
public class HelpViewModel extends BaseDisposablesViewModel {

    private final DataApiInterface dataApi;

    MutableStateLiveData<String> stateLiveData = new MutableStateLiveData<>();

    @Inject
    public HelpViewModel(DataApiInterface dataApi) {
        this.dataApi = dataApi;
    }

    void sendHelpRequest(String type, String text) {
        disposables.add(dataApi.sendHelpRequest(type, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> stateLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                })
                .map(ResponseApi::getMeta)
                .subscribe(meta -> stateLiveData.setSuccess(meta.getMessage()),
                        throwable -> stateLiveData.setError(throwable)));
    }

}
