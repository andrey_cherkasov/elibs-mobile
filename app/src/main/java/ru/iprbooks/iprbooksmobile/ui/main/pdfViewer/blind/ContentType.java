package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind;

public enum ContentType {
    CONTENT,
    BOOKMARK,
    QUOTE
}
