package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer;

import ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.ContentType;

public interface PdfViewerView {

    void find(String text);

    void showActionBar();

    void hideActionBar();

    void showActions();

    void hideActions();

    void showSearch();

    void hideSearch();

    void copyToClipBoard();

    void shareSelected();

    void openPdfViewerSettingsScreen();

    void applySettings();

    void showBlind(ContentType type);

    void hideBlind();

    void showAddBookmarkDialog();

}
