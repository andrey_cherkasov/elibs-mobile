package ru.iprbooks.iprbooksmobile.ui.main.restore;

import androidx.lifecycle.LiveData;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import ru.iprbooks.iprbooksmobile.data.MutableStateLiveData;
import ru.iprbooks.iprbooksmobile.data.StateData;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.data.remote.AuthApiInterface;
import ru.iprbooks.iprbooksmobile.ui.base.viewmodel.BaseDisposablesViewModel;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;

@HiltViewModel
public final class RestoreViewModel extends BaseDisposablesViewModel {

    private final MutableStateLiveData<User> stateLiveData = new MutableStateLiveData<>();

    private final AuthApiInterface authApi;

    @Inject
    RestoreViewModel(AuthApiInterface authApi) {
        this.authApi = authApi;
    }

    LiveData<StateData<User>> getUser() {
        return stateLiveData;
    }

    public void restore(String email) {
        disposables.add(authApi.restore(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(disposable -> stateLiveData.setLoading())
                .doOnSuccess(response -> {
                    if (!response.getMeta().isSuccess()) {
                        throw new ApiErrorException(response.getMeta().getMessage());
                    }
                }).map(ResponseData::getData)
                .subscribe(stateLiveData::setSuccess, stateLiveData::setError));
    }

}
