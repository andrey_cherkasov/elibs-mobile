package ru.iprbooks.iprbooksmobile.ui.main.ugnp;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.Ugnp;
import ru.iprbooks.iprbooksmobile.databinding.ItemUgnpBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;

public class UgnpAdapter extends RecyclerView.Adapter<UgnpAdapter.ViewHolder> {

    private final List<Ugnp> items = new ArrayList<>();
    private OnListViewItemClickListener<Ugnp> listener;


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUgnpBinding binding = ItemUgnpBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.onBind(items.get(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    void addItems(List<Ugnp> newItems) {
        final int size = items.size();
        items.addAll(newItems);
        notifyItemRangeInserted(size, newItems.size());
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<Ugnp> listener) {
        this.listener = listener;
    }


    class ViewHolder extends BaseViewHolder {

        ItemUgnpBinding binding;

        public ViewHolder(@NonNull ViewBinding itemBinding) {
            super(itemBinding);
            this.binding = (ItemUgnpBinding) itemBinding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            Ugnp ugnp = (Ugnp) item;
            binding.name.setText(ugnp.getName());
            binding.getRoot().setOnClickListener(v -> {
                if (UgnpAdapter.this.listener != null) {
                    UgnpAdapter.this.listener.onItemClick(v, ugnp, position);
                }
            });
        }
    }

}
