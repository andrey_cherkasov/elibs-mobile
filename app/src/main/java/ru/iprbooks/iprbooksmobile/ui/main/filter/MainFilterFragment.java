package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import java.util.List;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.PubType;
import ru.iprbooks.iprbooksmobile.databinding.FragmentMainFilterBinding;

public class MainFilterFragment extends Fragment {

    private FragmentMainFilterBinding binding;
    private FilterViewModel viewModel;
    private String[] compilations;
    private Filter filter;
    private final AutocompleteAdapter pubHouseAutocompleteAdapter = new AutocompleteAdapter();
    private final AutocompleteAdapter authorsAutocompleteAdapter = new AutocompleteAdapter();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentMainFilterBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(FilterViewModel.class);
        compilations = getResources().getStringArray(R.array.compilations);

        filter = requireArguments().getParcelable(Filter.TAG);
        binding.pubHouse.setText(filter.getPubHouse());
        binding.authors.setText(filter.getAuthors());
        binding.yearLeft.setText(filter.getYearLeft());
        binding.yearRight.setText(filter.getYearRight());
        if (filter.getPubTypes().size() > 0) {
            StringBuilder tmp = new StringBuilder();
            for (PubType pubType : filter.getPubTypes()) {
                if (pubType.getChecked()) {
                    tmp.append(pubType.getTitle()).append(", ");
                }
            }

            if (tmp.length() > 3) {
                String selected = tmp.substring(0, tmp.length() - 2);
                binding.selectedPybTypes.setText(selected);
            } else {
                binding.selectedPybTypes.setText(R.string.not_selected);
            }
        }
        binding.selectedCompilations.setText(compilations[filter.getCompilation()]);

        // pubhouse autocomplete
        binding.pubHouse.setAdapter(pubHouseAutocompleteAdapter);
        binding.pubHouse.setThreshold(2);
        binding.pubHouse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String search = charSequence.toString();
                if (search.length() > 2) {
                    viewModel.autocompletePerform(filter, Filter.AutocompleteField.PUB_HOUSE, search);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        viewModel.getPubHouse().observe(getViewLifecycleOwner(), listStateData
                -> pubHouseAutocompleteAdapter.setData(listStateData.getData()));

        // authors autocomplete
        binding.authors.setAdapter(authorsAutocompleteAdapter);
        binding.authors.setThreshold(2);
        binding.authors.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String search = charSequence.toString();
                if (search.length() > 2) {
                    viewModel.autocompletePerform(filter, Filter.AutocompleteField.AUTHORS, search);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        viewModel.getAuthors().observe(getViewLifecycleOwner(), listStateData
                -> authorsAutocompleteAdapter.setData(listStateData.getData()));

        // cleaners
        binding.pubHouseClear.setOnClickListener(v -> binding.pubHouse.setText(""));
        binding.authorsClear.setOnClickListener(v -> binding.authors.setText(""));
        binding.yearLeftClear.setOnClickListener(v -> binding.yearLeft.setText(""));
        binding.yearRightClear.setOnClickListener(v -> binding.yearRight.setText(""));

        viewModel.getCompilations().observe(getViewLifecycleOwner(), integerStateData -> {
            Integer compilationId = integerStateData.getData();
            if (compilationId != null) {
                binding.selectedCompilations.setText(compilations[compilationId]);
                filter.setCompilation(compilationId);
            } else {
                binding.selectedCompilations.setText(R.string.not_selected);
            }
        });

        viewModel.getPubTypesSelected().observe(getViewLifecycleOwner(), listStateData -> {
            List<PubType> selectedPubTypes = listStateData.getData();

            if (selectedPubTypes != null && selectedPubTypes.size() > 0) {
                StringBuilder tmp = new StringBuilder();
                for (PubType pubType : selectedPubTypes) {
                    if (pubType.getChecked()) {
                        tmp.append(pubType.getTitle()).append(", ");
                    }
                }

                if (tmp.length() > 3) {
                    String selected = tmp.substring(0, tmp.length() - 2);
                    binding.selectedPybTypes.setText(selected);
                } else {
                    binding.selectedPybTypes.setText(R.string.not_selected);
                }
                filter.setPubTypes(selectedPubTypes);
            } else {
                binding.selectedPybTypes.setText(R.string.not_selected);
            }
        });

        binding.pubTypesLayout.setOnClickListener(v -> openPubTypesScreen());
        binding.compilationsLayout.setOnClickListener(v -> openCompilationsScreen());
        binding.ok.setOnClickListener(v -> {
            filter.setPubHouse(binding.pubHouse.getText().toString());
            filter.setAuthors(binding.authors.getText().toString());
            filter.setYearLeft(binding.yearLeft.getText().toString());
            filter.setYearRight(binding.yearRight.getText().toString());
            viewModel.setFilter(filter);
        });

        if (filter.getType().equals(Filter.TYPE_JOURNAL)) {
            binding.authorsLayout.setVisibility(View.GONE);
            binding.years.setVisibility(View.GONE);
            binding.yearLeftLayout.setVisibility(View.GONE);
            binding.yearRightLayout.setVisibility(View.GONE);
            binding.pubTypesLayout.setVisibility(View.GONE);
            binding.compilationsLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void openPubTypesScreen() {
        Bundle args = new Bundle();
        args.putParcelable(Filter.TAG, filter);
        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_nav_main_filter_to_nav_pub_types_filter, args);
    }

    private void openCompilationsScreen() {
        Bundle args = new Bundle();
        args.putParcelable(Filter.TAG, filter);
        Navigation.findNavController(binding.getRoot()).navigate(R.id.action_nav_main_filter_to_nav_compilations_filter, args);
    }

}
