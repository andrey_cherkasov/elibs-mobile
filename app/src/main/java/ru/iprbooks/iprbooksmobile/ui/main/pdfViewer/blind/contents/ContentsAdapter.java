package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.contents;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewbinding.ViewBinding;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.Content;
import ru.iprbooks.iprbooksmobile.databinding.ItemBlindBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;

public class ContentsAdapter extends RecyclerView.Adapter<ContentsAdapter.BlindViewHolder> {

    private OnListViewItemClickListener<Content> listener;
    private final List<Content> items = new ArrayList<>();


    @NonNull
    @Override
    public BlindViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BlindViewHolder(ItemBlindBinding.inflate(LayoutInflater.from(parent.getContext()),
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BlindViewHolder holder, int position) {
        holder.onBind(items.get(position), position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @SuppressLint("NotifyDataSetChanged")
    void addItems(List<Content> newItems) {
        items.clear();
        items.addAll(newItems);
        notifyDataSetChanged();
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<Content> listener) {
        this.listener = listener;
    }

    class BlindViewHolder extends BaseViewHolder {

        ItemBlindBinding binding;

        public BlindViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemBlindBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            Content content = (Content) item;
            binding.text.setText(content.getText());
            binding.page.setText(String.format(Locale.getDefault(),
                    binding.getRoot().getContext().getString(R.string.item_blind_page),
                    content.getPage()));

            binding.delete.setVisibility(View.GONE);

            binding.getRoot().setOnClickListener(v -> {
                if (listener != null) {
                    int pos = getAbsoluteAdapterPosition();
                    listener.onItemClick(v, items.get(pos), pos);
                }
            });
        }
    }

}
