package ru.iprbooks.iprbooksmobile.ui.main.register;

public interface RegisterView {

    void openLoginScreen();

    void showNextStep();

    void showEmailError(boolean show);

    void showFioError(boolean show);

    void showPasswordError(boolean show);

}
