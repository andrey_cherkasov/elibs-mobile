package ru.iprbooks.iprbooksmobile.ui.base.adapter;

import android.view.View;

public interface OnListViewItemClickListener<T> {

    void onItemClick(View v, T item, int position);

}
