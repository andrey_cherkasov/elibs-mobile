package ru.iprbooks.iprbooksmobile.ui.main.publications;

import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;

public interface PublicationsView {

    void updateList();

    void openPublicationDetailScreen(PublicationListItemVO publication);

    void openSearchScreen();

    void showFilter();

    void clearFilter();

}
