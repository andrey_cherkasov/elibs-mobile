package ru.iprbooks.iprbooksmobile.ui.main.pdfViewer.blind.bookmarks;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.AsyncDifferConfig;
import androidx.recyclerview.widget.ListAdapter;
import androidx.viewbinding.ViewBinding;

import java.util.Locale;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.Bookmark;
import ru.iprbooks.iprbooksmobile.databinding.ItemBlindBinding;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseViewHolder;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.OnListViewItemClickListener;

public class BookmarksAdapter extends ListAdapter<Bookmark, BookmarksAdapter.BookmarkViewHolder> {

    private OnListViewItemClickListener<Bookmark> listener;
    private OnListViewItemClickListener<Bookmark> deleteListener;

    protected BookmarksAdapter(@NonNull AsyncDifferConfig<Bookmark> config) {
        super(config);
    }

    @NonNull
    @Override
    public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new BookmarkViewHolder(ItemBlindBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull BookmarkViewHolder holder, int position) {
        holder.onBind(getItem(position), position);
    }

    public void setOnListViewItemClickListener(OnListViewItemClickListener<Bookmark> listener) {
        this.listener = listener;
    }

    public void setDeleteListener(OnListViewItemClickListener<Bookmark> listener) {
        this.deleteListener = listener;
    }

    class BookmarkViewHolder extends BaseViewHolder {

        ItemBlindBinding binding;

        public BookmarkViewHolder(@NonNull ViewBinding binding) {
            super(binding);
            this.binding = (ItemBlindBinding) binding;
        }

        @Override
        public void onBind(Object item, int position) {
            super.onBind(item, position);
            Bookmark bookmark = (Bookmark) item;
            binding.text.setText(bookmark.getText());
            binding.page.setText(String.format(Locale.getDefault(),
                    binding.getRoot().getContext().getString(R.string.item_blind_page),
                    bookmark.getPage()));

            binding.getRoot().setOnClickListener(v -> {
                if (listener != null) {
                    int pos = getAbsoluteAdapterPosition();
                    listener.onItemClick(v, getCurrentList().get(pos), pos);
                }
            });

            binding.delete.setOnClickListener(v -> {
                if (deleteListener != null) {
                    int pos = getAbsoluteAdapterPosition();
                    deleteListener.onItemClick(v, getCurrentList().get(pos), pos);
                }
            });
        }
    }

}
