package ru.iprbooks.iprbooksmobile.ui.main.ugnp;

import ru.iprbooks.iprbooksmobile.data.model.Ugnp;

public interface UgnpView {

    void openCatalog(Ugnp ugnp);

}
