package ru.iprbooks.iprbooksmobile.ui.main.journalDetail;

import android.os.Parcel;
import android.os.Parcelable;

import ru.iprbooks.iprbooksmobile.data.model.Publication;

public class JournalNumber implements Parcelable {

    private String year;

    private Publication number;

    public JournalNumber(String year) {
        this.year = year;
        this.number = null;
    }

    public JournalNumber(Publication number) {
        this.year = null;
        this.number = number;
    }

    protected JournalNumber(Parcel in) {
        year = in.readString();
        number = in.readParcelable(Publication.class.getClassLoader());
    }

    public static final Creator<JournalNumber> CREATOR = new Creator<JournalNumber>() {
        @Override
        public JournalNumber createFromParcel(Parcel in) {
            return new JournalNumber(in);
        }

        @Override
        public JournalNumber[] newArray(int size) {
            return new JournalNumber[size];
        }
    };

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Publication getNumber() {
        return number;
    }

    public void setNumber(Publication number) {
        this.number = number;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(year);
        dest.writeParcelable(number, flags);
    }

}
