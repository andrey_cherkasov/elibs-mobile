package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.radiobutton.MaterialRadioButton;

import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.databinding.FragmentCompilationsFilterBinding;

public class CompilationsFilterFragment extends Fragment {

    private FragmentCompilationsFilterBinding binding;
    private FilterViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentCompilationsFilterBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(requireActivity()).get(FilterViewModel.class);

        Filter filter = requireArguments().getParcelable(Filter.TAG);
        String[] compilations = getResources().getStringArray(R.array.compilations);
        for (int i = 0; i < compilations.length; i++) {
            MaterialRadioButton radioButton = new MaterialRadioButton(requireContext());
            radioButton.setId(i);
            radioButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            radioButton.setText(compilations[i]);
            radioButton.setUseMaterialThemeColors(true);
            binding.compilationsRadioGroup.addView(radioButton);
        }

        binding.compilationsRadioGroup.check(filter.getCompilation());
        binding.compilationsRadioGroup.setOnCheckedChangeListener((group, checkedId)
                -> viewModel.setCompilation(checkedId));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}
