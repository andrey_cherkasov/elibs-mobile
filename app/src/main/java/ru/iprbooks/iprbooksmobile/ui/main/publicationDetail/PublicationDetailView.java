package ru.iprbooks.iprbooksmobile.ui.main.publicationDetail;

import ru.iprbooks.iprbooksmobile.data.model.Rating;

public interface PublicationDetailView {

    void downloadPublication();

    void publicationDownloaded();

    void cancelDownloading();

    void readPublication();

    void deletePublication();

    void publicationDeleted();

    void showRatingDialog(float rating);

    void updateRating(Rating rating);

    void updateFavoritesStatus(boolean favorite);


}
