package ru.iprbooks.iprbooksmobile.ui.main.filter;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.collection.ArrayMap;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.iprbooks.iprbooksmobile.data.model.PubType;

public class Filter implements Parcelable {

    public final static String TAG = "FILTER";
    public final static String TYPE_BOOK = "13";
    public final static String TYPE_JOURNAL = "28";

    public final static String AUTOCOMPLETE_TYPE = "type";
    public final static String AUTOCOMPLETE_FIELD = "field";
    public final static String AUTOCOMPLETE_SEARCH = "query";
    public final static String AUTOCOMPLETE_UGNP = "ugnp";

    public final static String UGNP = "ugnp";
    public final static String TITLE = "title";
    public final static String PUBHOUSE = "pubhouse";
    public final static String AUTHORS = "authors";
    public final static String YEAR_LEFT = "year_left";
    public final static String YEAR_RIGHT = "year_right";
    public final static String PUB_TYPES = "pub_types";
    public final static String COMPILATION = "selection_options";

    private String type;
    private String ugnp;
    private String title;
    private String pubHouse;
    private String authors;
    private String yearLeft;
    private String yearRight;
    private List<PubType> pubTypes;
    private int compilation;


    public Filter(String ugnp, String type) {
        this.type = type;
        this.ugnp = ugnp;
        this.title = "";
        this.pubHouse = "";
        this.authors = "";
        this.yearLeft = "";
        this.yearRight = "";
        this.pubTypes = new ArrayList<>();
        this.compilation = 0;
    }


    protected Filter(Parcel in) {
        type = in.readString();
        ugnp = in.readString();
        title = in.readString();
        pubHouse = in.readString();
        authors = in.readString();
        yearLeft = in.readString();
        yearRight = in.readString();
        pubTypes = in.createTypedArrayList(PubType.CREATOR);
        compilation = in.readInt();
    }

    public static final Creator<Filter> CREATOR = new Creator<Filter>() {
        @Override
        public Filter createFromParcel(Parcel in) {
            return new Filter(in);
        }

        @Override
        public Filter[] newArray(int size) {
            return new Filter[size];
        }
    };

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUgnp() {
        return ugnp;
    }

    public void setUgnp(String ugnp) {
        this.ugnp = ugnp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPubHouse() {
        return pubHouse;
    }

    public void setPubHouse(String pubHouse) {
        this.pubHouse = pubHouse;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getYearLeft() {
        return yearLeft;
    }

    public void setYearLeft(String yearLeft) {
        this.yearLeft = yearLeft;
    }

    public String getYearRight() {
        return yearRight;
    }

    public void setYearRight(String yearRight) {
        this.yearRight = yearRight;
    }

    public List<PubType> getPubTypes() {
        return pubTypes;
    }

    public void setPubTypes(List<PubType> pubTypes) {
        this.pubTypes = pubTypes;
    }

    public int getCompilation() {
        return compilation;
    }

    public void setCompilation(int compilation) {
        this.compilation = compilation;
    }

    public boolean isEmpty() {
        boolean pubTypeSelected = false;
        for (PubType pubType : pubTypes) {
            if (pubType.getChecked()) {
                pubTypeSelected = true;
                break;
            }
        }

        return this.title.equals("")
                && this.pubHouse.equals("")
                && this.authors.equals("")
                && this.yearLeft.equals("")
                && this.yearRight.equals("")
                && (this.pubTypes.size() == 0 || !pubTypeSelected)
                && this.compilation == 0;
    }

    public void clear() {
        this.title = "";
        this.pubHouse = "";
        this.authors = "";
        this.yearLeft = "";
        this.yearRight = "";
        this.pubTypes = new ArrayList<>();
        this.compilation = 0;
    }

    public Map<String, String> toMap() {
        StringBuilder selectedPubTypes = new StringBuilder();
        if (pubTypes != null && pubTypes.size() > 0) {
            for (PubType pubType : pubTypes) {
                if (pubType.getChecked()) {
                    selectedPubTypes.append(pubType.getId()).append("||");
                }
            }
        }

        Map<String, String> searchMap = new ArrayMap<>();
        searchMap.put(UGNP, ugnp);
        searchMap.put(TITLE, title);
        searchMap.put(PUBHOUSE, pubHouse);
        searchMap.put(AUTHORS, authors);
        searchMap.put(YEAR_LEFT, yearLeft);
        searchMap.put(YEAR_RIGHT, yearRight);
        searchMap.put(PUB_TYPES, selectedPubTypes.toString());
        searchMap.put(COMPILATION, String.valueOf(compilation));
        return searchMap;
    }

    public static String getField(AutocompleteField autocompleteField) {
        if (autocompleteField.equals(AutocompleteField.AUTHORS)) {
            return "tv_author";
        } else if (autocompleteField.equals(AutocompleteField.PUB_HOUSE)) {
            return "tv_pubhouse";
        } else {
            return "pagetitle";
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(ugnp);
        dest.writeString(title);
        dest.writeString(pubHouse);
        dest.writeString(authors);
        dest.writeString(yearLeft);
        dest.writeString(yearRight);
        dest.writeTypedList(pubTypes);
        dest.writeInt(compilation);
    }

    enum AutocompleteField {
        AUTHORS,
        PUB_HOUSE
    }

}
