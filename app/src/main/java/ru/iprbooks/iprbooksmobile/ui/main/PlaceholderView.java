package ru.iprbooks.iprbooksmobile.ui.main;

public interface PlaceholderView {

    void openMainScreen();

    void openLoginScreen();

}
