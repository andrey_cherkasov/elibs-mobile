package ru.iprbooks.iprbooksmobile.ui.main.restore;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.databinding.ActivityRestoreBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.login.LoginActivity;
import ru.iprbooks.iprbooksmobile.ui.main.register.RegisterActivity;
import ru.iprbooks.iprbooksmobile.util.Utils;

@AndroidEntryPoint
public class RestoreActivity extends BaseActivity implements RestoreView, LoadingView<User> {

    private ActivityRestoreBinding binding;
    private RestoreViewModel viewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRestoreBinding.inflate(getLayoutInflater());
        viewModel = new ViewModelProvider(this).get(RestoreViewModel.class);
        setContentView(binding.getRoot());
        init();
    }

    private void init() {
        binding.loginButton.setOnClickListener(v -> openLoginScreen());
        binding.signUpButton.setOnClickListener(v -> openRegisterScreen());
        binding.restoreButton.setOnClickListener(v -> {
            String email = binding.emailEditText.getText().toString();
            if (Utils.isValidEmail(email)) {
                showEmailError(false);
                viewModel.restore(email);
            } else {
                showEmailError(true);
            }
        });
        viewModel.getUser().observe(this, userStateData -> {
            switch (userStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    break;
                case ERROR:
                    showLoading(false);
                    showError(userStateData.getError());
                    break;
            }
        });
    }

    @Override
    public void showLoading(boolean loading) {
        binding.loadingLayout.getRoot().setVisibility(loading ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setData(User data) {

    }

    @Override
    public void openLoginScreen() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void openRegisterScreen() {
        Intent intent = new Intent(this, RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void showEmailError(boolean show) {
        binding.emailInputLayout.setError(show ? getString(R.string.invalid_email) : "");
    }

}
