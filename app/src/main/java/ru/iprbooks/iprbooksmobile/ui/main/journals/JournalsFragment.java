package ru.iprbooks.iprbooksmobile.ui.main.journals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import java.util.ArrayList;
import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.DataStatus;
import ru.iprbooks.iprbooksmobile.data.model.IListItem;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.NoConnectivityException;
import ru.iprbooks.iprbooksmobile.databinding.FragmentPublicationsBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BaseDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.BasePagingAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.LoadingDelegateAdapter;
import ru.iprbooks.iprbooksmobile.ui.base.adapter.PaginationListener;
import ru.iprbooks.iprbooksmobile.ui.main.filter.Filter;
import ru.iprbooks.iprbooksmobile.ui.main.filter.FilterActivity;
import ru.iprbooks.iprbooksmobile.ui.main.journalDetail.JournalDetailActivity;
import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.ui.main.search.SearchActivity;
import ru.iprbooks.iprbooksmobile.ui.main.ugnp.UgnpFragment;
import ru.iprbooks.iprbooksmobile.util.ApiErrorException;
import ru.iprbooks.iprbooksmobile.util.EmptyListException;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@AndroidEntryPoint
public class JournalsFragment extends BaseFragment implements JournalsView {

    private static final String TAG = "JournalsFragment";

    private FragmentPublicationsBinding binding;
    private JournalsViewModel viewModel;
    private MenuItem clearFilerMenuItem;
    private BasePagingAdapter<IListItem> adapter;

    private final ActivityResultLauncher<Intent> filterActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    Intent data = result.getData();
                    if (data != null) {
                        viewModel.setFilter(data.getParcelableExtra(Filter.TAG));
                        if (!viewModel.getFilter().getValue().isEmpty()) {
                            clearFilerMenuItem.setVisible(true);
                            updateList();
                        }
                    }
                }
            });

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(JournalsViewModel.class);

        JournalsDelegateAdapter journalsDelegateAdapter = new JournalsDelegateAdapter();
        journalsDelegateAdapter.setOnListViewItemClickListener((v, item, position) -> openJournalDetailScreen(item));

        List<BaseDelegateAdapter> delegateAdapters = new ArrayList<>();
        delegateAdapters.add(new LoadingDelegateAdapter());
        delegateAdapters.add(journalsDelegateAdapter);

        adapter = new BasePagingAdapter<>(new DiffUtil.ItemCallback<IListItem>() {
            @Override
            public boolean areItemsTheSame(@NonNull IListItem oldItem, @NonNull IListItem newItem) {
                if (oldItem instanceof PublicationListItemVO && newItem instanceof PublicationListItemVO) {
                    return ((PublicationListItemVO) oldItem).getId() == ((PublicationListItemVO) newItem).getId();
                }
                return false;
            }

            @Override
            public boolean areContentsTheSame(@NonNull IListItem oldItem, @NonNull IListItem newItem) {
                return true;
            }
        }, delegateAdapters);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentPublicationsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        // init params
        Bundle arguments = getArguments();
        String ugnp = "00";
        if (arguments != null) {
            ugnp = arguments.getString(UgnpFragment.UGNP_ID, "00");
        }

        // init ui
        binding.filter.setOnClickListener(v -> showFilter());

        binding.list.setAdapter(adapter);
        binding.list.addOnScrollListener(new PaginationListener((LinearLayoutManager) binding.list.getLayoutManager()) {
            @Override
            protected void loadMoreItems() {
                viewModel.getJournals();
            }
        });

        binding.noInternetLayout.refresh.setOnClickListener(v -> updateList());

        // observer
        viewModel.getJournalsLiveData().observe(getViewLifecycleOwner(), listStateData -> {
            DataStatus status = listStateData.getStatus();
            if (status.equals(DataStatus.SUCCESS)) {
                adapter.submitList(new ArrayList<>(listStateData.getData().getData()));
                binding.loadingLayout.getRoot().setVisibility(View.GONE);
            } else if (status.equals(DataStatus.ERROR)) {
                Throwable throwable = listStateData.getError();
                if (throwable != null) {
                    if (throwable instanceof EmptyListException) {
                        binding.notFoundLayout.getRoot().setVisibility(View.VISIBLE);
                    } else if (throwable instanceof ApiErrorException) {
                        showMessage(throwable.getMessage());
                    } else if (throwable instanceof NoConnectivityException) {
                        binding.noInternetLayout.getRoot().setVisibility(View.VISIBLE);
                        binding.filter.setVisibility(View.GONE);
                    } else {
                        showMessage(getString(R.string.error));
                        throwable.printStackTrace();
                    }
                }
            }
        });

        if (viewModel.getJournalsLiveData().getValue() == null) {
            viewModel.setFilter(new Filter(ugnp, Filter.TYPE_JOURNAL));
            updateList();
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.activity_filter, menu);
        clearFilerMenuItem = menu.findItem(R.id.action_clear_filter);
        clearFilerMenuItem.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == R.id.action_clear_filter) {
            clearFilter();
        } else if (menuId == R.id.action_search) {
            openSearchScreen();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void updateList() {
        adapter.submitList(null);
        binding.notFoundLayout.getRoot().setVisibility(View.GONE);
        binding.noInternetLayout.getRoot().setVisibility(View.GONE);
        binding.loadingLayout.getRoot().setVisibility(View.VISIBLE);
        viewModel.getJournalsInit();
    }

    @Override
    public void openJournalDetailScreen(PublicationListItemVO journal) {
        Intent intent = new Intent(getContext(), JournalDetailActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_ID, journal.getId());
        intent.putExtra(MainActivity.PUBLICATION_OPENED_FROM, JournalsFragment.TAG);
        startActivity(intent);
    }

    @Override
    public void openSearchScreen() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        intent.putExtra(MainActivity.PUBLICATION_TYPE, PublicationType.JOURNAL);
        startActivity(intent);
    }

    @Override
    public void showFilter() {
        Intent intent = new Intent(getContext(), FilterActivity.class);
        intent.putExtra(Filter.TAG, viewModel.getFilter().getValue());
        filterActivityResultLauncher.launch(intent);
    }

    @Override
    public void clearFilter() {
        clearFilerMenuItem.setVisible(false);
        viewModel.getFilter().getValue().clear();
        updateList();
    }

}
