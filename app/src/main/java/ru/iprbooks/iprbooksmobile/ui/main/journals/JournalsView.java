package ru.iprbooks.iprbooksmobile.ui.main.journals;

import ru.iprbooks.iprbooksmobile.data.model.PublicationListItemVO;

public interface JournalsView {

    void updateList();

    void openJournalDetailScreen(PublicationListItemVO journal);

    void openSearchScreen();

    void showFilter();

    void clearFilter();

}
