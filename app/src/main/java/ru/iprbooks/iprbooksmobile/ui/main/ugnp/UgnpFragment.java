package ru.iprbooks.iprbooksmobile.ui.main.ugnp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.model.Ugnp;
import ru.iprbooks.iprbooksmobile.databinding.FragmentUgnpBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseFragment;
import ru.iprbooks.iprbooksmobile.ui.base.LoadingView;
import ru.iprbooks.iprbooksmobile.ui.main.main.MainActivity;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@AndroidEntryPoint
public class UgnpFragment extends BaseFragment implements UgnpView, LoadingView<List<Ugnp>> {

    public final static String UGNP_ID = "UGNP_ID";
    public final static String UGNP_TYPE = "UGNP_TYPE";

    private FragmentUgnpBinding binding;
    private UgnpViewModel viewModel;
    private UgnpAdapter adapter;
    private int type = PublicationType.BOOK;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UgnpViewModel.class);
        adapter = new UgnpAdapter();
        Bundle args = getArguments();
        type = (args != null) ? args.getInt(UGNP_TYPE) : PublicationType.BOOK;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentUgnpBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter.setOnListViewItemClickListener((v, item, position) -> openCatalog(item));
        binding.list.setAdapter(adapter);

        if (viewModel.getUgnp().getValue() == null) {
            viewModel.loadUgnpList();
        }

        viewModel.getUgnp().observe(this.requireActivity(), listStateData -> {
            switch (listStateData.getStatus()) {
                case LOADING:
                    showLoading(true);
                    break;
                case SUCCESS:
                    showLoading(false);
                    setData(listStateData.getData());
                    break;
                case ERROR:
                    showLoading(false);
                    showError(listStateData.getError());
                    break;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void showLoading(boolean loading) {

    }

    @Override
    public void setData(List<Ugnp> data) {
        adapter.addItems(data);
    }

    @Override
    public void openCatalog(Ugnp ugnp) {
        Bundle args = new Bundle();
        args.putString(UGNP_ID, ugnp.getId());
        args.putString(MainActivity.SUBTITLE, ugnp.getName());
        args.putSerializable(MainActivity.IS_FAVORITE, false);
        args.putSerializable(MainActivity.IS_DOWNLOADED, false);
        if (type == PublicationType.BOOK) {
            args.putSerializable(MainActivity.PUBLICATION_TYPE, PublicationType.BOOK);
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_nav_ugnp_books_to_nav_books, args);
        } else if (type == PublicationType.JOURNAL) {
            Navigation.findNavController(binding.getRoot()).navigate(R.id.action_nav_ugnp_journals_to_nav_journals, args);
        }
    }

}
