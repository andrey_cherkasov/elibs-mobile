package ru.iprbooks.iprbooksmobile.ui.main.restore;

public interface RestoreView {

    void openLoginScreen();

    void openRegisterScreen();

    void showEmailError(boolean show);

}
