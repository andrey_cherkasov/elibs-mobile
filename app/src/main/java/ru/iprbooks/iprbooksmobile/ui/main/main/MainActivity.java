package ru.iprbooks.iprbooksmobile.ui.main.main;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.core.view.GravityCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.bumptech.glide.Glide;
import com.google.android.material.navigation.NavigationView;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import dagger.hilt.android.AndroidEntryPoint;
import ru.iprbooks.iprbooksmobile.R;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.databinding.ActivityMainBinding;
import ru.iprbooks.iprbooksmobile.databinding.NavHeaderMainBinding;
import ru.iprbooks.iprbooksmobile.ui.base.BaseActivity;
import ru.iprbooks.iprbooksmobile.util.PublicationType;
import ru.iprbooks.iprbooksmobile.ui.main.login.LoginActivity;
import ru.iprbooks.iprbooksmobile.ui.main.ugnp.UgnpFragment;

@AndroidEntryPoint
public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener,
        NavController.OnDestinationChangedListener {

    public final static String PUBLICATION = "PUBLICATION";
    public final static String PUBLICATION_ID = "PUBLICATION_ID";
    public final static String PUBLICATION_TYPE = "PUBLICATION_TYPE";
    public final static String PUBLICATION_OPENED_FROM = "PUBLICATION_OPENED_FROM";
    public final static String IS_FAVORITE = "IS_FAVORITE";
    public final static String IS_DOWNLOADED = "IS_DOWNLOADED";
    public final static String NEED_REMOVE = "NEED_REMOVE";

    public final static String SUBTITLE = "SUBTITLE";

    private ActivityMainBinding binding;
    private AppBarConfiguration appBarConfiguration;
    private NavController navController;
    private ActionBar actionBar;

    Integer[] dest = {
            R.id.nav_ugnp_books,
            R.id.nav_ugnp_journals,
            R.id.nav_favorites,
            R.id.nav_downloaded,
            R.id.nav_help,
            R.id.nav_instruction,
            R.id.nav_logout
    };

    private final Set<Integer> topLevelDest = new HashSet<>(Arrays.asList(dest));

    @Inject
    SharedPreferencesHelper preferencesHelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        NavHeaderMainBinding headerBinding = NavHeaderMainBinding.inflate(getLayoutInflater());

        setContentView(binding.getRoot());
        setSupportActionBar(binding.appBarMain.toolbar);
        actionBar = getSupportActionBar();

        appBarConfiguration = new AppBarConfiguration.Builder(topLevelDest)
                .setOpenableLayout(binding.drawerLayout)
                .build();

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);

        if (navHostFragment != null) {
            Bundle startArgs = new Bundle();
            startArgs.putSerializable(UgnpFragment.UGNP_TYPE, PublicationType.BOOK);

            navController = navHostFragment.getNavController();
            navController.setGraph(R.navigation.mobile_navigation, startArgs);
            navController.addOnDestinationChangedListener(this);

            NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
            NavigationUI.setupWithNavController(binding.navView, navController);
        }

        binding.navView.setNavigationItemSelectedListener(this);

        User user = preferencesHelper.loadUser();
        Glide.with(this).load(R.drawable.logo).into(headerBinding.logo);
        headerBinding.userName.setText(user.getFullName());
        headerBinding.orgName.setText(user.getOrgName());
        binding.navView.addHeaderView(headerBinding.getRoot());
    }

    @Override
    public void onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, appBarConfiguration) || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle args = new Bundle();
        int itemId = item.getItemId();
        if (itemId == R.id.nav_logout) {
            preferencesHelper.deleteUser();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return false;
        } else if (itemId == R.id.nav_instruction) {
            Uri uri = Uri.parse(getString(R.string.instruction_url));
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        } else if (itemId == R.id.nav_ugnp_journals) {
            args.putSerializable(UgnpFragment.UGNP_TYPE, PublicationType.JOURNAL);
        } else if (itemId == R.id.nav_ugnp_books) {
            args.putSerializable(UgnpFragment.UGNP_TYPE, PublicationType.BOOK);
        }

        navController.navigate(itemId, args);
        binding.navView.setCheckedItem(itemId);
        binding.drawerLayout.closeDrawers();
        return false;
    }

    @Override
    public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
        actionBar.setSubtitle(arguments != null ? arguments.getString(SUBTITLE, "") : "");
    }

}
