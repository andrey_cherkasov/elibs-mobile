package ru.iprbooks.iprbooksmobile.data;

import androidx.lifecycle.MutableLiveData;

import javax.annotation.Nullable;

public final class MutableStateLiveData<T> extends MutableLiveData<StateData<T>> {

    public void setLoading() {
        setValue(new StateData<T>().loading());
    }

    public void setSuccess(@Nullable T data) {
        setValue(new StateData<T>().success(data));
    }

    public void setError(Throwable error) {
        setValue(new StateData<T>().error(error));
    }

}
