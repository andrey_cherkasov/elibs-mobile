package ru.iprbooks.iprbooksmobile.data.local;

import com.pddstudio.preferences.encrypted.EncryptedPreferences;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import javax.inject.Inject;
import javax.inject.Singleton;

import ru.iprbooks.iprbooksmobile.data.model.User;

@Singleton
public class SharedPreferencesHelper {

    private final static String USER = "USER";

    private static final String IS_NIGHT_MODE = "IS_NIGHT_MODE";
    private static final String VIEW_MODE = "VIEW_MODE";
    private static final String BRIGHTNESS = "BRIGHTNESS";
    private static final String IS_SYSTEM_BRIGHTNESS = "IS_SYSTEM_BRIGHTNESS";
    private static final String ALWAYS_ON = "ALWAYS_ON";


    private final EncryptedPreferences encryptedPreferences;
    private final EncryptedPreferences.EncryptedEditor encryptedEditor;

    @Inject
    public SharedPreferencesHelper(EncryptedPreferences encryptedPreferences,
                                   EncryptedPreferences.EncryptedEditor encryptedEditor) {
        this.encryptedPreferences = encryptedPreferences;
        this.encryptedEditor = encryptedEditor;
    }

    /* user */
    public void saveUser(User user) {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<User> jsonAdapter = moshi.adapter(User.class);
        encryptedEditor.putString(USER, jsonAdapter.toJson(user)).apply();
    }

    public User loadUser() {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<User> jsonAdapter = moshi.adapter(User.class);

        User user = null;
        String userJsonString = encryptedPreferences.getString(USER, null);

        try {
            user = jsonAdapter.fromJson(userJsonString);
        } catch (Exception ignored) {
        }

        return user;
    }

    public void deleteUser() {
        encryptedEditor.putString(USER, "").apply();
    }

    /* settings */
    public void setNightMode(boolean isNightMode) {
        encryptedEditor.putBoolean(IS_NIGHT_MODE, isNightMode).apply();
    }

    public boolean isNightMode() {
        return encryptedPreferences.getBoolean(IS_NIGHT_MODE, false);
    }

    public void setViewMode(int viewMode) {
        encryptedEditor.putInt(VIEW_MODE, viewMode).apply();
    }

    public int getViewMode() {
        return encryptedPreferences.getInt(VIEW_MODE, 0);
    }

    public void setBrightness(int brightness) {
        encryptedEditor.putInt(BRIGHTNESS, brightness).apply();
    }

    public int getBrightness() {
        return encryptedPreferences.getInt(BRIGHTNESS, 30);
    }

    public void setSystemBrightness(boolean isSystemBrightness) {
        encryptedEditor.putBoolean(IS_SYSTEM_BRIGHTNESS, isSystemBrightness).apply();
    }

    public boolean isSystemBrightness() {
        return encryptedPreferences.getBoolean(IS_SYSTEM_BRIGHTNESS, true);
    }

    public void setAlwaysOn(boolean alwaysOn) {
        encryptedEditor.putBoolean(ALWAYS_ON, alwaysOn).apply();
    }

    public boolean isAlwaysOn() {
        return encryptedPreferences.getBoolean(ALWAYS_ON, false);
    }

    public static int viewModeMapping(int mode) {
        switch (mode) {
            case 2:
                return 3;
            case 3:
                return 6;
            default:
                return mode;
        }
    }


}
