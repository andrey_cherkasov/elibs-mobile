package ru.iprbooks.iprbooksmobile.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Single;
import ru.iprbooks.iprbooksmobile.data.model.Publication;

@Dao
public interface PublicationDao {

    int PER_PAGE = 50;

    @Query("SELECT * FROM Publication WHERE publicationType = :publicationType AND userId = :userId LIMIT "
            + PER_PAGE + " OFFSET " + PER_PAGE + " * (:page - 1)")
    Single<List<Publication>> getPublications(int page, int publicationType, long userId);

    @Query("SELECT count(*) FROM Publication WHERE publicationType = :publicationType AND userId = :userId")
    int getPublicationsCount(int publicationType, long userId);

    @Query("SELECT * FROM Publication WHERE id = :id AND userId = :userId")
    Single<Publication> getPublication(long id, long userId);

    @Query("SELECT * FROM Publication WHERE id = :id AND userId = :userId")
    Publication getPublicationSimple(long id, long userId);

    @Query("UPDATE Publication SET lastPage = :lastPage WHERE localId = :localId")
    void savePage(long localId, int lastPage);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Single<Long> insert(Publication publication);

    @Update
    void update(Publication publication);

    @Query("DELETE FROM Publication WHERE id = :id")
    Single<Integer> delete(Long id);

    @Delete
    void delete(Publication publication);

}
