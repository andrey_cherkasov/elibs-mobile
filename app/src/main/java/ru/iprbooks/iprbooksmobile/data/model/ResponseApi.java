package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

public class ResponseApi {

    @Json(name = "meta")
    protected Meta meta;


    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

}
