package ru.iprbooks.iprbooksmobile.data.remote.retrofit.iterseptors;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Calendar;

import javax.inject.Inject;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.model.User;
import ru.iprbooks.iprbooksmobile.util.Utils;

public class AuthInterceptor implements Interceptor {

    private final SharedPreferencesHelper preferencesHelper;
    private final Utils utils;


    @Inject
    public AuthInterceptor(SharedPreferencesHelper preferencesHelper, Utils utils) {
        this.preferencesHelper = preferencesHelper;
        this.utils = utils;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();

        HttpUrl.Builder newUrlBuilder = request.url().newBuilder();
        User user = preferencesHelper.loadUser();

        if (user != null) {
            newUrlBuilder.addQueryParameter("uid", user.getId().toString())
                    .addQueryParameter("deviceId", utils.getDeviceId())
                    .addQueryParameter("date", utils.getDay(Calendar.getInstance().getTimeInMillis()))
                    .addQueryParameter("accessToken", utils.generateHash(BuildConfig.SALT, user.getAccessToken()));
        }

        Request newRequest = request.newBuilder()
                .url(newUrlBuilder.build())
                .addHeader("VERSION-API", BuildConfig.API_VERSION)
                .build();

        return chain.proceed(newRequest);
    }

}
