package ru.iprbooks.iprbooksmobile.data.remote;

import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import ru.iprbooks.iprbooksmobile.data.model.Autocomplete;
import ru.iprbooks.iprbooksmobile.data.model.Bookmark;
import ru.iprbooks.iprbooksmobile.data.model.DataList;
import ru.iprbooks.iprbooksmobile.data.model.Journal;
import ru.iprbooks.iprbooksmobile.data.model.JournalYear;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.Quote;
import ru.iprbooks.iprbooksmobile.data.model.Rating;
import ru.iprbooks.iprbooksmobile.data.model.ResponseApi;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;

public interface DataApiInterface {

    @GET("books/catalog")
    Single<ResponseData<DataList<Publication>>> getBooksCatalog(@Query("page") Integer page,
                                                                @QueryMap Map<String, String> search);

    @GET("books/{id}")
    Single<ResponseData<Publication>> getBook(@Path("id") Long id);

    @GET("{id}/rating/update")
    Single<ResponseData<Rating>> updatePublicationRating(@Path("id") Long publicationId,
                                                         @Query("value") Integer rating);

    @PUT("favorite/{id}")
    Single<ResponseData<String>> addPublicationToFavorites(@Path("id") Long id);

    @HTTP(method = "DELETE", path = "favorite/{id}", hasBody = true)
    Single<ResponseData<String>> deletePublicationFromFavorites(@Path("id") Long id);

    @GET("books/favorite")
    Single<ResponseData<DataList<Publication>>> getFavoriteBooksCatalog(@Query("page") Integer page,
                                                                        @Query("last_id") Long lastId);

    @GET("journals/favorite")
    Single<ResponseData<DataList<Publication>>> getFavoriteNumbersCatalog(@Query("page") Integer page,
                                                                          @Query("last_id") Long lastId);

    @GET("autocomplete")
    Observable<ResponseData<DataList<Autocomplete>>> autocomplete(@QueryMap Map<String, String> search);

    @GET("search")
    Single<ResponseData<DataList<Publication>>> search(@Query("s") String search,
                                                       @Query("type") Integer type,
                                                       @Query("page") Integer page);

    @FormUrlEncoded
    @POST("send/request")
    Single<ResponseApi> sendHelpRequest(@Field("selected_type") String type,
                                        @Field("text") String text);

    @GET("journals/catalog")
    Single<ResponseData<DataList<Journal>>> getJournalsCatalog(@Query("page") Integer page,
                                                               @QueryMap Map<String, String> search);

    @GET("journals/{id}")
    Single<ResponseData<Journal>> getJournal(@Path("id") Long id);

    @GET("years/{yearId}")
    Single<ResponseData<JournalYear>> getNumbersByYear(@Path("yearId") Long journalYearId);

    @GET("numbers/{id}")
    Single<ResponseData<Publication>> getNumber(@Path("id") Long id);

    @GET("{id}/bookmarks/get")
    Single<ResponseData<DataList<Bookmark>>> getBookmarks(@Path("id") Long id);

    @GET("{id}/bookmarks/delete")
    Call<ResponseApi> deleteBookmark(@Path("id") Long id,
                                     @Query("bookmark_id") Long ext_id);

    @GET("{id}/bookmarks/add")
    Call<ResponseData<Bookmark>> addBookmark(@Path("id") Long id,
                                             @Query("page_id") Integer pageId,
                                             @Query("text") String text);

    @GET("{id}/quotes/get")
    Single<ResponseData<DataList<Quote>>> getQuotes(@Path("id") Long id);

    @GET("{id}/quotes/delete")
    Call<ResponseApi> deleteQuote(@Path("id") Long id,
                                  @Query("quote_id") Long ext_id);

    @GET("{id}/quotes/add")
    Call<ResponseData<Quote>> addQuote(@Path("id") Long id,
                                       @Query("page_id") Integer pageId,
                                       @Query("text") String text);

    @GET("quotes/{ext_id}/update")
    Call<ResponseData<Quote>> updateQuote(@Path("ext_id") Long extId,
                                          @Query("text") String text);

}
