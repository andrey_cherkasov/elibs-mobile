package ru.iprbooks.iprbooksmobile.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import java.util.List;

public class JournalYear implements Parcelable {

    @Json(name = "id")
    private Long id;

    @Json(name = "year")
    private String year;

    @Json(name = "pagetitle")
    private String title;

    @Json(name = "tv_desc")
    private String description;

    @Json(name = "numbers")
    private List<Publication> numbers;


    public JournalYear() {

    }

    protected JournalYear(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        year = in.readString();
        title = in.readString();
        description = in.readString();
        numbers = in.createTypedArrayList(Publication.CREATOR);
    }

    public static final Creator<JournalYear> CREATOR = new Creator<JournalYear>() {
        @Override
        public JournalYear createFromParcel(Parcel in) {
            return new JournalYear(in);
        }

        @Override
        public JournalYear[] newArray(int size) {
            return new JournalYear[size];
        }
    };


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Publication> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Publication> numbers) {
        this.numbers = numbers;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(year);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeTypedList(numbers);
    }

}
