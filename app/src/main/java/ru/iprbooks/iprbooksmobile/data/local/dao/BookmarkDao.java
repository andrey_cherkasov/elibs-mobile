package ru.iprbooks.iprbooksmobile.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.iprbooks.iprbooksmobile.data.model.Bookmark;

@Dao
public interface BookmarkDao {

    @Query("SELECT * FROM Bookmark WHERE publicationId = :publicationId AND userId = :userId AND status = 0")
    Observable<List<Bookmark>> getBookmarks(long publicationId, long userId);

    @Query("SELECT * FROM Bookmark WHERE publicationId = :publicationId AND userId = :userId AND status != 0")
    List<Bookmark> getNeedSyncBookmarks(long publicationId, long userId);

    @Query("DELETE FROM Bookmark WHERE externalId = :extId")
    void deleteByExtId(long extId);

    @Query("UPDATE Bookmark SET status = 1 WHERE externalId = :externalId")
    Single<Integer> setDeleted(long externalId);

    @Query("DELETE FROM Bookmark WHERE publicationId = :publicationId AND userId = :userId")
    void deleteByPublication(long publicationId, long userId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Bookmark bookmark);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Bookmark> order);

    @Update
    void update(Bookmark bookmark);

    @Delete
    void delete(Bookmark bookmark);

}
