package ru.iprbooks.iprbooksmobile.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.PrimaryKey;

import com.squareup.moshi.Json;

public class Content implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private Long id;

    @Json(name = "ext_id")
    private Long externalId;

    private Long publicationId;

    @Json(name = "page")
    private Integer page;

    @Json(name = "text")
    private String text;

    @Json(name = "create_date")
    private String date;

    private long userId;

    private byte status = 0;


    public Content() {

    }

    protected Content(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        if (in.readByte() == 0) {
            externalId = null;
        } else {
            externalId = in.readLong();
        }
        if (in.readByte() == 0) {
            publicationId = null;
        } else {
            publicationId = in.readLong();
        }
        if (in.readByte() == 0) {
            page = null;
        } else {
            page = in.readInt();
        }
        text = in.readString();
        date = in.readString();
        userId = in.readLong();
        status = in.readByte();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        if (externalId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(externalId);
        }
        if (publicationId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(publicationId);
        }
        if (page == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(page);
        }
        dest.writeString(text);
        dest.writeString(date);
        dest.writeLong(userId);
        dest.writeByte(status);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Content> CREATOR = new Creator<Content>() {
        @Override
        public Content createFromParcel(Parcel in) {
            return new Content(in);
        }

        @Override
        public Content[] newArray(int size) {
            return new Content[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getExternalId() {
        return externalId;
    }

    public void setExternalId(Long externalId) {
        this.externalId = externalId;
    }

    public Long getPublicationId() {
        return publicationId;
    }

    public void setPublicationId(Long publicationId) {
        this.publicationId = publicationId;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

}
