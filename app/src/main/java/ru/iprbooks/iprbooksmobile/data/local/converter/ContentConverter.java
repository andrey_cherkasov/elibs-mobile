package ru.iprbooks.iprbooksmobile.data.local.converter;

import androidx.room.TypeConverter;

import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.lang.reflect.Type;
import java.util.List;

import ru.iprbooks.iprbooksmobile.data.model.Content;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;


public class ContentConverter {

    @TypeConverter
    public String fromContentList(ResponseData<List<Content>> contentList) {
        if (contentList == null) {
            return (null);
        }

        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(ResponseData.class, List.class, Content.class);
        JsonAdapter<ResponseData<List<Content>>> jsonAdapter = moshi.adapter(type);
        return jsonAdapter.toJson(contentList);
    }

    @TypeConverter
    public ResponseData<List<Content>> toContentList(String contentListString) {
        if (contentListString == null) {
            return (null);
        }

        Moshi moshi = new Moshi.Builder().build();
        Type listContentType = Types.newParameterizedType(List.class, Content.class);
        Type type = Types.newParameterizedType(ResponseData.class, listContentType);
        ResponseData<List<Content>> contentList = null;
        JsonAdapter<ResponseData<List<Content>>> jsonAdapter = moshi.adapter(type);
        try {
            contentList = jsonAdapter.fromJson(contentListString);
        } catch (Exception ignored) {
        }

        return contentList;
    }

}
