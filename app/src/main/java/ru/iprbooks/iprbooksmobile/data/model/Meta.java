package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

public class Meta {

    @Json(name = "success")
    private Boolean success;

    @Json(name = "message")
    private String message;


    public Boolean isSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
