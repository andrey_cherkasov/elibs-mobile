package ru.iprbooks.iprbooksmobile.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

public class Ugnp implements Parcelable {

    @Json(name = "id")
    private String id;

    @Json(name = "name")
    private String name;

    public Ugnp(String id, String name) {
        this.id = id;
        this.name = name;
    }

    protected Ugnp(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Ugnp> CREATOR = new Creator<Ugnp>() {
        @Override
        public Ugnp createFromParcel(Parcel in) {
            return new Ugnp(in);
        }

        @Override
        public Ugnp[] newArray(int size) {
            return new Ugnp[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
