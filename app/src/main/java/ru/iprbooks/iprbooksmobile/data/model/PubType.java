package ru.iprbooks.iprbooksmobile.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import java.util.Objects;

public class PubType implements Parcelable {

    @Json(name = "id")
    private Integer id;

    @Json(name = "title")
    private String title;

    @Json(name = "parentId")
    private Integer parentId;

    private Boolean isChecked;

    public PubType() {
        isChecked = false;
    }

    protected PubType(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        title = in.readString();
        if (in.readByte() == 0) {
            parentId = null;
        } else {
            parentId = in.readInt();
        }
        byte tmpIsChecked = in.readByte();
        isChecked = tmpIsChecked == 0 ? null : tmpIsChecked == 1;
    }

    public static final Creator<PubType> CREATOR = new Creator<PubType>() {
        @Override
        public PubType createFromParcel(Parcel in) {
            return new PubType(in);
        }

        @Override
        public PubType[] newArray(int size) {
            return new PubType[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        parcel.writeString(title);
        if (parentId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(parentId);
        }
        parcel.writeByte((byte) (isChecked == null ? 0 : isChecked ? 1 : 2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PubType)) return false;
        PubType pubType = (PubType) o;
        return getId().equals(pubType.getId()) && getTitle().equals(pubType.getTitle()) && Objects.equals(getParentId(), pubType.getParentId()) && isChecked.equals(pubType.isChecked);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTitle(), getParentId(), isChecked);
    }
}
