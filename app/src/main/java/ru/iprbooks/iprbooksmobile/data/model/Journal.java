package ru.iprbooks.iprbooksmobile.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.squareup.moshi.Json;

import java.util.List;

public class Journal implements Parcelable, IListItem {

    @Json(name = "id")
    private Long id;

    @Json(name = "pagetitle")
    private String title;

    @Json(name = "coutry")
    private String country;

    @Json(name = "tv_desc")
    private String description;

    @Json(name = "tv_incollection")
    private String inCollection;

    @Json(name = "isbn")
    private String issn;

    @Json(name = "place")
    private String place;

    @Json(name = "tv_pubhouse")
    private String pubHouse;

    @Json(name = "tv_invak")
    private String inVak;

    @Json(name = "pub_year")
    private Integer pubYear;

    @Json(name = "image")
    private String image;

    @Json(name = "big_image")
    private String bigImage;

    @Json(name = "years")
    private List<JournalYear> years;


    public Journal() {

    }

    protected Journal(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        title = in.readString();
        country = in.readString();
        description = in.readString();
        inCollection = in.readString();
        issn = in.readString();
        place = in.readString();
        pubHouse = in.readString();
        inVak = in.readString();
        if (in.readByte() == 0) {
            pubYear = null;
        } else {
            pubYear = in.readInt();
        }
        image = in.readString();
        bigImage = in.readString();
        years = in.createTypedArrayList(JournalYear.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(title);
        dest.writeString(country);
        dest.writeString(description);
        dest.writeString(inCollection);
        dest.writeString(issn);
        dest.writeString(place);
        dest.writeString(pubHouse);
        dest.writeString(inVak);
        if (pubYear == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pubYear);
        }
        dest.writeString(image);
        dest.writeString(bigImage);
        dest.writeTypedList(years);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Journal> CREATOR = new Creator<Journal>() {
        @Override
        public Journal createFromParcel(Parcel in) {
            return new Journal(in);
        }

        @Override
        public Journal[] newArray(int size) {
            return new Journal[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInCollection() {
        return inCollection;
    }

    public void setInCollection(String inCollection) {
        this.inCollection = inCollection;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPubHouse() {
        return pubHouse;
    }

    public void setPubHouse(String pubHouse) {
        this.pubHouse = pubHouse;
    }

    public String getInVak() {
        return inVak;
    }

    public void setInVak(String inVak) {
        this.inVak = inVak;
    }

    public Integer getPubYear() {
        return pubYear;
    }

    public void setPubYear(Integer pubYear) {
        this.pubYear = pubYear;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBigImage() {
        return bigImage;
    }

    public void setBigImage(String bigImage) {
        this.bigImage = bigImage;
    }

    public List<JournalYear> getYears() {
        return years;
    }

    public void setYears(List<JournalYear> years) {
        this.years = years;
    }

}
