package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

public class User {

    @Json(name = "uid")
    private Long id;

    @Json(name = "username")
    private String username;

    @Json(name = "fullname")
    private String fullName;

    @Json(name = "email")
    private String email;

    @Json(name = "time")
    private Long time;

    @Json(name = "blockedAfter")
    private Long blockedAfter;

    @Json(name = "userperiod")
    private Long userPeriod;

    @Json(name = "clientname")
    private String clientName;

    @Json(name = "accessToken")
    private String accessToken;

    @Json(name = "clientLogotype")
    private String logo;

    @Json(name = "clientColor")
    private String clientColor;

    @Json(name = "org_name")
    private String orgName;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Long getBlockedAfter() {
        return blockedAfter;
    }

    public void setBlockedAfter(Long blockedAfter) {
        this.blockedAfter = blockedAfter;
    }

    public Long getUserPeriod() {
        return userPeriod;
    }

    public void setUserPeriod(Long userPeriod) {
        this.userPeriod = userPeriod;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getClientColor() {
        return clientColor;
    }

    public void setClientColor(String clientColor) {
        this.clientColor = clientColor;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

}
