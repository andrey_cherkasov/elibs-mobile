package ru.iprbooks.iprbooksmobile.data.model;

import java.util.Objects;

public class PublicationListItemVO implements IListItem {

    private final long id;
    private final String title;
    private final String subtitle;
    private final String description;
    private final String image;

    public PublicationListItemVO(long id, String title, String subtitle, String description, String image) {
        this.id = id;
        this.title = title;
        this.subtitle = subtitle;
        this.description = description;
        this.image = image;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PublicationListItemVO)) return false;
        PublicationListItemVO journalVO = (PublicationListItemVO) o;
        return id == journalVO.id
                && title.equals(journalVO.title)
                && subtitle.equals(journalVO.subtitle)
                && description.equals(journalVO.description)
                && image.equals(journalVO.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, subtitle, description, image);
    }

}
