package ru.iprbooks.iprbooksmobile.data.local;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.util.Locale;

import ru.iprbooks.iprbooksmobile.BuildConfig;

public class FileHelper {

    private final File filesDir;
    private final Long userId;

    public FileHelper(Context context, SharedPreferencesHelper preferencesHelper) {
        this.userId = preferencesHelper.loadUser().getId();
        filesDir = context.getFilesDir();
        if (!filesDir.exists()) {
            if (!filesDir.mkdirs()) {
                Log.e(BuildConfig.TAG, "Error while creating files directory");
            }
        }
    }

    public String getFilesPath() {
        return String.format(Locale.getDefault(), "%s/%d", filesDir.getAbsolutePath(), userId);
    }

    public void deleteFileIfExists(long fileId) {
        String pathFile = String.format(Locale.getDefault(), "%d/%d", userId, fileId);
        File file = new File(filesDir, pathFile);
        if (file.exists()) {
            file.delete();
        }

    }

    public File getFileById(long publicationId) {
        return new File(String.format(Locale.getDefault(), "%s/%d", getFilesPath(), publicationId));
    }

}
