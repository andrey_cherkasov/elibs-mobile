package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

public class Autocomplete {

    @Json(name = "item")
    private String item;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

}
