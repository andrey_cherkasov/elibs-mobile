package ru.iprbooks.iprbooksmobile.data;

public enum DataStatus {
    LOADING,
    SUCCESS,
    ERROR
}
