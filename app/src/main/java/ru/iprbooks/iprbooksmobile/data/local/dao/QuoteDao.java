package ru.iprbooks.iprbooksmobile.data.local.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import ru.iprbooks.iprbooksmobile.data.model.Quote;

@Dao
public interface QuoteDao {

    @Query("SELECT * FROM Quote WHERE publicationId = :publicationId AND userId = :userId AND status = 0")
    Observable<List<Quote>> getQuotes(long publicationId, long userId);

    @Query("SELECT * FROM Quote WHERE publicationId = :publicationId AND userId = :userId AND status != 0")
    List<Quote> getNeedSyncQuotes(long publicationId, long userId);

    @Query("DELETE FROM Quote WHERE externalId = :extId")
    void deleteByExtId(long extId);

    @Query("DELETE FROM Quote WHERE publicationId = :publicationId AND userId = :userId")
    void deleteByPublication(long publicationId, long userId);

    @Query("UPDATE Quote SET text = :text, date = :date WHERE id = :id")
    void update(long id, String text, String date);

    @Query("UPDATE Quote SET status = 1 WHERE externalId = :externalId")
    Single<Integer> setDeleted(long externalId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Long insert(Quote quote);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Quote> quote);

    @Update
    void update(Quote quote);

    @Delete
    void delete(Quote quote);

}
