package ru.iprbooks.iprbooksmobile.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.squareup.moshi.Json;

import java.util.List;
import java.util.Objects;

import ru.iprbooks.iprbooksmobile.data.local.converter.ContentConverter;
import ru.iprbooks.iprbooksmobile.util.PublicationType;

@Entity
public class Publication implements Parcelable, IListItem {

    @PrimaryKey(autoGenerate = true)
    private long localId;

    @Json(name = "id")
    private Long id;

    @Json(name = "pagetitle")
    private String title;

    @Json(name = "number")
    private String number;

    @Json(name = "tv_author")
    private String authors;

    @Json(name = "tv_liability")
    private String additionalAuthors;

    @Json(name = "tv_desc")
    private String description;

    @Json(name = "isbn")
    private String isbn;

    @Json(name = "place")
    private String place;

    @Json(name = "tv_pubhouse")
    private String pubHouse;

    @Json(name = "pub_year")
    private Integer pubYear;

    @Json(name = "image")
    private String image;

    @Json(name = "countpages")
    private String pagesCount;

    @Json(name = "big_image")
    private String bigImage;

    @Json(name = "size")
    private Long size;

    @Json(name = "isFavorite")
    private String isFavorite;

    @Json(name = "content")
    @TypeConverters({ContentConverter.class})
    private ResponseData<List<Content>> content;

    @Json(name = "rating")
    private Float rating;

    @Json(name = "user_rating")
    private Float userRating;

    private long userId = 0L;

    private int publicationType = PublicationType.BOOK;

    private boolean downloaded = false;

    private int lastPage = 0;

    private boolean downloading = false;


    public Publication() {

    }


    protected Publication(Parcel in) {
        localId = in.readLong();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        title = in.readString();
        number = in.readString();
        authors = in.readString();
        additionalAuthors = in.readString();
        description = in.readString();
        isbn = in.readString();
        place = in.readString();
        pubHouse = in.readString();
        if (in.readByte() == 0) {
            pubYear = null;
        } else {
            pubYear = in.readInt();
        }
        image = in.readString();
        pagesCount = in.readString();
        bigImage = in.readString();
        if (in.readByte() == 0) {
            size = null;
        } else {
            size = in.readLong();
        }
        isFavorite = in.readString();
        if (in.readByte() == 0) {
            rating = null;
        } else {
            rating = in.readFloat();
        }
        if (in.readByte() == 0) {
            userRating = null;
        } else {
            userRating = in.readFloat();
        }
        userId = in.readLong();
        publicationType = in.readInt();
        downloaded = in.readByte() != 0;
        lastPage = in.readInt();
        downloading = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(localId);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        dest.writeString(title);
        dest.writeString(number);
        dest.writeString(authors);
        dest.writeString(additionalAuthors);
        dest.writeString(description);
        dest.writeString(isbn);
        dest.writeString(place);
        dest.writeString(pubHouse);
        if (pubYear == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pubYear);
        }
        dest.writeString(image);
        dest.writeString(pagesCount);
        dest.writeString(bigImage);
        if (size == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(size);
        }
        dest.writeString(isFavorite);
        if (rating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeFloat(rating);
        }
        if (userRating == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeFloat(userRating);
        }
        dest.writeLong(userId);
        dest.writeInt(publicationType);
        dest.writeByte((byte) (downloaded ? 1 : 0));
        dest.writeInt(lastPage);
        dest.writeByte((byte) (downloading ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Publication> CREATOR = new Creator<Publication>() {
        @Override
        public Publication createFromParcel(Parcel in) {
            return new Publication(in);
        }

        @Override
        public Publication[] newArray(int size) {
            return new Publication[size];
        }
    };

    public long getLocalId() {
        return localId;
    }

    public void setLocalId(long localId) {
        this.localId = localId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAuthors() {
        return authors;
    }

    public void setAuthors(String authors) {
        this.authors = authors;
    }

    public String getAdditionalAuthors() {
        return additionalAuthors;
    }

    public void setAdditionalAuthors(String additionalAuthors) {
        this.additionalAuthors = additionalAuthors;
    }

    public String getFullAuthors() {
        return String.format("%s %s", !TextUtils.isEmpty(this.authors) ? this.authors : "",
                !TextUtils.isEmpty(this.additionalAuthors) ? this.additionalAuthors : "");
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPubHouse() {
        return pubHouse;
    }

    public void setPubHouse(String pubHouse) {
        this.pubHouse = pubHouse;
    }

    public Integer getPubYear() {
        return pubYear;
    }

    public void setPubYear(Integer pubYear) {
        this.pubYear = pubYear;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(String pagesCount) {
        this.pagesCount = pagesCount;
    }

    public String getBigImage() {
        return bigImage;
    }

    public void setBigImage(String bigImage) {
        this.bigImage = bigImage;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(String isFavorite) {
        this.isFavorite = isFavorite;
    }

    public boolean isFavorite() {
        return isFavorite.equals("1");
    }

    public void setFavorite(boolean favorite) {
        this.isFavorite = favorite ? "1" : "0";
    }

    public ResponseData<List<Content>> getContent() {
        return content;
    }

    public void setContent(ResponseData<List<Content>> content) {
        this.content = content;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Float getUserRating() {
        return userRating;
    }

    public void setUserRating(Float userRating) {
        this.userRating = userRating;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public int getPublicationType() {
        return publicationType;
    }

    public void setPublicationType(int publicationType) {
        this.publicationType = publicationType;
    }

    public boolean isDownloaded() {
        return downloaded;
    }

    public void setDownloaded(boolean downloaded) {
        this.downloaded = downloaded;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public boolean isDownloading() {
        return downloading;
    }

    public void setDownloading(boolean downloading) {
        this.downloading = downloading;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Publication)) return false;
        Publication that = (Publication) o;
        return localId == that.localId
                && userId == that.userId
                && publicationType == that.publicationType
                && downloaded == that.downloaded
                && lastPage == that.lastPage
                && downloading == that.downloading
                && id.equals(that.id)
                && title.equals(that.title)
                && Objects.equals(number, that.number)
                && Objects.equals(authors, that.authors)
                && Objects.equals(additionalAuthors, that.additionalAuthors)
                && Objects.equals(description, that.description)
                && Objects.equals(isbn, that.isbn)
                && Objects.equals(place, that.place)
                && Objects.equals(pubHouse, that.pubHouse)
                && Objects.equals(pubYear, that.pubYear)
                && Objects.equals(image, that.image)
                && Objects.equals(pagesCount, that.pagesCount)
                && Objects.equals(bigImage, that.bigImage)
                && Objects.equals(size, that.size)
                && Objects.equals(isFavorite, that.isFavorite)
                && Objects.equals(content, that.content)
                && Objects.equals(rating, that.rating)
                && Objects.equals(userRating, that.userRating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(localId, id, title, number, authors, additionalAuthors, description, isbn,
                place, pubHouse, pubYear, image, pagesCount, bigImage, size, isFavorite, content,
                rating, userRating, userId, publicationType, downloaded, lastPage, downloading);
    }

}
