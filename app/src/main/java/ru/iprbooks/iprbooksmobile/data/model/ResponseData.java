package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;


public class ResponseData<T> extends ResponseApi {

    @Json(name = "data")
    protected T data;


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}