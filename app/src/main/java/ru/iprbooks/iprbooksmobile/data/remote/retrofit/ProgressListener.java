package ru.iprbooks.iprbooksmobile.data.remote.retrofit;

public interface ProgressListener {

    void update(long bytesRead, long contentLength, boolean done) throws Exception;

}
