package ru.iprbooks.iprbooksmobile.data.remote.service;

import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.hilt.work.HiltWorker;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import dagger.assisted.Assisted;
import dagger.assisted.AssistedInject;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import ru.iprbooks.iprbooksmobile.BuildConfig;
import ru.iprbooks.iprbooksmobile.data.local.FileHelper;
import ru.iprbooks.iprbooksmobile.data.local.SharedPreferencesHelper;
import ru.iprbooks.iprbooksmobile.data.local.dao.PublicationDao;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.remote.DownloadFileInterface;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.Progress;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.ProgressListener;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.iterseptors.AuthInterceptor;
import ru.iprbooks.iprbooksmobile.data.remote.retrofit.iterseptors.LoggingInterceptor;
import ru.iprbooks.iprbooksmobile.util.Utils;

@HiltWorker
public class DownloadWorkManager extends Worker {

    public final static String PUBLICATION_ID = "PUBLICATION_ID";
    public final static String PROGRESS = "PROGRESS";
    public final static String PROGRESS_BYTES = "PROGRESS_BYTES";

    private final FileHelper fileHelper;
    private final SharedPreferencesHelper preferencesHelper;
    private final Utils utils;
    private final PublicationDao publicationDao;

    private Call<ResponseBody> request;
    private Long publicationId;
    private Publication book;

    private final ProgressListener listener = (bytesRead, contentLength, done) -> {
        double progressBytes = bytesRead / Math.pow(1024, 2);
        int progress = (int) ((bytesRead * 100) / book.getSize());

        Intent intent = new Intent(PROGRESS + "_" + publicationId);
        intent.putExtra(PROGRESS, progress);
        intent.putExtra(PROGRESS_BYTES, progressBytes);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
    };

    @AssistedInject
    public DownloadWorkManager(@Assisted @NonNull Context context,
                               @Assisted @NonNull WorkerParameters workerParams,
                               FileHelper filehelper,
                               SharedPreferencesHelper preferencesHelper,
                               Utils utils,
                               PublicationDao publicationDao) {
        super(context, workerParams);
        this.fileHelper = filehelper;
        this.preferencesHelper = preferencesHelper;
        this.utils = utils;
        this.publicationDao = publicationDao;
    }

    @Override
    public void onStopped() {
        super.onStopped();
        request.cancel();
    }

    @NonNull
    @Override
    public Result doWork() {
        publicationId = getInputData().getLong(PUBLICATION_ID, 0);

        if (publicationId == 0) {
            return Result.failure();
        }

        book = publicationDao.getPublicationSimple(publicationId, preferencesHelper.loadUser().getId());
        if (book != null && book.isDownloading() && !book.isDownloaded()) {
            try {
                request = initDownloadRequest();
                retrofit2.Response<ResponseBody> response = request.execute();
                if (response.code() == 403) {
                    if (response.errorBody() == null) {
                        publicationDao.delete(book);
                        return Result.failure();
                    }
                } else if (response.code() == 200 && response.body() != null) {
                    fileHelper.deleteFileIfExists(publicationId);

                    int count;
                    byte[] data = new byte[1024 * 4];
                    InputStream bis = new BufferedInputStream(response.body().byteStream(), 1024 * 8);
                    String path = fileHelper.getFilesPath();

                    File outputFile = new File(path + "/", publicationId.toString());
                    File parent = outputFile.getParentFile();
                    if (parent != null && !parent.exists() && !parent.mkdirs()) {
                        throw new IllegalStateException("Couldn't create dir: " + parent);
                    }

                    OutputStream output = new FileOutputStream(outputFile);
                    while ((count = bis.read(data)) != -1) {
                        output.write(data, 0, count);
                    }

                    book.setDownloaded(true);
                    book.setDownloading(false);
                    publicationDao.update(book);

                    output.flush();
                    output.close();
                    bis.close();
                } else {
                    publicationDao.delete(book);
                    return Result.failure();
                }
            } catch (Exception e) {
                publicationDao.delete(book);
                e.printStackTrace();
                return Result.failure();
            }
        }

        return Result.success();
    }

    private Call<ResponseBody> initDownloadRequest() {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(LoggingInterceptor.create())
                .addInterceptor(new AuthInterceptor(preferencesHelper, utils))
                .addNetworkInterceptor(chain -> {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new Progress(originalResponse.body(), listener))
                            .build();
                }).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .client(client)
                .build();

        return retrofit.create(DownloadFileInterface.class).download(publicationId);
    }

}
