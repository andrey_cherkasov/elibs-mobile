package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

public class Rating {

    @Json(name = "value")
    private Float rating;

    @Json(name = "user_value")
    private Float userRating;

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public Float getUserRating() {
        return userRating;
    }

    public void setUserRating(Float userRating) {
        this.userRating = userRating;
    }

}
