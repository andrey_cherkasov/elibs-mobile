package ru.iprbooks.iprbooksmobile.data.remote;

import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;

public interface DownloadFileInterface {

    @GET("download")
    @Streaming
    Call<ResponseBody> download(@Query("id") Long publicationId);

}
