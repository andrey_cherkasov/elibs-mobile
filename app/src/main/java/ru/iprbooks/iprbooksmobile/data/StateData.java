package ru.iprbooks.iprbooksmobile.data;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class StateData<T> {

    private DataStatus status;
    private T data;
    private Throwable error;


    StateData<T> loading() {
        status = DataStatus.LOADING;
        data = null;
        return this;
    }

    public StateData<T> success(@Nullable T data) {
        this.status = DataStatus.SUCCESS;
        this.data = data;
        this.error = null;
        return this;
    }

    public StateData<T> error(@NonNull Throwable error) {
        this.status = DataStatus.ERROR;
        this.data = null;
        this.error = error;
        return this;
    }

    @NonNull
    public DataStatus getStatus() {
        return status;
    }

    @Nullable
    public T getData() {
        return data;
    }

    @Nullable
    public Throwable getError() {
        return error;
    }

}
