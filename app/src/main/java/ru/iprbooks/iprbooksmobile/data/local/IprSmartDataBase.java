package ru.iprbooks.iprbooksmobile.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import ru.iprbooks.iprbooksmobile.data.local.dao.BookmarkDao;
import ru.iprbooks.iprbooksmobile.data.local.dao.PublicationDao;
import ru.iprbooks.iprbooksmobile.data.local.dao.QuoteDao;
import ru.iprbooks.iprbooksmobile.data.model.Bookmark;
import ru.iprbooks.iprbooksmobile.data.model.Publication;
import ru.iprbooks.iprbooksmobile.data.model.Quote;

@Database(entities = {Publication.class, Quote.class, Bookmark.class}, version = 1)
public abstract class IprSmartDataBase extends RoomDatabase {

    public abstract PublicationDao publicationDao();

    public abstract QuoteDao quoteDao();

    public abstract BookmarkDao bookmarkDao();

}
