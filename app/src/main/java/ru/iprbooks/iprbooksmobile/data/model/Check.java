package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

public class Check {

    @Json(name = "is_active")
    Integer isActive;

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

}
