package ru.iprbooks.iprbooksmobile.data.remote;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.iprbooks.iprbooksmobile.data.model.ResponseApi;
import ru.iprbooks.iprbooksmobile.data.model.ResponseData;
import ru.iprbooks.iprbooksmobile.data.model.User;


public interface AuthApiInterface {

    @FormUrlEncoded
    @POST("users/auth/")
    Single<ResponseData<User>> auth(@Field("username") String username,
                                    @Field("device_id") String deviceId,
                                    @Field("passwd") String password);

    @GET("/users/restore")
    Single<ResponseData<User>> restore(@Query("email") String email);

    @GET("/users/check")
    Single<ResponseApi> checkLibraryCredentials(@Query("username") String libLogin,
                                                @Query("password") String libPass);

    @POST("/users/create")
    Single<ResponseData<User>> registerNewUser(@Query("username") String libLogin,
                                               @Query("password") String libPass,
                                               @Query("fullname") String fio,
                                               @Query("email") String email,
                                               @Query("usertype") Integer userType,
                                               @Query("user_password") String userPass);

}
