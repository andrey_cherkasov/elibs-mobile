package ru.iprbooks.iprbooksmobile.data.model;

import com.squareup.moshi.Json;

import java.util.List;

public class DataList<T> {

    @Json(name = "total")
    private int total;

    @Json(name = "last_page")
    private int lastPage;

    @Json(name = "per_page")
    private int perPage;

    @Json(name = "current_page")
    private int currentPage;

    @Json(name = "data")
    private List<T> data;

    public DataList() {
    }

    public DataList(int total, int lastPage, int perPage, int currentPage, List<T> data) {
        this.total = total;
        this.lastPage = lastPage;
        this.perPage = perPage;
        this.currentPage = currentPage;
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getLastPage() {
        return lastPage;
    }

    public void setLastPage(int lastPage) {
        this.lastPage = lastPage;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

}
